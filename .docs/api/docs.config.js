module.exports = {
  /**
   * See also: https://github.com/ph1p/vuepress-jsdoc
   */
  /**
   * Title
   */
  title: 'API',
  /**
   * Source directory
   * > relative to root folder
   */
  sourceDir: './src',
  /**
   * Dist directory
   * > relative to root folder
   */
  distDir: './.docs/api',
  /**
   * Output directory
   * > relative to distDir folder
   */
  outputDir: 'output',
  /**
   * Exclude files
   */
  exclude: '**/*/*.test.js',
  /**
   * Report file (will contain vuepress-jsdoc stdout)
   * Path to file, relative to root folder
   */
  reportFile: './.docs/api.report.txt',
  /**
   * Prefix for path in sidebar
   */
  sidebarPathPrefix: '/guide/ui/api/'
}
