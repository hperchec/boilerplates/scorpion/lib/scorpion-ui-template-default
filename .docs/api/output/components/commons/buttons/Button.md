---
title: Button
---

  # Button

  
  > Button component
  
  
  
  
  
  
  

  
## Props

  | Prop name     | Description | Type      | Values      | Default     |
  | ------------- | ----------- | --------- | ----------- | ----------- |
  | to | 'To', if button is a router-link | string | - | undefined |
| variant | Tailwind variant for the button | string | - | '' |
| textAlign | Define text alignment | string | - | 'center' |
| classes | Classes to add to the t-button | string | - | '' |
| fluid | Define if button is full width | boolean | - | false |
| disabled | Define if button is disabled | boolean | - | false |
| inline | Define if button is displayed as inline-block | boolean | - | false |

  
  
  
  
## Slots

  | Name          | Description  | Bindings |
  | ------------- | ------------ | -------- |
  | default |  |  |

  ---


  
  