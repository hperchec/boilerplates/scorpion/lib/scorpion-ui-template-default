---
title: Input
---

  # Input

  
  > Input component
  
  
  
  
  
  
  

  
## Props

  | Prop name     | Description | Type      | Values      | Default     |
  | ------------- | ----------- | --------- | ----------- | ----------- |
  | label | label | string | - | '' |
| labelClass | labelClass | string | - | '' |
| name | name | string | - | '' |
| type | type | string | - | 'text' |
| variant | variant | string | - | '' |
| fluid | fluid: define is width: 100% | boolean | - | false |
| required | required | boolean | - | false |
| placeholder | placeholder | string | - | '' |
| state | state | boolean | - | true |
| inputClass | inputClass | string | - | '' |
| inputRef | inputRef | string | - | function () {<br/>  return `input_${random.alphaNumeric(10)}` // Random string of length: 10<br/>} |
| validatedStatus | validatedStatus | boolean | - | null |
| description | description | string | - | '' |
| descriptionClass | descriptionClass | string | - | '' |
| error | error | string | - | '' |
| errorClass | errorClass | string | - | '' |
| value | value (to use v-model directive)<br/>See -> https://vuejs.org/v2/guide/components-custom-events.html#Customizing-Component-v-model | string | - | '' |
| lazyValidation | lazyValidation | func | - | null |
| wait | wait | number | - | 1000 |

  
  
  
## Events

  | Event name     | Properties     | Description  |
  | -------------- |--------------- | -------------|
  | input |  | 
| changed |  | 

  
  
## Slots

  | Name          | Description  | Bindings |
  | ------------- | ------------ | -------- |
  | label |  |  |
| input |  |  |
| description |  |  |
| error |  |  |

  ---


  
  