---
title: BaseIcon
---

  # BaseIcon

  
  > BaseIcon component
  
  
  
  
  
  
  

  
## Props

  | Prop name     | Description | Type      | Values      | Default     |
  | ------------- | ----------- | --------- | ----------- | ----------- |
  | fill | fill | string | - | 'red' |
| hoverFill | fill when 'hover' | string | - | function() {<br/>  return this.fill<br/>} |
| width | width | string | - | '26px' |
| height | height | string | - | function() {<br/>  return this.width<br/>} |

  
  
  
  
  ---


  
  