---
title: Link
---

  # Link

  
  > Link component
  
  
  
  
  
  
  

  
## Props

  | Prop name     | Description | Type      | Values      | Default     |
  | ------------- | ----------- | --------- | ----------- | ----------- |
  | to | 'To', if link is a router-link | string | - | undefined |
| href | 'href', if link is an anchor | string | - | undefined |
| external | Define if the link is external (add indicator icon) | boolean | - | false |
| noUnderline | Define if not apply 'underline' effect | boolean | - | false |

  
  
  
  
## Slots

  | Name          | Description  | Bindings |
  | ------------- | ------------ | -------- |
  | default |  |  |

  ---


  
  