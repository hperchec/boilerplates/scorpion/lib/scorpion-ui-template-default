---
title: EmailSentModal
---

  # EmailSentModal

  
  > EmailSentModal component
  
  
  
  
  
  
  

  
## Props

  | Prop name     | Description | Type      | Values      | Default     |
  | ------------- | ----------- | --------- | ----------- | ----------- |
  | layer | layer<br/>`@description` Can be a number or "foreground". (Default: 1)<br/>`@type` undefined | union | - | function() {<br/>  return 1<br/>} |

  
  
  
  
  ---


  
  