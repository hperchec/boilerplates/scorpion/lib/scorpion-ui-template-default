---
title: TabView
---

  # TabView

  
  > TabView component
  
  
  
  
  
  
  

  
## Props

  | Prop name     | Description | Type      | Values      | Default     |
  | ------------- | ----------- | --------- | ----------- | ----------- |
  | tabs | Tabs<br/><br/>Object like:<br/>```javascript<br/>{<br/>  tab1: {<br/>    title: String<br/>  },<br/>  tab2: {<br/>    title: String<br/>  },<br/>  ...<br/>}<br/>``` | object\|array | - | function () {<br/>  return {}<br/>} |
| defaultTab | Index of default tab to display when component is mounted (default 0: first tab). | string | - | function () {<br/>  return this.tabs instanceof Array ? this.tabs[0] : Object.keys(this.tabs)[0]<br/>} |
| stack | stack<br/>stack header tabs to left or right | string | - | null |

  
  
  
  
## Slots

  | Name          | Description  | Bindings |
  | ------------- | ------------ | -------- |
  | default |  | <br/> |

  ---


  
  