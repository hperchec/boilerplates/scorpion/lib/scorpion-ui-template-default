---
title: AnonymousThumbnail
---

  # AnonymousThumbnail

  
  
  
  
  
  
  
  
  

  
## Props

  | Prop name     | Description | Type      | Values      | Default     |
  | ------------- | ----------- | --------- | ----------- | ----------- |
  | width |  | string\|number | - | 250 |
| height |  | string\|number | - | 250 |

  
  
  
  
  ---


  
  