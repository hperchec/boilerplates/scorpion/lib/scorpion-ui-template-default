---
title: UserThumbnail
---

  # UserThumbnail

  
  > UserThumbnail component
  
  
  
  
  
  
  

  
## Props

  | Prop name     | Description | Type      | Values      | Default     |
  | ------------- | ----------- | --------- | ----------- | ----------- |
  | data |  | string | - |  |
| width |  | number | - | 250 |
| height |  | number | - | function () {<br/>  return this.width<br/>} |

  
  
  
  
  ---


  
  