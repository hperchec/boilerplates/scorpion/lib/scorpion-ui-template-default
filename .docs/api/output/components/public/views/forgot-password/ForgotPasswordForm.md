---
title: ForgotPasswordForm
---

  # ForgotPasswordForm

  
  > ForgotPasswordForm component
  
  
  
  
  
  
  

  
  
  
## Events

  | Event name     | Properties     | Description  |
  | -------------- |--------------- | -------------|
  | email-sent |  | 

  
  
  ---


  
  