---
title: Models
headline: Models
sidebarTitle: .models
prev: false # Disable prev link
next: false # Disable prev link
---

# Models

<a name="models"></a>

## .models : <code>Object</code>
> [context](./).[models](#)

**Kind**: static member  
<a name="models.User"></a>

### models.User : <code>function</code>
**Kind**: static class of [<code>models</code>](#models)  
**See**: [User](./User)  
