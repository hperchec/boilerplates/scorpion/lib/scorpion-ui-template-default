---
title: User class
headline: User class
sidebarTitle: .User
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# User class

<a name="User"></a>

## User ⇐ <code>Model</code>
> [context](../).[models](./).[User](#)Class that represents a user

**Kind**: global class  
**Extends**: <code>Model</code>  

* [User](#User) ⇐ <code>Model</code>
    * [new User(data)](#new_User_new)
    * _async methods_
        * [.setThumbnailBase64()](#User+setThumbnailBase64) ⇒ <code>Promise</code>
    * _methods_
        * [.fullName()](#User+fullName) ⇒ <code>string</code>
    * _properties_
        * [.id](#User+id) : <code>number</code>
        * [.firstname](#User+firstname) : <code>string</code> \| <code>null</code>
        * [.lastname](#User+lastname) : <code>string</code> \| <code>null</code>
        * [.email](#User+email) : <code>string</code>
        * [.thumbnailFilename](#User+thumbnailFilename) : <code>string</code> \| <code>null</code>
        * [.createdAt](#User+createdAt) : <code>Date</code>
        * [.updatedAt](#User+updatedAt) : <code>Date</code> \| <code>null</code>
        * [.emailVerifiedAt](#User+emailVerifiedAt) : <code>Date</code> \| <code>null</code>
        * [.lastLogin](#User+lastLogin) : <code>Date</code> \| <code>null</code>
        * [.thumbnailURL](#User+thumbnailURL) : <code>string</code> \| <code>null</code>
        * [.thumbnailBase64](#User+thumbnailBase64) : <code>string</code> \| <code>null</code>

<a name="new_User_new"></a>

### new User(data)
Create a new User instance


| Param | Type | Description |
| --- | --- | --- |
| data | <code>Object</code> | Data object |
| data.id | <code>number</code> | User id |
| data.firstname | <code>string</code> \| <code>null</code> | User firstname |
| data.lastname | <code>string</code> \| <code>null</code> | User lastname |
| data.email | <code>string</code> | User email |
| data.thumbnail | <code>string</code> \| <code>null</code> | User thumbnail filename |
| data.created_at | <code>string</code> | User creation date |
| data.updated_at | <code>string</code> \| <code>null</code> | User last update date |
| data.email_verified_at | <code>string</code> \| <code>null</code> | User email verification date |
| data.last_login | <code>string</code> \| <code>null</code> | User last login date |

<a name="User+setThumbnailBase64"></a>

### user.setThumbnailBase64() ⇒ <code>Promise</code>
Set base64 thumbnail data (async request: `[GET] <ServerAPIBaseURL>/users/<id>/thumbnail`)

**Kind**: instance method of [<code>User</code>](#User)  
**Category**: async methods  
**Fulfil**: <code>void</code>  
<a name="User+fullName"></a>

### user.fullName() ⇒ <code>string</code>
Returns concatenated firstname and lastname with a white space

**Kind**: instance method of [<code>User</code>](#User)  
**Category**: methods  
<a name="User+id"></a>

### user.id : <code>number</code>
User id accessor> Set in constructor from `data.id`

**Kind**: instance property of [<code>User</code>](#User)  
**Default**: <code>undefined</code>  
**Category**: properties  
<a name="User+firstname"></a>

### user.firstname : <code>string</code> \| <code>null</code>
User firstname accessor> Set in constructor from `data.firstname`

**Kind**: instance property of [<code>User</code>](#User)  
**Default**: <code>&quot;null&quot;</code>  
**Category**: properties  
<a name="User+lastname"></a>

### user.lastname : <code>string</code> \| <code>null</code>
User lastname accessor> Set in constructor from `data.lastname`

**Kind**: instance property of [<code>User</code>](#User)  
**Default**: <code>&quot;null&quot;</code>  
**Category**: properties  
<a name="User+email"></a>

### user.email : <code>string</code>
User email accessor> Set in constructor from `data.email`

**Kind**: instance property of [<code>User</code>](#User)  
**Default**: <code>&quot;undefined&quot;</code>  
**Category**: properties  
<a name="User+thumbnailFilename"></a>

### user.thumbnailFilename : <code>string</code> \| <code>null</code>
User thumbnail filename accessor> Set in constructor from `data.thumbnail`

**Kind**: instance property of [<code>User</code>](#User)  
**Default**: <code>&quot;null&quot;</code>  
**Category**: properties  
<a name="User+createdAt"></a>

### user.createdAt : <code>Date</code>
User creation date accessor> Set in constructor from `data.created_at`

**Kind**: instance property of [<code>User</code>](#User)  
**Default**: <code>undefined</code>  
**Category**: properties  
<a name="User+updatedAt"></a>

### user.updatedAt : <code>Date</code> \| <code>null</code>
User last update date accessor> Set in constructor from `data.created_at`

**Kind**: instance property of [<code>User</code>](#User)  
**Default**: <code>null</code>  
**Category**: properties  
<a name="User+emailVerifiedAt"></a>

### user.emailVerifiedAt : <code>Date</code> \| <code>null</code>
User email verification date accessor> Set in constructor from `data.email_verified_at`

**Kind**: instance property of [<code>User</code>](#User)  
**Default**: <code>null</code>  
**Category**: properties  
<a name="User+lastLogin"></a>

### user.lastLogin : <code>Date</code> \| <code>null</code>
User last login date accessor> Set in constructor from `data.last_login`

**Kind**: instance property of [<code>User</code>](#User)  
**Default**: <code>null</code>  
**Category**: properties  
<a name="User+thumbnailURL"></a>

### user.thumbnailURL : <code>string</code> \| <code>null</code>
User thumbnail URL accessor

**Kind**: instance property of [<code>User</code>](#User)  
**Category**: properties  
**Read only**: true  
<a name="User+thumbnailBase64"></a>

### user.thumbnailBase64 : <code>string</code> \| <code>null</code>
User base64 thumbnail data accessor

**Kind**: instance property of [<code>User</code>](#User)  
**Category**: properties  
**Read only**: true  
