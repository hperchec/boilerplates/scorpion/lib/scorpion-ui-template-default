---
title: Router - AdminMiddleware
headline: Router - AdminMiddleware
sidebarTitle: .AdminMiddleware
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# Router - AdminMiddleware

<a name="AdminMiddleware"></a>

## .AdminMiddleware ⇒ <code>void</code>
> [context](../../../).[router](../../).[middlewares](../).[AdminMiddleware](./)See also [vue-router beforeEnter guard documentation](https://v3.router.vuejs.org/guide/advanced/navigation-guards.html#global-before-guards)If the target route has `meta.requiresAdmin` to `true`, check if current user (authenticated user) is admin. Else, redirect to `/404`.

**Kind**: static member  
