---
title: Router - AuthMiddleware
headline: Router - AuthMiddleware
sidebarTitle: .AuthMiddleware
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# Router - AuthMiddleware

<a name="AuthMiddleware"></a>

## .AuthMiddleware ⇒ <code>void</code>
> [context](../../../).[router](../../).[middlewares](../).[AuthMiddleware](./)See also [vue-router beforeEnter guard documentation](https://v3.router.vuejs.org/guide/advanced/navigation-guards.html#global-before-guards)Authentication middleware.Check if route `meta.requiresAuth` is `true` and redirect to `/login` if user is not authenticated.

**Kind**: static member  
