---
title: Router - middlewares
headline: Router - middlewares
sidebarTitle: .middlewares
prev: false # Disable prev link
next: false # Disable prev link
---

# Router - middlewares

<a name="middlewares"></a>

## .middlewares : <code>Object</code>
> [context](../../).[router](../).[middlewares](./)::: tip INFOPlease check the [routing](../../../../routing) documentation.:::

**Kind**: static member  

* [.middlewares](#middlewares) : <code>Object</code>
    * [.AuthMiddleware()](#middlewares.AuthMiddleware) : <code>function</code>
    * [.AdminMiddleware()](#middlewares.AdminMiddleware) : <code>function</code>

<a name="middlewares.AuthMiddleware"></a>

### middlewares.AuthMiddleware() : <code>function</code>
**Kind**: static method of [<code>middlewares</code>](#middlewares)  
**See**: [AuthMiddleware](./AuthMiddleware)  
<a name="middlewares.AdminMiddleware"></a>

### middlewares.AdminMiddleware() : <code>function</code>
**Kind**: static method of [<code>middlewares</code>](#middlewares)  
**See**: [AdminMiddleware](./AdminMiddleware)  
