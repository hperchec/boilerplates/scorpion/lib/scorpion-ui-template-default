---
title: Router - routes
headline: Router - routes
sidebarTitle: .routes
prev: false # Disable prev link
next: false # Disable prev link
---

# Router - routes

<a name="routes"></a>

## .routes : <code>Object</code>
> [context](../).[router](./).[routes](#)

**Kind**: static member  
<a name="routes.public"></a>

### routes.public : <code>Object</code>
Public routes

**Kind**: static property of [<code>routes</code>](#routes)  
**See**: [public](./public)  
