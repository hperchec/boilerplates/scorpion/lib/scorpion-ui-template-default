---
title: Router - 'private' routes
headline: Router - 'private' routes
sidebarTitle: .private
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# Router - 'private' routes

<a name="private"></a>

## .private : <code>Object</code>
> [context](../../).[router](../).[routes](./).[private](#)

**Kind**: static member  

* [.private](#private) : <code>Object</code>
    * [.Settings](#private.Settings) : <code>Object</code>
    * [.SettingsEditAccount](#private.SettingsEditAccount) : <code>Object</code>
    * [.SettingsVerifyEmail](#private.SettingsVerifyEmail) : <code>Object</code>

<a name="private.Settings"></a>

### private.Settings : <code>Object</code>
Settings route

**Kind**: static property of [<code>private</code>](#private)  
**See**: [Settings](./Settings)  
<a name="private.SettingsEditAccount"></a>

### private.SettingsEditAccount : <code>Object</code>
SettingsEditAccount route

**Kind**: static property of [<code>private</code>](#private)  
**See**: [SettingsEditAccount](./SettingsEditAccount)  
<a name="private.SettingsVerifyEmail"></a>

### private.SettingsVerifyEmail : <code>Object</code>
SettingsVerifyEmail route

**Kind**: static property of [<code>private</code>](#private)  
**See**: [SettingsVerifyEmail](./SettingsVerifyEmail)  
