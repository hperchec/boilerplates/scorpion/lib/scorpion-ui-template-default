---
title: Settings route
headline: Settings route
sidebarTitle: .Settings
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# Settings route

<a name="Settings"></a>

## .Settings : <code>PrivateRoute</code>
> [context](../../../).[router](../../).[routes](../).[private](./).[Settings](#)- **Name**: Settings- **Path**: `/settings`- **Params**: *none*- **Query**: *none*

**Kind**: static member  

* [.Settings](#Settings) : <code>PrivateRoute</code>
    * [.path](#Settings.path) : <code>string</code>
    * [.name](#Settings.name) : <code>string</code>
    * [.component](#Settings.component) : <code>Object</code>
    * [.meta](#Settings.meta) : <code>Object</code>
        * [.breadcrumb](#Settings.meta.breadcrumb) : <code>Object</code>
            * [.schema](#Settings.meta.breadcrumb.schema)
            * [.title()](#Settings.meta.breadcrumb.title) ⇒ <code>string</code>
        * [.pageMeta](#Settings.meta.pageMeta) ⇒ <code>string</code>

<a name="Settings.path"></a>

### Settings.path : <code>string</code>
Route path

**Kind**: static property of [<code>Settings</code>](#Settings)  
**Default**: <code>&quot;/settings&quot;</code>  
<a name="Settings.name"></a>

### Settings.name : <code>string</code>
Route name

**Kind**: static property of [<code>Settings</code>](#Settings)  
**Default**: <code>&quot;Settings&quot;</code>  
<a name="Settings.component"></a>

### Settings.component : <code>Object</code>
Route component

**Kind**: static property of [<code>Settings</code>](#Settings)  
**Default**: <code>Core.components.private.views.Settings.Index</code>  
<a name="Settings.meta"></a>

### Settings.meta : <code>Object</code>
Route meta

**Kind**: static property of [<code>Settings</code>](#Settings)  

* [.meta](#Settings.meta) : <code>Object</code>
    * [.breadcrumb](#Settings.meta.breadcrumb) : <code>Object</code>
        * [.schema](#Settings.meta.breadcrumb.schema)
        * [.title()](#Settings.meta.breadcrumb.title) ⇒ <code>string</code>
    * [.pageMeta](#Settings.meta.pageMeta) ⇒ <code>string</code>

<a name="Settings.meta.breadcrumb"></a>

#### meta.breadcrumb : <code>Object</code>
Breadcrumb options

**Kind**: static property of [<code>meta</code>](#Settings.meta)  

* [.breadcrumb](#Settings.meta.breadcrumb) : <code>Object</code>
    * [.schema](#Settings.meta.breadcrumb.schema)
    * [.title()](#Settings.meta.breadcrumb.title) ⇒ <code>string</code>

<a name="Settings.meta.breadcrumb.schema"></a>

##### breadcrumb.schema
**Kind**: static property of [<code>breadcrumb</code>](#Settings.meta.breadcrumb)  
**Default**: <code>[&#x27;&lt;this&gt;&#x27;]</code>  
<a name="Settings.meta.breadcrumb.title"></a>

##### breadcrumb.title() ⇒ <code>string</code>
Return translated Settings view title (translate: `views.Settings.Index.title`)

**Kind**: static method of [<code>breadcrumb</code>](#Settings.meta.breadcrumb)  
<a name="Settings.meta.pageMeta"></a>

#### meta.pageMeta ⇒ <code>string</code>
Return translated Settings view title (translate: `views.Settings.Index.title`)

**Kind**: static property of [<code>meta</code>](#Settings.meta)  
