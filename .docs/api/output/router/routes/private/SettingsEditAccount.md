---
title: SettingsEditAccount route
headline: SettingsEditAccount route
sidebarTitle: .SettingsEditAccount
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# SettingsEditAccount route

<a name="SettingsEditAccount"></a>

## .SettingsEditAccount : <code>PrivateRoute</code>
> [context](../../../).[router](../../).[routes](../).[private](./).[SettingsEditAccount](#)- **Name**: SettingsEditAccount- **Path**: `/settings/edit-account`- **Params**: *none*- **Query**: *none*

**Kind**: static member  

* [.SettingsEditAccount](#SettingsEditAccount) : <code>PrivateRoute</code>
    * [.path](#SettingsEditAccount.path) : <code>string</code>
    * [.name](#SettingsEditAccount.name) : <code>string</code>
    * [.component](#SettingsEditAccount.component) : <code>Object</code>
    * [.meta](#SettingsEditAccount.meta) : <code>Object</code>
        * [.breadcrumb](#SettingsEditAccount.meta.breadcrumb) : <code>Object</code>
            * [.schema](#SettingsEditAccount.meta.breadcrumb.schema)
            * [.title()](#SettingsEditAccount.meta.breadcrumb.title) ⇒ <code>string</code>
        * [.pageMeta](#SettingsEditAccount.meta.pageMeta) ⇒ <code>string</code>

<a name="SettingsEditAccount.path"></a>

### SettingsEditAccount.path : <code>string</code>
Route path

**Kind**: static property of [<code>SettingsEditAccount</code>](#SettingsEditAccount)  
**Default**: <code>&quot;/settings/edit-account&quot;</code>  
<a name="SettingsEditAccount.name"></a>

### SettingsEditAccount.name : <code>string</code>
Route name

**Kind**: static property of [<code>SettingsEditAccount</code>](#SettingsEditAccount)  
**Default**: <code>&quot;SettingsEditAccount&quot;</code>  
<a name="SettingsEditAccount.component"></a>

### SettingsEditAccount.component : <code>Object</code>
Route component

**Kind**: static property of [<code>SettingsEditAccount</code>](#SettingsEditAccount)  
**Default**: <code>Core.components.private.views.Settings.EditAccount.Index</code>  
<a name="SettingsEditAccount.meta"></a>

### SettingsEditAccount.meta : <code>Object</code>
Route meta

**Kind**: static property of [<code>SettingsEditAccount</code>](#SettingsEditAccount)  

* [.meta](#SettingsEditAccount.meta) : <code>Object</code>
    * [.breadcrumb](#SettingsEditAccount.meta.breadcrumb) : <code>Object</code>
        * [.schema](#SettingsEditAccount.meta.breadcrumb.schema)
        * [.title()](#SettingsEditAccount.meta.breadcrumb.title) ⇒ <code>string</code>
    * [.pageMeta](#SettingsEditAccount.meta.pageMeta) ⇒ <code>string</code>

<a name="SettingsEditAccount.meta.breadcrumb"></a>

#### meta.breadcrumb : <code>Object</code>
Breadcrumb options

**Kind**: static property of [<code>meta</code>](#SettingsEditAccount.meta)  

* [.breadcrumb](#SettingsEditAccount.meta.breadcrumb) : <code>Object</code>
    * [.schema](#SettingsEditAccount.meta.breadcrumb.schema)
    * [.title()](#SettingsEditAccount.meta.breadcrumb.title) ⇒ <code>string</code>

<a name="SettingsEditAccount.meta.breadcrumb.schema"></a>

##### breadcrumb.schema
**Kind**: static property of [<code>breadcrumb</code>](#SettingsEditAccount.meta.breadcrumb)  
**Default**: <code>[&#x27;Settings&#x27;, &#x27;&lt;this&gt;&#x27;]</code>  
<a name="SettingsEditAccount.meta.breadcrumb.title"></a>

##### breadcrumb.title() ⇒ <code>string</code>
Return translated SettingsEditAccount view title (translate: `views.Settings.EditAccount.Index.title`)

**Kind**: static method of [<code>breadcrumb</code>](#SettingsEditAccount.meta.breadcrumb)  
<a name="SettingsEditAccount.meta.pageMeta"></a>

#### meta.pageMeta ⇒ <code>string</code>
Return translated SettingsEditAccount view title (translate: `views.Settings.EditAccount.Index.title`)

**Kind**: static property of [<code>meta</code>](#SettingsEditAccount.meta)  
