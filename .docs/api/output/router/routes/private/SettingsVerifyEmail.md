---
title: SettingsVerifyEmail route
headline: SettingsVerifyEmail route
sidebarTitle: .SettingsVerifyEmail
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# SettingsVerifyEmail route

<a name="SettingsVerifyEmail"></a>

## .SettingsVerifyEmail : <code>PrivateRoute</code>
> [context](../../../).[router](../../).[routes](../).[private](./).[SettingsVerifyEmail](#)- **Name**: SettingsVerifyEmail- **Path**: `/settings/email/verify/:hash`- **Params**:  - `hash {String}`: The temporary hash- **Query**: *none*

**Kind**: static member  

* [.SettingsVerifyEmail](#SettingsVerifyEmail) : <code>PrivateRoute</code>
    * [.path](#SettingsVerifyEmail.path) : <code>string</code>
    * [.name](#SettingsVerifyEmail.name) : <code>string</code>
    * [.component](#SettingsVerifyEmail.component) : <code>Object</code>
    * [.meta](#SettingsVerifyEmail.meta) : <code>Object</code>
        * [.breadcrumb](#SettingsVerifyEmail.meta.breadcrumb) : <code>Object</code>
            * [.schema](#SettingsVerifyEmail.meta.breadcrumb.schema)
            * [.title()](#SettingsVerifyEmail.meta.breadcrumb.title) ⇒ <code>string</code>
        * [.pageMeta](#SettingsVerifyEmail.meta.pageMeta) ⇒ <code>string</code>

<a name="SettingsVerifyEmail.path"></a>

### SettingsVerifyEmail.path : <code>string</code>
Route path

**Kind**: static property of [<code>SettingsVerifyEmail</code>](#SettingsVerifyEmail)  
**Default**: <code>&quot;/settings/email/verify/:hash&quot;</code>  
<a name="SettingsVerifyEmail.name"></a>

### SettingsVerifyEmail.name : <code>string</code>
Route name

**Kind**: static property of [<code>SettingsVerifyEmail</code>](#SettingsVerifyEmail)  
**Default**: <code>&quot;SettingsVerifyEmail&quot;</code>  
<a name="SettingsVerifyEmail.component"></a>

### SettingsVerifyEmail.component : <code>Object</code>
Route component

**Kind**: static property of [<code>SettingsVerifyEmail</code>](#SettingsVerifyEmail)  
**Default**: <code>Core.components.private.views.Settings.VerifyEmail.Index</code>  
<a name="SettingsVerifyEmail.meta"></a>

### SettingsVerifyEmail.meta : <code>Object</code>
Route meta

**Kind**: static property of [<code>SettingsVerifyEmail</code>](#SettingsVerifyEmail)  

* [.meta](#SettingsVerifyEmail.meta) : <code>Object</code>
    * [.breadcrumb](#SettingsVerifyEmail.meta.breadcrumb) : <code>Object</code>
        * [.schema](#SettingsVerifyEmail.meta.breadcrumb.schema)
        * [.title()](#SettingsVerifyEmail.meta.breadcrumb.title) ⇒ <code>string</code>
    * [.pageMeta](#SettingsVerifyEmail.meta.pageMeta) ⇒ <code>string</code>

<a name="SettingsVerifyEmail.meta.breadcrumb"></a>

#### meta.breadcrumb : <code>Object</code>
Breadcrumb options

**Kind**: static property of [<code>meta</code>](#SettingsVerifyEmail.meta)  

* [.breadcrumb](#SettingsVerifyEmail.meta.breadcrumb) : <code>Object</code>
    * [.schema](#SettingsVerifyEmail.meta.breadcrumb.schema)
    * [.title()](#SettingsVerifyEmail.meta.breadcrumb.title) ⇒ <code>string</code>

<a name="SettingsVerifyEmail.meta.breadcrumb.schema"></a>

##### breadcrumb.schema
**Kind**: static property of [<code>breadcrumb</code>](#SettingsVerifyEmail.meta.breadcrumb)  
**Default**: <code>[&#x27;Settings&#x27;, &#x27;&lt;this&gt;&#x27;]</code>  
<a name="SettingsVerifyEmail.meta.breadcrumb.title"></a>

##### breadcrumb.title() ⇒ <code>string</code>
Return translated SettingsVerifyEmail view title (translate: `views.Settings.VerifyEmail.Index.title`)

**Kind**: static method of [<code>breadcrumb</code>](#SettingsVerifyEmail.meta.breadcrumb)  
<a name="SettingsVerifyEmail.meta.pageMeta"></a>

#### meta.pageMeta ⇒ <code>string</code>
Return translated SettingsVerifyEmail view title (translate: `views.Settings.VerifyEmail.Index.title`)

**Kind**: static property of [<code>meta</code>](#SettingsVerifyEmail.meta)  
