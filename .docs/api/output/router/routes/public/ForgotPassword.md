---
title: ForgotPassword route
headline: ForgotPassword route
sidebarTitle: .ForgotPassword
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# ForgotPassword route

<a name="ForgotPassword"></a>

## .ForgotPassword : <code>PublicRoute</code>
> [context](../../../).[router](../../).[routes](../).[public](./).[ForgotPassword](#)- **Name**: ForgotPassword- **Path**: `/forgot-password`- **Params**: *none*- **Query**: *none*

**Kind**: static member  

* [.ForgotPassword](#ForgotPassword) : <code>PublicRoute</code>
    * [.path](#ForgotPassword.path) : <code>string</code>
    * [.name](#ForgotPassword.name) : <code>string</code>
    * [.meta](#ForgotPassword.meta) : <code>Object</code>
        * [.pageMeta](#ForgotPassword.meta.pageMeta) ⇒ <code>string</code>
    * [.beforeEnter()](#ForgotPassword.beforeEnter) ⇒ <code>void</code>

<a name="ForgotPassword.path"></a>

### ForgotPassword.path : <code>string</code>
Route path

**Kind**: static property of [<code>ForgotPassword</code>](#ForgotPassword)  
**Default**: <code>&quot;/forgot-password&quot;</code>  
<a name="ForgotPassword.name"></a>

### ForgotPassword.name : <code>string</code>
Route name

**Kind**: static property of [<code>ForgotPassword</code>](#ForgotPassword)  
**Default**: <code>&quot;ForgotPassword&quot;</code>  
<a name="ForgotPassword.meta"></a>

### ForgotPassword.meta : <code>Object</code>
Route meta

**Kind**: static property of [<code>ForgotPassword</code>](#ForgotPassword)  
<a name="ForgotPassword.meta.pageMeta"></a>

#### meta.pageMeta ⇒ <code>string</code>
Return translated ForgotPassword view title (translate: `views.ForgotPassword.Index.title`)

**Kind**: static property of [<code>meta</code>](#ForgotPassword.meta)  
<a name="ForgotPassword.beforeEnter"></a>

### ForgotPassword.beforeEnter() ⇒ <code>void</code>
> See also [beforeEnter per-route guard documentation](https://v3.router.vuejs.org/guide/advanced/navigation-guards.html#per-route-guard)Redirect if already authenticated

**Kind**: static method of [<code>ForgotPassword</code>](#ForgotPassword)  
