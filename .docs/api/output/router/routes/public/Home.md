---
title: Home route
headline: Home route
sidebarTitle: .Home
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# Home route

<a name="Home"></a>

## .Home : <code>PublicRoute</code>
> [context](../../../).[router](../../).[routes](../).[public](./).[Home](#)- **Name**: Home- **Path**: `/`- **Params**: *none*- **Query**: *none*

**Kind**: static member  

* [.Home](#Home) : <code>PublicRoute</code>
    * [.path](#Home.path) : <code>string</code>
    * [.name](#Home.name) : <code>string</code>

<a name="Home.path"></a>

### Home.path : <code>string</code>
Route path

**Kind**: static property of [<code>Home</code>](#Home)  
**Default**: <code>&quot;/&quot;</code>  
<a name="Home.name"></a>

### Home.name : <code>string</code>
Route name

**Kind**: static property of [<code>Home</code>](#Home)  
**Default**: <code>&quot;Home&quot;</code>  
