---
title: Login route
headline: Login route
sidebarTitle: .Login
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# Login route

<a name="Login"></a>

## .Login : <code>PublicRoute</code>
> [context](../../../).[router](../../).[routes](../).[public](./).[Login](#)- **Name**: Login- **Path**: `/login`- **Params**: *none*- **Query**: *none*

**Kind**: static member  

* [.Login](#Login) : <code>PublicRoute</code>
    * [.path](#Login.path) : <code>string</code>
    * [.name](#Login.name) : <code>string</code>
    * [.meta](#Login.meta) : <code>Object</code>
        * [.pageMeta](#Login.meta.pageMeta) ⇒ <code>string</code>
    * [.beforeEnter()](#Login.beforeEnter) ⇒ <code>void</code>

<a name="Login.path"></a>

### Login.path : <code>string</code>
Route path

**Kind**: static property of [<code>Login</code>](#Login)  
**Default**: <code>&quot;/login&quot;</code>  
<a name="Login.name"></a>

### Login.name : <code>string</code>
Route name

**Kind**: static property of [<code>Login</code>](#Login)  
**Default**: <code>&quot;Login&quot;</code>  
<a name="Login.meta"></a>

### Login.meta : <code>Object</code>
Route meta

**Kind**: static property of [<code>Login</code>](#Login)  
<a name="Login.meta.pageMeta"></a>

#### meta.pageMeta ⇒ <code>string</code>
Return translated Login view title (translate: `views.Login.Index.title`)

**Kind**: static property of [<code>meta</code>](#Login.meta)  
<a name="Login.beforeEnter"></a>

### Login.beforeEnter() ⇒ <code>void</code>
> See also [beforeEnter per-route guard documentation](https://v3.router.vuejs.org/guide/advanced/navigation-guards.html#per-route-guard)Redirect if already authenticated

**Kind**: static method of [<code>Login</code>](#Login)  
