---
title: NotFound route
headline: NotFound route
sidebarTitle: .NotFound
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# NotFound route

<a name="NotFound"></a>

## .NotFound : <code>PublicRoute</code>
> [context](../../../).[router](../../).[routes](../).[public](./).[NotFound](#)- **Name**: NotFound- **Path**: `/404`- **Params**: *none*- **Query**: *none*

**Kind**: static member  

* [.NotFound](#NotFound) : <code>PublicRoute</code>
    * [.path](#NotFound.path) : <code>string</code>
    * [.name](#NotFound.name) : <code>string</code>
    * [.meta](#NotFound.meta) : <code>Object</code>
        * [.pageMeta](#NotFound.meta.pageMeta) ⇒ <code>string</code>

<a name="NotFound.path"></a>

### NotFound.path : <code>string</code>
Route path

**Kind**: static property of [<code>NotFound</code>](#NotFound)  
**Default**: <code>&quot;/404&quot;</code>  
<a name="NotFound.name"></a>

### NotFound.name : <code>string</code>
Route name

**Kind**: static property of [<code>NotFound</code>](#NotFound)  
**Default**: <code>&quot;NotFound&quot;</code>  
<a name="NotFound.meta"></a>

### NotFound.meta : <code>Object</code>
Route meta

**Kind**: static property of [<code>NotFound</code>](#NotFound)  
<a name="NotFound.meta.pageMeta"></a>

#### meta.pageMeta ⇒ <code>string</code>
Return translated NotFound view title (translate: `views.NotFound.Index.title`)

**Kind**: static property of [<code>meta</code>](#NotFound.meta)  
