---
title: Router - 'public' routes
headline: Router - 'public' routes
sidebarTitle: .public
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# Router - 'public' routes

<a name="public"></a>

## .public : <code>Object</code>
> [context](../../).[router](../).[routes](./).[public](#)

**Kind**: static member  

* [.public](#public) : <code>Object</code>
    * [.Home](#public.Home) : <code>Object</code>
    * [.Login](#public.Login) : <code>Object</code>
    * [.SignUp](#public.SignUp) : <code>Object</code>
    * [.ForgotPassword](#public.ForgotPassword) : <code>Object</code>
    * [.ResetPassword](#public.ResetPassword) : <code>Object</code>
    * [.NotFound](#public.NotFound) : <code>Object</code>

<a name="public.Home"></a>

### public.Home : <code>Object</code>
Home route

**Kind**: static property of [<code>public</code>](#public)  
**See**: [Home](./Home)  
<a name="public.Login"></a>

### public.Login : <code>Object</code>
Login route

**Kind**: static property of [<code>public</code>](#public)  
**See**: [Login](./Login)  
<a name="public.SignUp"></a>

### public.SignUp : <code>Object</code>
SignUp route

**Kind**: static property of [<code>public</code>](#public)  
**See**: [SignUp](./SignUp)  
<a name="public.ForgotPassword"></a>

### public.ForgotPassword : <code>Object</code>
ForgotPassword route

**Kind**: static property of [<code>public</code>](#public)  
**See**: [ForgotPassword](./ForgotPassword)  
<a name="public.ResetPassword"></a>

### public.ResetPassword : <code>Object</code>
ResetPassword route

**Kind**: static property of [<code>public</code>](#public)  
**See**: [ResetPassword](./ResetPassword)  
<a name="public.NotFound"></a>

### public.NotFound : <code>Object</code>
NotFound route

**Kind**: static property of [<code>public</code>](#public)  
**See**: [NotFound](./NotFound)  
