---
title: ResetPassword route
headline: ResetPassword route
sidebarTitle: .ResetPassword
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# ResetPassword route

<a name="ResetPassword"></a>

## .ResetPassword : <code>PublicRoute</code>
> [context](../../../).[router](../../).[routes](../).[public](./).[ResetPassword](#)- **Name**: ResetPassword- **Path**: `/reset-password`- **Params**:  - `token {String}`: The temporary token to reset password- **Query**: *none*

**Kind**: static member  

* [.ResetPassword](#ResetPassword) : <code>PublicRoute</code>
    * [.path](#ResetPassword.path) : <code>string</code>
    * [.name](#ResetPassword.name) : <code>string</code>
    * [.meta](#ResetPassword.meta) : <code>Object</code>
        * [.pageMeta](#ResetPassword.meta.pageMeta) ⇒ <code>string</code>
    * [.beforeEnter()](#ResetPassword.beforeEnter) ⇒ <code>void</code>

<a name="ResetPassword.path"></a>

### ResetPassword.path : <code>string</code>
Route path

**Kind**: static property of [<code>ResetPassword</code>](#ResetPassword)  
**Default**: <code>&quot;/reset-password/:token&quot;</code>  
<a name="ResetPassword.name"></a>

### ResetPassword.name : <code>string</code>
Route name

**Kind**: static property of [<code>ResetPassword</code>](#ResetPassword)  
**Default**: <code>&quot;ResetPassword&quot;</code>  
<a name="ResetPassword.meta"></a>

### ResetPassword.meta : <code>Object</code>
Route meta

**Kind**: static property of [<code>ResetPassword</code>](#ResetPassword)  
<a name="ResetPassword.meta.pageMeta"></a>

#### meta.pageMeta ⇒ <code>string</code>
Return translated ResetPassword view title (translate: `views.ResetPassword.Index.title`)

**Kind**: static property of [<code>meta</code>](#ResetPassword.meta)  
<a name="ResetPassword.beforeEnter"></a>

### ResetPassword.beforeEnter() ⇒ <code>void</code>
> See also [beforeEnter per-route guard documentation](https://v3.router.vuejs.org/guide/advanced/navigation-guards.html#per-route-guard)Redirect if already authenticated

**Kind**: static method of [<code>ResetPassword</code>](#ResetPassword)  
