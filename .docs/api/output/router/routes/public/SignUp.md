---
title: SignUp route
headline: SignUp route
sidebarTitle: .SignUp
sidebarDepth: 0 # To disable auto sidebar links
prev: false # Disable prev link
next: false # Disable prev link
---

# SignUp route

<a name="SignUp"></a>

## .SignUp : <code>PublicRoute</code>
> [context](../../../).[router](../../).[routes](../).[public](./).[SignUp](#)- **Name**: SignUp- **Path**: `/sign-up`- **Params**: *none*- **Query**: *none*

**Kind**: static member  

* [.SignUp](#SignUp) : <code>PublicRoute</code>
    * [.path](#SignUp.path) : <code>string</code>
    * [.name](#SignUp.name) : <code>string</code>
    * [.meta](#SignUp.meta) : <code>Object</code>
        * [.pageMeta](#SignUp.meta.pageMeta) ⇒ <code>string</code>
    * [.beforeEnter()](#SignUp.beforeEnter) ⇒ <code>void</code>

<a name="SignUp.path"></a>

### SignUp.path : <code>string</code>
Route path

**Kind**: static property of [<code>SignUp</code>](#SignUp)  
**Default**: <code>&quot;/sign-up&quot;</code>  
<a name="SignUp.name"></a>

### SignUp.name : <code>string</code>
Route name

**Kind**: static property of [<code>SignUp</code>](#SignUp)  
**Default**: <code>&quot;SignUp&quot;</code>  
<a name="SignUp.meta"></a>

### SignUp.meta : <code>Object</code>
Route meta

**Kind**: static property of [<code>SignUp</code>](#SignUp)  
<a name="SignUp.meta.pageMeta"></a>

#### meta.pageMeta ⇒ <code>string</code>
Return translated SignUp view title (translate: `views.SignUp.Index.title`)

**Kind**: static property of [<code>meta</code>](#SignUp.meta)  
<a name="SignUp.beforeEnter"></a>

### SignUp.beforeEnter() ⇒ <code>void</code>
> See also [beforeEnter per-route guard documentation](https://v3.router.vuejs.org/guide/advanced/navigation-guards.html#per-route-guard)Redirect if already authenticated

**Kind**: static method of [<code>SignUp</code>](#SignUp)  
