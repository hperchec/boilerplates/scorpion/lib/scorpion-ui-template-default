module.exports = [
  {
    path: '/guide/ui/api/components/',
    children: [
      {
        path: '/guide/ui/api/components/App',
        children: [],
        title: 'App'
      },
      {
        path: '/guide/ui/api/components/commons/',
        children: [
          {
            path: '/guide/ui/api/components/commons/breadcrumb/Breadcrumb',
            children: [
              {
                path: '/guide/ui/api/components/commons/breadcrumb/Breadcrumb',
                children: [],
                title: 'Breadcrumb'
              }
            ],
            title: 'breadcrumb'
          },
          {
            path: '/guide/ui/api/components/commons/buttons/Button',
            children: [
              {
                path: '/guide/ui/api/components/commons/buttons/Button',
                children: [],
                title: 'Button'
              }
            ],
            title: 'buttons'
          },
          {
            path: '/guide/ui/api/components/commons/dropdown/LangDropdown',
            children: [
              {
                path: '/guide/ui/api/components/commons/dropdown/LangDropdown',
                children: [],
                title: 'LangDropdown'
              },
              {
                path: '/guide/ui/api/components/commons/dropdown/ThemeDropdown',
                children: [],
                title: 'ThemeDropdown'
              }
            ],
            title: 'dropdown'
          },
          {
            path: '/guide/ui/api/components/commons/form/BaseForm',
            children: [
              {
                path: '/guide/ui/api/components/commons/form/BaseForm',
                children: [],
                title: 'BaseForm'
              },
              {
                path: '/guide/ui/api/components/commons/form/BaseInput',
                children: [],
                title: 'BaseInput'
              },
              {
                path: '/guide/ui/api/components/commons/form/Input',
                children: [],
                title: 'Input'
              },
              {
                path: '/guide/ui/api/components/commons/form/InputFile',
                children: [],
                title: 'InputFile'
              },
              {
                path: '/guide/ui/api/components/commons/form/InputPassword',
                children: [],
                title: 'InputPassword'
              },
              {
                path: '/guide/ui/api/components/commons/form/RichSelect',
                children: [],
                title: 'RichSelect'
              },
              {
                path: '/guide/ui/api/components/commons/form/Select',
                children: [],
                title: 'Select'
              }
            ],
            title: 'form'
          },
          {
            path: '/guide/ui/api/components/commons/icons/BaseIcon',
            children: [
              {
                path: '/guide/ui/api/components/commons/icons/BaseIcon',
                children: [],
                title: 'BaseIcon'
              },
              {
                path: '/guide/ui/api/components/commons/icons/I18nFlag',
                children: [],
                title: 'I18nFlag'
              },
              {
                path: '/guide/ui/api/components/commons/icons/LoadingIcon-old',
                children: [],
                title: 'LoadingIcon-old'
              },
              {
                path: '/guide/ui/api/components/commons/icons/LoadingIcon',
                children: [],
                title: 'LoadingIcon'
              },
              {
                path: '/guide/ui/api/components/commons/icons/MenuIcon',
                children: [],
                title: 'MenuIcon'
              }
            ],
            title: 'icons'
          },
          {
            path: '/guide/ui/api/components/commons/layouts/',
            children: [
              {
                path: '/guide/ui/api/components/commons/layouts/Layout',
                children: [],
                title: 'Layout'
              }
            ],
            title: 'layouts'
          },
          {
            path: '/guide/ui/api/components/commons/links/Link',
            children: [
              {
                path: '/guide/ui/api/components/commons/links/Link',
                children: [],
                title: 'Link'
              }
            ],
            title: 'links'
          },
          {
            path: '/guide/ui/api/components/commons/main-loading/MainLoading',
            children: [
              {
                path: '/guide/ui/api/components/commons/main-loading/MainLoading',
                children: [],
                title: 'MainLoading'
              }
            ],
            title: 'main-loading'
          },
          {
            path: '/guide/ui/api/components/commons/modals/BaseModal',
            children: [
              {
                path: '/guide/ui/api/components/commons/modals/BaseModal',
                children: [],
                title: 'BaseModal'
              },
              {
                path: '/guide/ui/api/components/commons/modals/EmailSentModal',
                children: [],
                title: 'EmailSentModal'
              },
              {
                path: '/guide/ui/api/components/commons/modals/GlobalErrorModal',
                children: [],
                title: 'GlobalErrorModal'
              },
              {
                path: '/guide/ui/api/components/commons/modals/PasswordGuardModal',
                children: [],
                title: 'PasswordGuardModal'
              }
            ],
            title: 'modals'
          },
          {
            path: '/guide/ui/api/components/commons/Page',
            children: [],
            title: 'Page'
          },
          {
            path: '/guide/ui/api/components/commons/tab-view/TabView',
            children: [
              {
                path: '/guide/ui/api/components/commons/tab-view/TabView',
                children: [],
                title: 'TabView'
              }
            ],
            title: 'tab-view'
          },
          {
            path: '/guide/ui/api/components/commons/thumbnail/AnonymousThumbnail',
            children: [
              {
                path: '/guide/ui/api/components/commons/thumbnail/AnonymousThumbnail',
                children: [],
                title: 'AnonymousThumbnail'
              },
              {
                path: '/guide/ui/api/components/commons/thumbnail/UserThumbnail',
                children: [],
                title: 'UserThumbnail'
              }
            ],
            title: 'thumbnail'
          }
        ],
        title: 'commons'
      },
      {
        path: '/guide/ui/api/components/public/',
        children: [
          {
            path: '/guide/ui/api/components/public/views/',
            children: [
              {
                path: '/guide/ui/api/components/public/views/forgot-password/',
                children: [
                  {
                    path: '/guide/ui/api/components/public/views/forgot-password/ForgotPasswordForm',
                    children: [],
                    title: 'ForgotPasswordForm'
                  },
                  {
                    path: '/guide/ui/api/components/public/views/forgot-password/Index',
                    children: [],
                    title: 'Index'
                  }
                ],
                title: 'forgot-password'
              },
              {
                path: '/guide/ui/api/components/public/views/home/',
                children: [
                  {
                    path: '/guide/ui/api/components/public/views/home/Index',
                    children: [],
                    title: 'Index'
                  }
                ],
                title: 'home'
              },
              {
                path: '/guide/ui/api/components/public/views/login/',
                children: [
                  {
                    path: '/guide/ui/api/components/public/views/login/Index',
                    children: [],
                    title: 'Index'
                  },
                  {
                    path: '/guide/ui/api/components/public/views/login/LoginForm',
                    children: [],
                    title: 'LoginForm'
                  }
                ],
                title: 'login'
              },
              {
                path: '/guide/ui/api/components/public/views/not-found/',
                children: [
                  {
                    path: '/guide/ui/api/components/public/views/not-found/Index',
                    children: [],
                    title: 'Index'
                  }
                ],
                title: 'not-found'
              },
              {
                path: '/guide/ui/api/components/public/views/reset-password/',
                children: [
                  {
                    path: '/guide/ui/api/components/public/views/reset-password/Index',
                    children: [],
                    title: 'Index'
                  }
                ],
                title: 'reset-password'
              },
              {
                path: '/guide/ui/api/components/public/views/sign-up/',
                children: [
                  {
                    path: '/guide/ui/api/components/public/views/sign-up/Index',
                    children: [],
                    title: 'Index'
                  }
                ],
                title: 'sign-up'
              }
            ],
            title: 'views'
          }
        ],
        title: 'public'
      }
    ],
    title: 'components'
  },
  {
    children: [
      {
        path: '/guide/ui/api/config/config',
        children: [],
        title: 'config'
      }
    ],
    title: 'config'
  },
  {
    path: '/guide/ui/api/directives/',
    children: [
      {
        path: '/guide/ui/api/directives/vue-ripple-directive/',
        children: [
          {
            path: '/guide/ui/api/directives/vue-ripple-directive/create-directive',
            children: [],
            title: 'create-directive'
          }
        ],
        title: 'vue-ripple-directive'
      }
    ],
    title: 'directives'
  },
  {
    path: '/guide/ui/api/i18n/',
    children: [
      {
        path: '/guide/ui/api/i18n/messages/',
        children: [
          {
            path: '/guide/ui/api/i18n/messages/en/',
            children: [],
            title: 'en'
          },
          {
            path: '/guide/ui/api/i18n/messages/fr/',
            children: [],
            title: 'fr'
          }
        ],
        title: 'messages'
      }
    ],
    title: 'i18n'
  },
  {
    path: '/guide/ui/api/models/',
    children: [
      {
        path: '/guide/ui/api/models/User',
        children: [],
        title: '.User'
      }
    ],
    title: '.models'
  },
  {
    path: '/guide/ui/api/plugins/',
    children: [
      {
        path: '/guide/ui/api/plugins/vue-tailwind/',
        children: [
          {
            path: '/guide/ui/api/plugins/vue-tailwind/create-plugin',
            children: [],
            title: 'create-plugin'
          },
          {
            path: '/guide/ui/api/plugins/vue-tailwind/options/',
            children: [
              {
                path: '/guide/ui/api/plugins/vue-tailwind/options/TAlert',
                children: [],
                title: 'TAlert'
              },
              {
                path: '/guide/ui/api/plugins/vue-tailwind/options/TButton',
                children: [],
                title: 'TButton'
              },
              {
                path: '/guide/ui/api/plugins/vue-tailwind/options/TDropdown',
                children: [],
                title: 'TDropdown'
              },
              {
                path: '/guide/ui/api/plugins/vue-tailwind/options/TDropdownButton',
                children: [],
                title: 'TDropdownButton'
              },
              {
                path: '/guide/ui/api/plugins/vue-tailwind/options/TInput',
                children: [],
                title: 'TInput'
              },
              {
                path: '/guide/ui/api/plugins/vue-tailwind/options/TMenuButton',
                children: [],
                title: 'TMenuButton'
              },
              {
                path: '/guide/ui/api/plugins/vue-tailwind/options/TModal',
                children: [],
                title: 'TModal'
              },
              {
                path: '/guide/ui/api/plugins/vue-tailwind/options/TRadio',
                children: [],
                title: 'TRadio'
              },
              {
                path: '/guide/ui/api/plugins/vue-tailwind/options/TRichSelect',
                children: [],
                title: 'TRichSelect'
              },
              {
                path: '/guide/ui/api/plugins/vue-tailwind/options/TTextArea',
                children: [],
                title: 'TTextArea'
              },
              {
                path: '/guide/ui/api/plugins/vue-tailwind/options/TToggle',
                children: [],
                title: 'TToggle'
              }
            ],
            title: 'options'
          }
        ],
        title: 'vue-tailwind'
      }
    ],
    title: 'plugins'
  },
  {
    path: '/guide/ui/api/root-instance/',
    children: [
      {
        path: '/guide/ui/api/root-instance/options',
        children: [],
        title: 'options'
      },
      {
        path: '/guide/ui/api/root-instance/remote/',
        children: [
          {
            path: '/guide/ui/api/root-instance/remote/auth',
            children: [],
            title: 'auth'
          },
          {
            path: '/guide/ui/api/root-instance/remote/ui',
            children: [],
            title: 'ui'
          }
        ],
        title: 'remote'
      },
      {
        path: '/guide/ui/api/root-instance/root-mixin',
        children: [],
        title: 'root-mixin'
      }
    ],
    title: 'root-instance'
  },
  {
    path: '/guide/ui/api/router/',
    children: [
      {
        path: '/guide/ui/api/router/middlewares/',
        children: [
          {
            path: '/guide/ui/api/router/middlewares/AdminMiddleware',
            children: [],
            title: '.AdminMiddleware'
          },
          {
            path: '/guide/ui/api/router/middlewares/AuthMiddleware',
            children: [],
            title: '.AuthMiddleware'
          }
        ],
        title: '.middlewares'
      },
      {
        path: '/guide/ui/api/router/routes/',
        children: [
          {
            path: '/guide/ui/api/router/routes/private/',
            children: [
              {
                path: '/guide/ui/api/router/routes/private/Settings',
                children: [],
                title: '.Settings'
              },
              {
                path: '/guide/ui/api/router/routes/private/SettingsEditAccount',
                children: [],
                title: '.SettingsEditAccount'
              },
              {
                path: '/guide/ui/api/router/routes/private/SettingsVerifyEmail',
                children: [],
                title: '.SettingsVerifyEmail'
              }
            ],
            title: '.private'
          },
          {
            path: '/guide/ui/api/router/routes/public/',
            children: [
              {
                path: '/guide/ui/api/router/routes/public/ForgotPassword',
                children: [],
                title: '.ForgotPassword'
              },
              {
                path: '/guide/ui/api/router/routes/public/Home',
                children: [],
                title: '.Home'
              },
              {
                path: '/guide/ui/api/router/routes/public/Login',
                children: [],
                title: '.Login'
              },
              {
                path: '/guide/ui/api/router/routes/public/NotFound',
                children: [],
                title: '.NotFound'
              },
              {
                path: '/guide/ui/api/router/routes/public/ResetPassword',
                children: [],
                title: '.ResetPassword'
              },
              {
                path: '/guide/ui/api/router/routes/public/SignUp',
                children: [],
                title: '.SignUp'
              }
            ],
            title: '.public'
          }
        ],
        title: '.routes'
      }
    ],
    title: 'router'
  },
  {
    path: '/guide/ui/api/store/',
    children: [
      {
        path: '/guide/ui/api/store/modules/',
        children: [
          {
            path: '/guide/ui/api/store/modules/resources/',
            children: [
              {
                path: '/guide/ui/api/store/modules/resources/modules/',
                children: [
                  {
                    path: '/guide/ui/api/store/modules/resources/modules/users/',
                    children: [
                      {
                        path: '/guide/ui/api/store/modules/resources/modules/users/options',
                        children: [],
                        title: 'options'
                      }
                    ],
                    title: 'users'
                  }
                ],
                title: 'modules'
              }
            ],
            title: 'resources'
          },
          {
            path: '/guide/ui/api/store/modules/system/',
            children: [
              {
                path: '/guide/ui/api/store/modules/system/modules/',
                children: [
                  {
                    path: '/guide/ui/api/store/modules/system/modules/auth/',
                    children: [],
                    title: 'auth'
                  },
                  {
                    path: '/guide/ui/api/store/modules/system/modules/modals/',
                    children: [],
                    title: 'modals'
                  }
                ],
                title: 'modules'
              }
            ],
            title: 'system'
          }
        ],
        title: 'modules'
      },
      {
        path: '/guide/ui/api/store/plugins/',
        children: [
          {
            path: '/guide/ui/api/store/plugins/display-modal-on-system-error',
            children: [],
            title: 'display-modal-on-system-error'
          }
        ],
        title: 'plugins'
      }
    ],
    title: 'store'
  },
  {
    children: [],
    title: 'style'
  }
]
