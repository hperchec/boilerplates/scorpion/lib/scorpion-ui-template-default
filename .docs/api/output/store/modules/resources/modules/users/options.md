---
title: options
---

# options

## Members

<dl>
<dt><a href="#state">state</a> : <code>Object</code></dt>
<dd><p>Store module state</p>
</dd>
<dt><a href="#getters">getters</a> : <code>Object</code></dt>
<dd><p>Store module getters</p>
</dd>
<dt><a href="#actions">actions</a> : <code>Object</code></dt>
<dd><p>Store module actions</p>
</dd>
<dt><a href="#mutations">mutations</a> : <code>Object</code></dt>
<dd><p>Store module mutations</p>
</dd>
</dl>

<a name="state"></a>

## state : <code>Object</code>
Store module state

**Kind**: global variable  
**Read only**: true  
<a name="getters"></a>

## getters : <code>Object</code>
Store module getters

**Kind**: global variable  
**Read only**: true  
<a name="actions"></a>

## actions : <code>Object</code>
Store module actions

**Kind**: global variable  
**Access**: protected  

* [actions](#actions) : <code>Object</code>
    * [.updateThumbnail(context, payload)](#actions.updateThumbnail) ⇒ <code>boolean</code> \| <code>Error</code>
    * [.verifyEmail(context, payload)](#actions.verifyEmail) ⇒ <code>boolean</code> \| <code>Error</code>
    * [.resendEmailVerificationLink(context, id)](#actions.resendEmailVerificationLink) ⇒ <code>boolean</code> \| <code>Error</code>

<a name="actions.updateThumbnail"></a>

### actions.updateThumbnail(context, payload) ⇒ <code>boolean</code> \| <code>Error</code>
Update user thumbnail method.

**Kind**: static method of [<code>actions</code>](#actions)  
**Returns**: <code>boolean</code> \| <code>Error</code> - Error or return true  

| Param | Type | Description |
| --- | --- | --- |
| context | <code>Object</code> | vuex context |
| payload | <code>Object</code> | Method payload |
| payload.id | <code>number</code> | User id |
| payload.data | <code>Object</code> | Method payload -> formData |

<a name="actions.verifyEmail"></a>

### actions.verifyEmail(context, payload) ⇒ <code>boolean</code> \| <code>Error</code>
Verify user email method.Request `[POST] /api/users/<userId>/email/verify/<hash>?expires=<expires>&signature=<signature>`

**Kind**: static method of [<code>actions</code>](#actions)  
**Returns**: <code>boolean</code> \| <code>Error</code> - Error or return true  

| Param | Type | Description |
| --- | --- | --- |
| context | <code>Object</code> | vuex context |
| payload | <code>Object</code> | Method payload |
| payload.id | <code>number</code> | User id |
| payload.hash | <code>string</code> | Hash |
| payload.query | <code>Object</code> | Query params |
| payload.query.expires | <code>string</code> | When route expires |
| payload.query.signature | <code>string</code> | The route signature |

<a name="actions.resendEmailVerificationLink"></a>

### actions.resendEmailVerificationLink(context, id) ⇒ <code>boolean</code> \| <code>Error</code>
Resend user email verification link method.Request `[POST] /api/users/<userId>/email/resend-verification-link`

**Kind**: static method of [<code>actions</code>](#actions)  
**Returns**: <code>boolean</code> \| <code>Error</code> - Error or return true  

| Param | Type | Description |
| --- | --- | --- |
| context | <code>Object</code> | vuex context |
| id | <code>number</code> | User id |

<a name="mutations"></a>

## mutations : <code>Object</code>
Store module mutations

**Kind**: global variable  
**Access**: protected  
