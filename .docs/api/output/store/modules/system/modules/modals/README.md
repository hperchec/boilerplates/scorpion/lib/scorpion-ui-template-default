---
title: __index__
---

# __index__

## Members

<dl>
<dt><a href="#state">state</a> : <code>Object</code></dt>
<dd><p>Store module state</p>
</dd>
<dt><a href="#getters">getters</a> : <code>Object</code></dt>
<dd><p>Store module getters</p>
</dd>
<dt><a href="#actions">actions</a> : <code>Object</code></dt>
<dd><p>Store module actions</p>
</dd>
<dt><a href="#mutations">mutations</a> : <code>Object</code></dt>
<dd><p>Store module mutations</p>
</dd>
</dl>

<a name="state"></a>

## state : <code>Object</code>
Store module state

**Kind**: global variable  
**Read only**: true  
**Example**  
```js
// Access to the module stateStore.state.Core.Modals
```

* [state](#state) : <code>Object</code>
    * [.modals](#state.modals) : <code>Object</code>
        * [.EmailSentModal](#state.modals.EmailSentModal) : <code>Object</code>
            * [.show](#state.modals.EmailSentModal.show) : <code>boolean</code>
            * [.props](#state.modals.EmailSentModal.props) : <code>Object</code>
        * [.GlobalErrorModal](#state.modals.GlobalErrorModal) : <code>Object</code>
            * [.show](#state.modals.GlobalErrorModal.show) : <code>boolean</code>
            * [.props](#state.modals.GlobalErrorModal.props) : <code>Object</code>
        * [.HelpLearnMoreModal](#state.modals.HelpLearnMoreModal) : <code>Object</code>
            * [.show](#state.modals.HelpLearnMoreModal.show) : <code>boolean</code>
            * [.props](#state.modals.HelpLearnMoreModal.props) : <code>Object</code>
        * [.NotCompletedProfileModal](#state.modals.NotCompletedProfileModal) : <code>Object</code>
            * [.show](#state.modals.NotCompletedProfileModal.show) : <code>boolean</code>
            * [.props](#state.modals.NotCompletedProfileModal.props) : <code>Object</code>
        * [.PasswordGuardModal](#state.modals.PasswordGuardModal) : <code>Object</code>
            * [.show](#state.modals.PasswordGuardModal.show) : <code>boolean</code>
            * [.props](#state.modals.PasswordGuardModal.props) : <code>Object</code>
        * [.RequiresVerifiedEmailModal](#state.modals.RequiresVerifiedEmailModal) : <code>Object</code>
            * [.show](#state.modals.RequiresVerifiedEmailModal.show) : <code>boolean</code>
            * [.props](#state.modals.RequiresVerifiedEmailModal.props) : <code>Object</code>

<a name="state.modals"></a>

### state.modals : <code>Object</code>
Modals data

**Kind**: static property of [<code>state</code>](#state)  

* [.modals](#state.modals) : <code>Object</code>
    * [.EmailSentModal](#state.modals.EmailSentModal) : <code>Object</code>
        * [.show](#state.modals.EmailSentModal.show) : <code>boolean</code>
        * [.props](#state.modals.EmailSentModal.props) : <code>Object</code>
    * [.GlobalErrorModal](#state.modals.GlobalErrorModal) : <code>Object</code>
        * [.show](#state.modals.GlobalErrorModal.show) : <code>boolean</code>
        * [.props](#state.modals.GlobalErrorModal.props) : <code>Object</code>
    * [.HelpLearnMoreModal](#state.modals.HelpLearnMoreModal) : <code>Object</code>
        * [.show](#state.modals.HelpLearnMoreModal.show) : <code>boolean</code>
        * [.props](#state.modals.HelpLearnMoreModal.props) : <code>Object</code>
    * [.NotCompletedProfileModal](#state.modals.NotCompletedProfileModal) : <code>Object</code>
        * [.show](#state.modals.NotCompletedProfileModal.show) : <code>boolean</code>
        * [.props](#state.modals.NotCompletedProfileModal.props) : <code>Object</code>
    * [.PasswordGuardModal](#state.modals.PasswordGuardModal) : <code>Object</code>
        * [.show](#state.modals.PasswordGuardModal.show) : <code>boolean</code>
        * [.props](#state.modals.PasswordGuardModal.props) : <code>Object</code>
    * [.RequiresVerifiedEmailModal](#state.modals.RequiresVerifiedEmailModal) : <code>Object</code>
        * [.show](#state.modals.RequiresVerifiedEmailModal.show) : <code>boolean</code>
        * [.props](#state.modals.RequiresVerifiedEmailModal.props) : <code>Object</code>

<a name="state.modals.EmailSentModal"></a>

#### modals.EmailSentModal : <code>Object</code>
EmailSentModal

**Kind**: static property of [<code>modals</code>](#state.modals)  

* [.EmailSentModal](#state.modals.EmailSentModal) : <code>Object</code>
    * [.show](#state.modals.EmailSentModal.show) : <code>boolean</code>
    * [.props](#state.modals.EmailSentModal.props) : <code>Object</code>

<a name="state.modals.EmailSentModal.show"></a>

##### EmailSentModal.show : <code>boolean</code>
Defines if visible

**Kind**: static property of [<code>EmailSentModal</code>](#state.modals.EmailSentModal)  
**Default**: <code>false</code>  
<a name="state.modals.EmailSentModal.props"></a>

##### EmailSentModal.props : <code>Object</code>
Modal props

**Kind**: static property of [<code>EmailSentModal</code>](#state.modals.EmailSentModal)  
<a name="state.modals.GlobalErrorModal"></a>

#### modals.GlobalErrorModal : <code>Object</code>
GlobalErrorModal

**Kind**: static property of [<code>modals</code>](#state.modals)  

* [.GlobalErrorModal](#state.modals.GlobalErrorModal) : <code>Object</code>
    * [.show](#state.modals.GlobalErrorModal.show) : <code>boolean</code>
    * [.props](#state.modals.GlobalErrorModal.props) : <code>Object</code>

<a name="state.modals.GlobalErrorModal.show"></a>

##### GlobalErrorModal.show : <code>boolean</code>
Defines if visible

**Kind**: static property of [<code>GlobalErrorModal</code>](#state.modals.GlobalErrorModal)  
**Default**: <code>false</code>  
<a name="state.modals.GlobalErrorModal.props"></a>

##### GlobalErrorModal.props : <code>Object</code>
Modal props

**Kind**: static property of [<code>GlobalErrorModal</code>](#state.modals.GlobalErrorModal)  
<a name="state.modals.HelpLearnMoreModal"></a>

#### modals.HelpLearnMoreModal : <code>Object</code>
HelpLearnMoreModal

**Kind**: static property of [<code>modals</code>](#state.modals)  

* [.HelpLearnMoreModal](#state.modals.HelpLearnMoreModal) : <code>Object</code>
    * [.show](#state.modals.HelpLearnMoreModal.show) : <code>boolean</code>
    * [.props](#state.modals.HelpLearnMoreModal.props) : <code>Object</code>

<a name="state.modals.HelpLearnMoreModal.show"></a>

##### HelpLearnMoreModal.show : <code>boolean</code>
Defines if visible

**Kind**: static property of [<code>HelpLearnMoreModal</code>](#state.modals.HelpLearnMoreModal)  
**Default**: <code>false</code>  
<a name="state.modals.HelpLearnMoreModal.props"></a>

##### HelpLearnMoreModal.props : <code>Object</code>
Modal props

**Kind**: static property of [<code>HelpLearnMoreModal</code>](#state.modals.HelpLearnMoreModal)  
<a name="state.modals.NotCompletedProfileModal"></a>

#### modals.NotCompletedProfileModal : <code>Object</code>
NotCompletedProfileModal

**Kind**: static property of [<code>modals</code>](#state.modals)  

* [.NotCompletedProfileModal](#state.modals.NotCompletedProfileModal) : <code>Object</code>
    * [.show](#state.modals.NotCompletedProfileModal.show) : <code>boolean</code>
    * [.props](#state.modals.NotCompletedProfileModal.props) : <code>Object</code>

<a name="state.modals.NotCompletedProfileModal.show"></a>

##### NotCompletedProfileModal.show : <code>boolean</code>
Defines if visible

**Kind**: static property of [<code>NotCompletedProfileModal</code>](#state.modals.NotCompletedProfileModal)  
**Default**: <code>false</code>  
<a name="state.modals.NotCompletedProfileModal.props"></a>

##### NotCompletedProfileModal.props : <code>Object</code>
Modal props

**Kind**: static property of [<code>NotCompletedProfileModal</code>](#state.modals.NotCompletedProfileModal)  
<a name="state.modals.PasswordGuardModal"></a>

#### modals.PasswordGuardModal : <code>Object</code>
PasswordGuardModal

**Kind**: static property of [<code>modals</code>](#state.modals)  

* [.PasswordGuardModal](#state.modals.PasswordGuardModal) : <code>Object</code>
    * [.show](#state.modals.PasswordGuardModal.show) : <code>boolean</code>
    * [.props](#state.modals.PasswordGuardModal.props) : <code>Object</code>

<a name="state.modals.PasswordGuardModal.show"></a>

##### PasswordGuardModal.show : <code>boolean</code>
Defines if visible

**Kind**: static property of [<code>PasswordGuardModal</code>](#state.modals.PasswordGuardModal)  
**Default**: <code>false</code>  
<a name="state.modals.PasswordGuardModal.props"></a>

##### PasswordGuardModal.props : <code>Object</code>
Modal props

**Kind**: static property of [<code>PasswordGuardModal</code>](#state.modals.PasswordGuardModal)  
<a name="state.modals.RequiresVerifiedEmailModal"></a>

#### modals.RequiresVerifiedEmailModal : <code>Object</code>
RequiresVerifiedEmailModal

**Kind**: static property of [<code>modals</code>](#state.modals)  

* [.RequiresVerifiedEmailModal](#state.modals.RequiresVerifiedEmailModal) : <code>Object</code>
    * [.show](#state.modals.RequiresVerifiedEmailModal.show) : <code>boolean</code>
    * [.props](#state.modals.RequiresVerifiedEmailModal.props) : <code>Object</code>

<a name="state.modals.RequiresVerifiedEmailModal.show"></a>

##### RequiresVerifiedEmailModal.show : <code>boolean</code>
Defines if visible

**Kind**: static property of [<code>RequiresVerifiedEmailModal</code>](#state.modals.RequiresVerifiedEmailModal)  
**Default**: <code>false</code>  
<a name="state.modals.RequiresVerifiedEmailModal.props"></a>

##### RequiresVerifiedEmailModal.props : <code>Object</code>
Modal props

**Kind**: static property of [<code>RequiresVerifiedEmailModal</code>](#state.modals.RequiresVerifiedEmailModal)  
<a name="getters"></a>

## getters : <code>Object</code>
Store module getters

**Kind**: global variable  
**Read only**: true  
**Example**  
```js
// Access to the module getters, where <name> is the getter nameStore.getters['Core/Modals/<name>']
```
<a name="actions"></a>

## actions : <code>Object</code>
Store module actions

**Kind**: global variable  
**Access**: protected  
**Example**  
```js
// Access to the module action, where <name> is the action nameStore.dispatch('Core/Modals/<name>')
```

* [actions](#actions) : <code>Object</code>
    * [.showModal(payload)](#actions.showModal) ⇒ <code>void</code>
    * [.hideModal(name)](#actions.hideModal) ⇒ <code>void</code>

<a name="actions.showModal"></a>

### actions.showModal(payload) ⇒ <code>void</code>
Show modal

**Kind**: static method of [<code>actions</code>](#actions)  

| Param | Type | Description |
| --- | --- | --- |
| payload | <code>Object</code> | Payload |
| payload.name | <code>string</code> | Modal name |
| [payload.props] | <code>Object</code> | (Optional) Modal props |

**Example**  
```js
this.$store.dispatch('Core/Modals/showModal', { name: 'GlobalErrorModal' })
```
<a name="actions.hideModal"></a>

### actions.hideModal(name) ⇒ <code>void</code>
Hide modal

**Kind**: static method of [<code>actions</code>](#actions)  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | Modal name |

**Example**  
```js
this.$store.dispatch('Core/Modals/showModal', 'GlobalErrorModal')
```
<a name="mutations"></a>

## mutations : <code>Object</code>
Store module mutations

**Kind**: global variable  
**Access**: protected  
**Example**  
```js
// Dispatch a module mutation, where <mutation_name> is the mutation nameStore.commit('Core/Modals/<mutation_name>', [payload])
```

* [mutations](#mutations) : <code>Object</code>
    * [.SET_SHOW_MODAL(payload)](#mutations.SET_SHOW_MODAL) ⇒ <code>void</code>
    * [.SET_MODAL_PROPS(payload)](#mutations.SET_MODAL_PROPS) ⇒ <code>void</code>

<a name="mutations.SET_SHOW_MODAL"></a>

### mutations.SET\_SHOW\_MODAL(payload) ⇒ <code>void</code>
Mutate `state.modals.<name>.show`

**Kind**: static method of [<code>mutations</code>](#mutations)  

| Param | Type | Description |
| --- | --- | --- |
| payload | <code>object</code> | Mutation payload |
| payload.name | <code>string</code> | Modal name |
| payload.value | <code>boolean</code> | True or false |

<a name="mutations.SET_MODAL_PROPS"></a>

### mutations.SET\_MODAL\_PROPS(payload) ⇒ <code>void</code>
Mutate `state.modals.<name>.props`

**Kind**: static method of [<code>mutations</code>](#mutations)  

| Param | Type | Description |
| --- | --- | --- |
| payload | <code>object</code> | Mutation payload |
| payload.name | <code>string</code> | Modal name |
| payload.value | <code>boolean</code> | True or false |

