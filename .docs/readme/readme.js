/**
 * @hperchec/readme-generator Template EJS data example file
 */

'use strict'

// Dependencies
const markdownTable = require('markdown-table') // ! v3 not compatible, use v2
const asciitree = require('ascii-tree')

// Based on the package.json file, get some data and informations
const packageJson = require('../../package.json')
const packageName = packageJson.name
const packageUrl = `https://www.npmjs.com/package/${packageName}`
const dependencies = packageJson.dependencies || {}
const devDependencies = packageJson.devDependencies || {}
const peerDependencies = packageJson.peerDependencies || {}
const author = packageJson.author
const contributors = packageJson.contributors || []
const license = packageJson.license || 'Unknown'
const homepage = packageJson.homepage
const projectUrl = packageJson.repository.url.match(/^git\+(.*)\.git$/)[1] // find string between 'git+' and '.git'
const projectPath = projectUrl.replace('https://gitlab.com/', '') // remove domain name
const issuesUrl = packageJson.bugs.url

const discordServerID = '972997305739395092'
const discordInviteLink = 'https://discord.gg/AWTgVAVKaR'
const scorpionUiVersion = peerDependencies['@hperchec/scorpion-ui']

// Output a markdown formatted table from a js object
// Like:
// |name|version|
// |----|-------|
// |    |       |
function getMdDependencies (deps) {
  return markdownTable([
    [ 'name', 'version' ],
    ...(Object.entries(deps))
  ])
}

/**
 * Return author link
 * @param {Object} author
 * @return {string}
 */
function getMdAuthor (author) {
  return '[' + author.name + '](' + author.url + ')'
}

/**
 * Return markdown list of persons
 * @param {Array} contributors
 * @return {String}
 */
function getMdContributors (contributors) {
  let mdString = ''
  contributors.forEach((person) => {
    mdString += '- [' + person.name + '](' + person.url + ')\n'
  })
  return mdString
}

/**
 * Export data for readme file templating
 */
module.exports = {
  packageName,
  packageUrl,
  projectUrl,
  projectPath,
  homepage,
  issuesUrl,
  dependencies: getMdDependencies(dependencies),
  devDependencies: getMdDependencies(devDependencies),
  peerDependencies: getMdDependencies(peerDependencies),
  author: getMdAuthor(author),
  contributors: getMdContributors(contributors),
  license,
  discordServerID,
  discordInviteLink,
  scorpionUiVersion,
  generateAsciiTree: function (str) {
    return asciitree.generate(str) // See documentation https://www.npmjs.com/package/ascii-tree
  }
}
