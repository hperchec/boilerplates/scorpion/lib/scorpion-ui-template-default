module.exports = {
  globals: {
    __VERSION__: false,
    __DOC_BASE_URL__: false,
    __getDocUrl__: false,
    ScorpionUI: false
  },
  env: {
    node: true
  },
  extends: [
    // @vue standard config
    '@vue/standard',
    // eslint-plugin-vue
    'plugin:vue/essential',
    'plugin:vue/strongly-recommended',
    'plugin:vue/recommended',
    // @intlify/eslint-plugin-vue-i18n
    'plugin:@intlify/vue-i18n/recommended',
    // eslint-plugin-tailwindcss
    'plugin:tailwindcss/recommended',
    'plugin:jsdoc/recommended-typescript'
  ],
  parserOptions: {
    parser: '@babel/eslint-parser',
    sourceType: 'module'
  },
  processor: 'disable/disable', // For "disable" plugin
  plugins: [
    'disable', // plugin to disable other plugins (https://www.npmjs.com/package/eslint-plugin-disable)
    'jsdoc' // parse docblocks (JSDoc)
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-unused-vars': [ 'error', { args: 'none' } ],
    'array-bracket-spacing': [ 'error', 'always' ],
    indent: [ 'error', 2, {
      ignoredNodes: [ 'ConditionalExpression' ],
      SwitchCase: 1
    } ],
    // Vue
    'vue/multi-word-component-names': 'off',
    // @intlify/eslint-plugin-vue-i18n
    '@intlify/vue-i18n/no-raw-text': [ 'error', {
      ignorePattern: '^[0-9-+#:()&"/\'.!?*→€]+$',
      ignoreText: [ 'English', 'Français' ]
    } ],
    '@intlify/vue-i18n/no-html-messages': 'error',
    '@intlify/vue-i18n/no-missing-keys': 'error',
    '@intlify/vue-i18n/no-v-html': 'warn',
    // eslint-plugin-tailwindcss
    'tailwindcss/no-custom-classname': [ 'off', {
      whitelist: []
    } ],
    // jsdoc
    'jsdoc/no-undefined-types': [ 'error', {
      definedTypes: [
        'Vue',
        'VNode',
        'VueComponent'
      ]
    } ],
    'jsdoc/check-tag-names': [ 'error', {
      definedTags: [
        'vuepress',
        'category', // jsdoc2md/dmd featured tag
        'typicalname', // jsdoc2md/dmd featured tag
        'sidebar_title', // custom tag for sidebar title
        'scorpion_ui_service', // custom tag for services
        'todo' // custom tag for project management
      ]
    } ],
    'jsdoc/require-param': [ 'warn', {
      checkDestructured: false
    } ],
    'jsdoc/check-param-names': [ 'warn', {
      checkDestructured: false
    } ],
    'jsdoc/no-defaults': 'off', // unexpected conflict with doc generation (TO DO)
    'jsdoc/no-types': 'off' // unexpected conflict with typescript errors
  },
  settings: {
    'disable/plugins': [
      'vue'
    ],
    'disable/externalProcessor': 'vue/.vue',
    jsdoc: {
      mode: 'typescript',
      tagNamePreference: {
        return: 'returns',
        augments: 'extends'
      }
    },
    // eslint-plugin-vue-i18n
    // See also: https://eslint-plugin-vue-i18n.intlify.dev/started.html#settings-vue-i18n
    'vue-i18n': {
      localeDir: [
        {
          // Translations from Scorpion-UI library
          // See also: https://eslint-plugin-vue-i18n.intlify.dev/started.html#configuration-eslintrc
          pattern: './node_modules/@hperchec/scorpion-ui/src/services/i18n/messages/**/*.json',
          localePattern: /^.*\/(?<locale>[A-Za-z0-9-_]+)\/.*\.json$/,
          localeKey: 'path'
        },
        {
          // Our own translations
          pattern: './src/i18n/**/*.json', // extention is glob formatting!
          localePattern: /^.*\/(?<locale>[A-Za-z0-9-_]+)\/.*\.json$/,
          localeKey: 'path'
        }
      ]
    }
  },
  overrides: [
    /**
     * Disable jsdoc plugin for .vue files
     */
    {
      files: [
        '*.vue',
        '*rollup-plugin-vue=script.js' // script in .vue files are processed as file that ends with ".vue?rollup-plugin-vue=script.js"
      ],
      settings: {
        'disable/plugins': [
          'jsdoc'
        ]
      }
    },
    /**
     * For services identifiers: always quoted
     * Default is not consistent
     */
    {
      files: [
        './src/config/index.js'
      ],
      rules: {
        'quote-props': [ 'error', 'consistent' ]
      }
    },
    /**
     * Ignore folders for jsdoc rules
     */
    {
      files: [
        './build/**/*'
      ],
      settings: {
        'disable/plugins': [
          'jsdoc'
        ]
      }
    },
    /**
     * Tests environment
     */
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)'
      ],
      env: {
        jest: true
      }
    }
  ]
}
