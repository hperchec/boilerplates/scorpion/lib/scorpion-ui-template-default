## 1.0.0-rc.2 (2024-11-06)

* wip: update template ([a7fb746](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/commit/a7fb746))
* ci: temporary disable gitlab-ci ([a01bb55](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/commit/a01bb55))
* fix: add missing layout components ([4168a6f](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/commit/4168a6f))



## 1.0.0-rc.1 (2024-11-05)

* feat: add "to" configuration to route for Breadcrumb component ([b7922ab](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/commit/b7922ab))
* build: fix sass-loader version ([2279bec](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/commit/2279bec))



## 1.0.0-rc.0 (2024-11-05)

* docs: update docs ([92afd8b](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/commit/92afd8b))
* docs: update docs & types ([d5612fd](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/commit/d5612fd))
* build: temporary ignore api doc on release ([eedb523](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/commit/eedb523))
* build: update .docs path ([557a931](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/commit/557a931))
* feat: prepare v1 ([926ef4f](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/commit/926ef4f))
* wip: wip ([e257ff4](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/commit/e257ff4))
* wip: wip ([bfe1cca](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/commit/bfe1cca))



## [0.0.6](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/compare/v0.0.5...v0.0.6) (2023-04-09)



## [0.0.5](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/compare/v0.0.4...v0.0.5) (2023-03-20)



## [0.0.4](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/compare/v0.0.3...v0.0.4) (2022-10-03)



## [0.0.3](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/compare/v0.0.2...v0.0.3) (2022-10-03)



## [0.0.2](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/compare/v0.0.1...v0.0.2) (2022-10-03)



## [0.0.1](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/compare/v0.0.1-alpha.6...v0.0.1) (2022-10-03)



## [0.0.1](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/compare/v0.0.1-alpha.6...v0.0.1) (2022-10-03)



## [0.0.1](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/compare/v0.0.1-alpha.6...v0.0.1) (2022-10-03)



## [0.0.1](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/compare/v0.0.1-alpha.6...v0.0.1) (2022-10-03)



## [0.0.1](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/compare/v0.0.1-alpha.6...v0.0.1) (2022-10-03)



## [0.0.1](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/compare/v0.0.1-alpha.6...v0.0.1) (2022-10-03)



## [0.0.1-alpha.6](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/compare/v0.0.1-alpha.4...v0.0.1-alpha.6) (2022-10-03)



## [0.0.1-alpha.4](https://gitlab.com/hperchec/boilerplates/scorpion/lib/scorpion-ui-template-default/compare/v0.0.1-alpha.3...v0.0.1-alpha.4) (2022-10-03)



## 0.0.1-alpha.3 (2022-10-03)



## 0.0.1-alpha.2 (2022-09-30)



## 0.0.1-alpha.2 (2022-09-30)



## 0.0.1-alpha.2 (2022-09-30)



## 0.0.1-alpha.2 (2022-09-30)



## 0.0.1-alpha.2 (2022-09-30)



## 0.0.1-alpha.2 (2022-09-30)



## 0.0.1-alpha.2 (2022-09-30)



## 0.0.1-alpha.2 (2022-09-30)



## 0.0.1-alpha.2 (2022-09-30)



## 0.0.1-alpha.2 (2022-09-30)



## 0.0.1-alpha.2 (2022-09-30)



## 0.0.1-alpha.2 (2022-09-30)



