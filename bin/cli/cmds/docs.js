// Exports command object
module.exports = {
  /**
   * Command syntax
   */
  command: 'docs <command>',
  /**
   * Aliases
   */
  aliases: [],
  /**
   * Command description
   */
  describe: 'Manage project documentation',
  /**
   * Builder
   * @param {Object} yargs
   * @return {Object}
   */
  builder: function (yargs) {
    return yargs.commandDir('docs_cmds')
      .demandCommand(1, ('Command is missing. See help to learn more.').red)
  },
  /**
   * Handler
   * @param {Object} argv - The argv
   * @return {void}
   */
  handler: async function (argv) {}
}
