const juisy = require('@hperchec/juisy')

const { run } = juisy.utils

// Exports command object
module.exports = {
  /**
   * Command syntax
   */
  command: 'generate:readme',
  /**
   * Aliases
   */
  aliases: [],
  /**
   * Command description
   */
  describe: 'Generate README.md file based on template with @hperchec/readme-generator package',
  /**
   * Builder
   * @param {object} yargs
   * @returns {object}
   */
  builder: function (yargs) {
    yargs.option('c', {
      alias: 'config',
      type: 'string',
      describe: 'Same as @hperchec/readme-generator --config option',
      requiresArg: true
    })
    return yargs
  },
  /**
   * Handler
   * @param {object} argv - The argv
   * @returns {void}
   */
  handler: async function (argv) {
    /**
     * Call readme-generator command
     */
    await run(
      'npx',
      [
        'readme-generator',
        ...(argv.config ? [ '--config', argv.config ] : [])
      ],
      { stdio: 'inherit' }
    )
  }
}
