/**
 * @file Build configuration
 * @author Hervé Perchec <herve.perchec@gmail.com>
 * @description @hperchec/scorpion-ui [/build/config.js]
 */

const path = require('path')
// Rollup plugins
const alias = require('@rollup/plugin-alias')
const eslint = require('@rollup/plugin-eslint')
const vue = require('rollup-plugin-vue')
const scss = require('rollup-plugin-scss')
const json = require('@rollup/plugin-json')
const cjs = require('@rollup/plugin-commonjs')
const nodePolyfills = require('rollup-plugin-polyfill-node')
const node = require('@rollup/plugin-node-resolve').nodeResolve
const replace = require('@rollup/plugin-replace')
// Custom rollup plugins
const extractCSS = require('@hperchec/scorpion-ui/shared/rollup/plugins/extract-css')

const eslintOptions = {
  // include: [
  //   'src/**/*.{js,vue}'
  // ]
  include: 'src/**/*.+(vue|js)',
  exclude: /rollup-plugin-vue=script\.js/
}

const vueOptions = {
  css: false,
  normalizer: '~@hperchec/scorpion-ui/shared/rollup/plugins/vue/lib/runtime/component-normalizer.js'
}

const scssOptions = {
  // Defaults to output.css, Rollup may add a hash to this!
  name: 'scorpion-ui-template-default.css',
  sass: require('sass'),
  quietDeps: true
}

// package.json
const packageJson = require('../package.json')

// Get version
const version = process.env.VERSION || packageJson.version

/**
 * The banners to prepend to bundle content
 */
const banner =
  '/*!\n' +
  ` * Scorpion UI default template v${version}\n` +
  ' * @author [Hervé Perchec](http://herve-perchec.fr)\n' +
  ' * Released under the GPLv3 License.\n' +
  ' */'

/**
 * Aliases
 */
const aliases = require('./alias')
// 'resolve' util
const resolve = p => {
  const base = p.split('/')[0]
  if (aliases[base]) {
    return path.resolve(aliases[base], p.slice(base.length + 1))
  } else {
    return path.resolve(__dirname, '../', p)
  }
}

/**
 * Globals
 */
const globals = {
  '@hperchec/scorpion-ui': 'ScorpionUI',
  'animate-sass': 'animateSass',
  // vuex: 'Vuex',
  'vue-awesome': 'vueAwesome',
  'vue-ripple-directive': 'vueRippleDirective',
  'vue-tailwind': 'vueTailwind',
  tailwindcss: 'tailwindcss'
}

const builds = {
  // Non minified ESM Development build
  devESM: {
    entry: {
      'scorpion-ui-template-default': resolve('@/index.js')
    },
    dest: resolve('dist'),
    moduleName: 'scorpion-ui-template-default',
    format: 'esm',
    env: 'development',
    external: function (id) {
      for (const key in globals) {
        if (id.startsWith(key)) {
          return true
        }
      }
    },
    plugins: [
      eslint(eslintOptions),
      extractCSS(),
      vue(vueOptions),
      scss(scssOptions),
      json(),
      cjs(),
      nodePolyfills(),
      node()
    ],
    banner
  },
  // Minified ESM production build
  minESM: {
    entry: {
      'scorpion-ui-template-default.min': resolve('@/index.js')
    },
    dest: resolve('dist'),
    moduleName: 'scorpion-ui-template-default',
    format: 'esm',
    env: 'production',
    external: function (id) {
      for (const key in globals) {
        if (id.startsWith(key)) {
          return true
        }
      }
    },
    plugins: [
      eslint(eslintOptions),
      extractCSS(),
      vue(vueOptions),
      scss(scssOptions),
      json(),
      cjs(),
      nodePolyfills(),
      node()
    ],
    banner
  },
  // Non minified CommonJS build
  commonJs: {
    entry: {
      'scorpion-ui-template-default.common': resolve('@/index.js')
    },
    dest: resolve('dist'),
    moduleName: 'scorpion-ui-template-default',
    format: 'cjs',
    env: 'production',
    external: function (id) {
      for (const key in globals) {
        if (id.startsWith(key)) {
          return true
        }
      }
    },
    plugins: [
      eslint(eslintOptions),
      extractCSS(),
      vue(vueOptions),
      scss(scssOptions),
      json(),
      cjs(),
      nodePolyfills(),
      node()
    ],
    banner
  },
  // Minified CommonJS build
  minCommonJs: {
    entry: {
      'scorpion-ui-template-default.common.min': resolve('@/index.js')
    },
    dest: resolve('dist'),
    moduleName: 'scorpion-ui-template-default',
    format: 'cjs',
    exports: 'named',
    env: 'production',
    external: function (id) {
      for (const key in globals) {
        if (id.startsWith(key)) {
          return true
        }
      }
    },
    plugins: [
      eslint(eslintOptions),
      extractCSS(),
      vue(vueOptions),
      scss(scssOptions),
      json(),
      cjs(),
      nodePolyfills(),
      node()
    ],
    banner
  }
}

/**
 * Generate rollup config
 * @param {string} name - The build name
 * @returns {object} - The generated config
 */
function genConfig (name) {
  const opts = builds[name]

  // console.log('__dir', __dirname)
  const config = {
    input: opts.entry,
    external: opts.external,
    plugins: [
      alias({
        entries: Object.assign({}, aliases, opts.alias)
      })
    ].concat(opts.plugins || []),
    output: {
      // file: opts.dest,
      dir: opts.dest,
      format: opts.format,
      banner: opts.banner,
      globals: opts.globals || {},
      name: opts.moduleName,
      exports: opts.exports || 'auto',
      // inlineDynamicImports: true,
      entryFileNames: (chunkInfo) => {
        console.log('chunkInfo.name:', chunkInfo.name)
        return '[name].js'
      },
      assetFileNames: (assetInfo) => {
        // css file?
        if (assetInfo.name.endsWith('.css')) {
          return 'style/[name][extname]'
        } else {
          return 'assets/[name][extname]'
        }
      }
    },
    onwarn: (msg, warn) => {
      if (!/Circular/.test(msg)) {
        warn(msg)
      }
    }
  }

  const vars = {
    __VERSION__: `'${version}'`,
    __DOC_BASE_URL__: `'${packageJson.homepage}'`,
    // __getDocUrl__: `((url) => '${packageJson.homepage}' + url)`,
    __getDocUrl__: '((url) => "<DOC_URL>" + url)',
    preventAssignment: true
  }
  config.plugins.push(replace(vars))

  Object.defineProperty(config, '_name', {
    enumerable: false,
    value: name
  })

  return config
}

if (process.env.TARGET) {
  module.exports = genConfig(process.env.TARGET)
} else {
  exports.globals = globals
  exports.getBuild = genConfig
  exports.getAllBuilds = () => Object.keys(builds).map(genConfig)
}
