module.exports = {
  globals: {
    __GLOBALS__: false,
    // Root app instance
    __ROOT_VUE_INSTANCE__: false,
    // App version
    __APP_VERSION__: false,
    // App name
    __APP_NAME__: false
  },
  rules: {
    'array-bracket-spacing': [
      'error',
      'always'
    ],
    '@intlify/vue-i18n/no-raw-text': [
      'error',
      {
        ignorePattern: '^[-+#:()|&"\'.!?*]+$'
      }
    ],
    '@intlify/vue-i18n/no-html-messages': 'error',
    '@intlify/vue-i18n/no-missing-keys': 'off',
    '@intlify/vue-i18n/no-v-html': 'warn',
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off'
  },
  extends: [
    'plugin:@intlify/vue-i18n/recommended',
    'plugin:vue/essential',
    '@vue/standard'
  ],
  root: true,
  env: {
    node: true
  },
  parserOptions: {
    parser: '@babel/eslint-parser'
  },
  ignorePatterns: [
    '**/dist/*' // For this example, we need to ignore template as local dependency
  ],
  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)'
      ],
      env: {
        jest: true
      }
    }
  ]
}
