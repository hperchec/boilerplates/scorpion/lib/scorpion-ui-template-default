const path = require('path')
// Project root path
const rootPath = path.resolve(__dirname, '../')
// Resolve from root path
const resolve = (...args) => { // eslint-disable-line no-unused-vars
  return path.join(rootPath, ...args)
}

/**
 * Aliases for webpack
 */
module.exports = {
  // Uncomment the following alias to test the other versions of scorpion-ui
  // '@hperchec/scorpion-ui$': '@hperchec/scorpion-ui/dist/scorpion-ui.min.js'
  // '@hperchec/scorpion-ui$': '@hperchec/scorpion-ui/dist/scorpion-ui.common.cjs'
  // '@': path.resolve(rootPath, 'src')
  'vue-awesome': path.resolve(rootPath, 'node_modules/@hperchec/scorpion-ui-template-default/node_modules/vue-awesome/'),
  vue$: path.resolve(rootPath, 'node_modules/vue/dist/vue.esm.js'),
  '@hperchec/scorpion-ui$': path.resolve(rootPath, 'node_modules/@hperchec/scorpion-ui/dist/scorpion-ui.min.js')
}
