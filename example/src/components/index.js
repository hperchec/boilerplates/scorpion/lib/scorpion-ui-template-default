import { Core, toolbox } from '@hperchec/scorpion-ui'

import publicComponents from './public'

const { mergeContext } = toolbox.template.utils

// Merge public components
mergeContext(Core.context.vue.components, 'public', publicComponents)
