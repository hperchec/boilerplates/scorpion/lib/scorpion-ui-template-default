/* eslint-disable quote-props */

/**
 * Export custom 'dev' configuration
 */
export default {
  /**
   * Services configuration (development)
   */
  services: {
    'api-manager': {
      apis: {
        ServerAPI: {
          URL: 'http://localhost:8000', // Local server url
          baseURL: 'http://localhost:8000/api'
        }
      }
    },
    'logger': {
      log: true
    }
  }
}
