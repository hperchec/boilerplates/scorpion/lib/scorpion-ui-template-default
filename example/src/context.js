// Internationalization
import '@/i18n'
// Router
import '@/router'
// Components
import '@/components'
