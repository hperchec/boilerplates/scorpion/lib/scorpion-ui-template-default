import global from './global.json'
import views from './views.json'

export default {
  ...global,
  ...views
}
