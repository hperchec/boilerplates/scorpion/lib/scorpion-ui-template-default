// import privateRoutes from './private'
import publicRoutes from './public'

export default {
  // private: privateRoutes,
  public: publicRoutes
}
