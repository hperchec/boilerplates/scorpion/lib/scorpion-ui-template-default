// Route component
import component from '@/components/private/views/agent/schedule/Index'

/**
 * Private route:
 */
export default {
  path: '/agent/schedule',
  name: 'AgentSchedule',
  component: () => component,
  options: {
    meta: {
      layout: 'agent',
      forProfile: 'agent'
    }
  }
}
