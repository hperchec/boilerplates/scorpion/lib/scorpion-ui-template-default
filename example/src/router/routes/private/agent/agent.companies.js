// Route component
import component from '@/components/private/views/agent/companies/Index'

/**
 * Private route:
 * - **Path**: `/company/agents`
 * - **Params**: *none*
 */
export default {
  path: '/agent/companies',
  name: 'agent.companies',
  component: () => component,
  options: {
    query: {
      /**
       * (Optional) The current tab to set as default when page appears
       */
      current_tab: {
        type: String
      }
    },
    meta: {
      layout: 'agent',
      forProfile: 'agent',
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.agent.companies.index.title'), 1)
      }
    }
  }
}
