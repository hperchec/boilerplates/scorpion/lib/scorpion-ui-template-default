import { Core } from '@hperchec/scorpion-ui'
// Route component
import component from '@/components/private/views/agent/companies/view-company/Index'

// Models
import Company from '#models/Company'

/**
 * Private route:
 * - **Name**: agent.companies.view
 * - **Path**: `/agent/companies/:company`
 * - **Params**:
 *   - `company {Number}`: the unique company ID
 * - **Query**: *none*
 */
export default {
  path: '/agent/companies/:company',
  name: 'agent.companies.view',
  component: () => component,
  options: {
    modelBindings: {
      company: {
        model: Company,
        param: 'company',
        bind: (pkValue) => {
          // La company doit être lié à l'agent
          const authenticatedUser = Core.service('store').getters['Auth/currentUser']
          return authenticatedUser.agentProfile.companies.find((company) => company.id === pkValue)
        }
      }
    },
    meta: {
      layout: 'agent',
      forProfile: 'agent',
      // breadcrumb: {
      //   schema: [ '[Company]', '[CompanySites]', '<this>' ],
      //   title: vm => {
      //     return vm.site ? `${vm.site.name} (#${vm.site.id})` : ''
      //   }
      // },
      pageMeta: {
        title: vm => vm.company ? `${vm.company.name} (#${vm.company.id})` : ''
      }
    }
  }
}
