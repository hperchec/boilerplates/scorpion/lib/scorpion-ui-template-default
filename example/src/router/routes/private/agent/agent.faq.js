import { Core } from '@hperchec/scorpion-ui'
// Route component
import component from '@/components/private/views/agent/faq/Index'

/**
 * Private route:
 */
export default {
  path: '/agent/faq',
  name: 'agent.faq',
  component: () => component,
  options: {
    meta: {
      layout: 'agent',
      // DON'T RESTRICT THIS ROUTE => see beforeEnter below
      // forProfile: 'agent',
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.agent.FAQ.Index.title'), 1)
      }
    },
    beforeEnter: (to, from, next) => {
      const localStorageManager = Core.service('local-storage-manager')
      // ⚠ HERE THE VUE ROOT INSTANCE IS NOT YET MOUNTED
      // So we set mode user settings via local-storage-manager service
      localStorageManager.items['user-settings'].mode = 'agent'
      next()
    }
  }
}
