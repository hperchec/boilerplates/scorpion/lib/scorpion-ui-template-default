import { Core } from '@hperchec/scorpion-ui'
// Route component
import component from '@/components/private/views/agent/Index'

/**
 * Private route:
 * - **Name**: Agent
 * - **Path**: `/agent`
 * - **Params**: *none*
 * - **Query**: *none*
 */
export default {
  path: '/agent',
  name: 'agent',
  component: () => component,
  options: {
    meta: {
      layout: 'agent',
      requiresVerifiedEmail: true,
      requiresSubscription: false,
      // DON'T RESTRICT THIS ROUTE => see beforeEnter below
      // forProfile: 'agent',
      breadcrumb: {
        schema: null,
        title: vm => vm.$t('views.agent.index.title')
      },
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.agent.index.title'), 1)
      }
    },
    beforeEnter: (to, from, next) => {
      const localStorageManager = Core.service('local-storage-manager')
      // ⚠ HERE THE VUE ROOT INSTANCE IS NOT YET MOUNTED
      // So we set mode user settings via local-storage-manager service
      localStorageManager.items['user-settings'].mode = 'agent'
      next()
    }
  }
}
