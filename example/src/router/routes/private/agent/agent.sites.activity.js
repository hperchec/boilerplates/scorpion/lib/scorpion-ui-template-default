import { Core } from '@hperchec/scorpion-ui'
// Route component
import component from '@/components/private/views/agent/sites/activity/Index'
// Models
import Site from '#models/Site'
/**
 * Private route:
 * - **Name**: agent.sites.activity
 * - **Path**: `/agent/sites/:id/activity`
 * - **Params**: none
 * - **Query**:
 *   - `id {Number}` *(optional)*: the unique ID of site
 */
export default {
  path: '/agent/sites/:site/activity',
  name: 'agent.sites.activity',
  component: () => component,
  options: {
    // Meta fields
    modelBindings: {
      site: {
        model: Site,
        param: 'site',
        bind: (pkValue) => {
          // Le site a voir doit être un site lié à
          // l'entreprise de l'utilisateur authentifié
          const authenticatedUser = Core.service('store').getters['Auth/currentUser']

          // Si il est pas responsable du site
          if (!authenticatedUser.agentProfile.isResponsibleOnSite(pkValue)) {
            // Si l'agent n'est pas en service sur le site en question
            const onShiftSite = authenticatedUser.agentProfile.currentAgentShift?.site

            if (!onShiftSite || onShiftSite.id !== pkValue) {
              return undefined
            }
          }

          return authenticatedUser.agentProfile.activeSites.find((site) => site.id === pkValue)
        }
      }
    },
    meta: {
      layout: 'agent',
      keepAlive: true,
      forProfile: 'agent',
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.agent.sites.ViewActivity.title'), 1)
      }
    }
  }
}
