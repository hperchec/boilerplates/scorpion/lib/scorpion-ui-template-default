import { Core } from '@hperchec/scorpion-ui'
// Route component
import component from '@/components/private/views/agent/sites/instructions/create-instruction/Index'

// Models
import Site from '#models/Site'

/**
 * Private route:
 * - **Name**: agent.sites.instructions.create
 * - **Path**: `/agent/sites/:site/instructions/create`
 *   - `site {Number}`: the unique site
 * - **Query**: *none*
 */
export default {
  path: '/agent/sites/:site/instructions/create',
  name: 'agent.sites.instructions.create',
  component: () => component,
  options: {
    modelBindings: {
      site: {
        model: Site,
        param: 'site',
        bind: (pkValue, props, route) => {
          // Le site active a voir doit être un site lié à
          // l'agent de l'utilisateur authentifié
          const authenticatedUser = Core.service('store').getters['Auth/currentUser']
          return authenticatedUser.agentProfile.activeSites.find((site) => site.id === pkValue)
        }
      }
    },
    meta: {
      layout: 'agent',
      forProfile: 'agent',
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.agent.sites.instructions.create.title'), 1)
      }
    }
  }
}
