// Route component
import component from '@/components/private/views/agent/sites/Index'

/**
 * Private route:
 */
export default {
  path: '/agent/sites',
  name: 'agent.site',
  component: () => component,
  options: {
    meta: {
      layout: 'agent',
      forProfile: 'agent',
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.agent.sites.index.title'), 1)
      }
    }
  }
}
