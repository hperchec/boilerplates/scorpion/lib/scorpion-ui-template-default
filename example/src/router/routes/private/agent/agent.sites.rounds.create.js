import { Core } from '@hperchec/scorpion-ui'
// Route component
import component from '@/components/private/views/agent/sites/rounds/create/Index'

// Models
import Site from '#models/Site'

/**
 * Private route:
 * - **Path**: `/agent/sites/:site/rounds/create`
 * - **Params**:
 *   - `site {Number}`: the unique site ID
 */
export default {
  path: '/agent/sites/:site/rounds/create',
  name: 'agent.sites.rounds.create',
  component: () => component,
  options: {
    modelBindings: {
      site: {
        model: Site,
        param: 'site',
        bind: (pkValue) => {
          // Le site a voir doit être un site lié à
          // l'entreprise de l'utilisateur authentifié
          const authenticatedUser = Core.service('store').getters['Auth/currentUser']
          return authenticatedUser.agentProfile.activeSites.find((site) => site.id === pkValue)
        }
      }
    },
    meta: {
      layout: 'agent',
      forProfile: 'agent',
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.agent.sites.rounds.create.title'), 1)
      }
    }
  }
}
