import { Core } from '@hperchec/scorpion-ui'
// Route component
import component from '@/components/private/views/agent/sites/view-site/Index'

// Models
import Site from '#models/Site'

/**
 * Private route:
 * - **Name**: AgentViewSite
 * - **Path**: `/agent/sites/:siteId`
 * - **Params**:
 *   - `siteId {Number}`: the unique site ID
 * - **Query**: *none*
 */
export default {
  path: '/agent/sites/:site',
  name: 'agent.sites.view',
  component: () => component,
  options: {
    modelBindings: {
      site: {
        model: Site,
        param: 'site',
        bind: (pkValue) => {
          // Le site a voir doit être un site lié à
          // l'entreprise de l'utilisateur authentifié
          const authenticatedUser = Core.service('store').getters['Auth/currentUser']
          return authenticatedUser.agentProfile.activeSites.find((site) => site.id === pkValue)
        }
      }
    },
    meta: {
      layout: 'agent',
      forProfile: 'agent',
      // breadcrumb: {
      //   schema: [ '[Company]', '[CompanySites]', '<this>' ],
      //   title: vm => {
      //     return vm.site ? `${vm.site.name} (#${vm.site.id})` : ''
      //   }
      // },
      pageMeta: {
        title: vm => vm.site ? `${vm.site.name} (#${vm.site.id})` : ''
      }
    }
  }
}
