// Route component
import component from '@/components/private/views/company/agents/add/Index'

/**
 * Private route:
 * - **Path**: `/company/agents/add?[identifier=<String>]&[from=<String>]`
 * - **Params**: none
 */
export default {
  path: '/company/agents/add',
  name: 'company.agents.add',
  component: () => component,
  options: {
    query: {
      /**
       * (Optional) The unique business ID of the agent profile to target
       */
      identifier: {
        type: String
      },
      /**
       * (Optional) The path from
       */
      from: {
        type: String
      }
    },
    meta: {
      layout: 'company',
      forProfile: 'company',
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.company.agents.add.title'), 1)
      }
    }
  }
}
