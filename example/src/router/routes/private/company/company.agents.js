// Route component
import component from '@/components/private/views/company/agents/Index'

/**
 * Private route:
 * - **Path**: `/company/agents`
 * - **Params**: *none*
 */
export default {
  path: '/company/agents',
  name: 'company.agents',
  component: () => component,
  options: {
    query: {
      /**
       * (Optional) The current tab to set as default when page appears
       */
      current_tab: {
        type: String
      }
    },
    meta: {
      layout: 'company',
      forProfile: 'company',
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.company.agents.index.title'), 1)
      }
    }
  }
}
