import { Core } from '@hperchec/scorpion-ui'
// Route component
import component from '@/components/private/views/company/agents/view/Index'
// Models
import AgentProfile from '#models/AgentProfile'

/**
 * Private route:
 * - **Path**: `/company/agents/:agent`
 * - **Params**:
 *   - `agent {Number}`: the unique agent ID
 */
export default {
  path: '/company/agents/:agent',
  name: 'company.agents.view',
  component: () => component,
  options: {
    modelBindings: {
      agent: {
        model: AgentProfile,
        param: 'agent',
        bind: (pkValue) => {
          // Le profil agent doit être lié à l'entreprise de l'utilisateur authentifié
          const authenticatedUser = Core.service('store').getters['Auth/currentUser']
          return authenticatedUser.companyProfile.company.activeValidatedAgents.find((profile) => profile.id === pkValue)
        }
      }
    },
    meta: {
      layout: 'company',
      forProfile: 'company',
      pageMeta: {
        title: vm => vm.agent ? `${vm.agent.user.fullName()} (#${vm.agent.id})` : ''
      }
    }
  }
}
