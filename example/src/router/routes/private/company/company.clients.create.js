// Route component
import component from '@/components/private/views/company/clients/create/Index'

/**
 * Private route:
 * - **Path**: `/company/clients/create`
 * - **Params**: *none*
 */
export default {
  path: '/company/clients/create',
  name: 'company.clients.create',
  component: () => component,
  options: {
    meta: {
      layout: 'company',
      forProfile: 'company',
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.company.clients.create.title'), 1)
      }
    }
  }
}
