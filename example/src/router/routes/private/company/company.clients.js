// Route component
import component from '@/components/private/views/company/clients/Index'

/**
 * Private route:
 * - **Path**: `/company/clients`
 * - **Params**: *none*
 */
export default {
  path: '/company/clients',
  name: 'company.clients',
  component: () => component,
  options: {
    query: {
      /**
       * (Optional) The current tab to set as default when page appears
       */
      current_tab: {
        type: String
      }
    },
    meta: {
      layout: 'company',
      forProfile: 'company',
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.company.clients.index.title'), 1)
      }
    }
  }
}
