import { Core } from '@hperchec/scorpion-ui'
// Route component
import component from '@/components/private/views/company/clients/update/Index'

// Models
import Client from '#models/Client'

/**
 * Private route:
 * - **Path**: `/company/clients/:client/update`
 * - **Params**:
 *   - `client {Number}`: the unique client ID
 */
export default {
  path: '/company/clients/:client/update',
  name: 'company.clients.update',
  component: () => component,
  options: {
    modelBindings: {
      client: {
        model: Client,
        param: 'client',
        bind: (pkValue) => {
          // Le client doit être lié à l'entreprise de l'utilisateur authentifié
          const authenticatedUser = Core.service('store').getters['Auth/currentUser']
          return authenticatedUser.companyProfile.company.clients.find((client) => client.id === pkValue)
        }
      }
    },
    meta: {
      layout: 'company',
      forProfile: 'company',
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.company.clients.update.title'), 1)
      }
    }
  }
}
