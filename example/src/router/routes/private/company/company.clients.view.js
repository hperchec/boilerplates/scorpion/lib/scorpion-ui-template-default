import { Core } from '@hperchec/scorpion-ui'
// Route component
import component from '@/components/private/views/company/clients/view/Index'

// Models
import Client from '#models/Client'

/**
 * Private route:
 * - **Path**: `/company/clients/:client`
 * - **Params**:
 *   - `client {Number}`: the unique client ID
 */
export default {
  path: '/company/clients/:client',
  name: 'company.clients.view',
  component: () => component,
  options: {
    modelBindings: {
      client: {
        model: Client,
        param: 'client',
        bind: (pkValue) => {
          // Le profil agent doit être lié à l'entreprise de l'utilisateur authentifié
          const authenticatedUser = Core.service('store').getters['Auth/currentUser']
          return authenticatedUser.companyProfile.company.activeClients.find((client) => client.id === pkValue)
        }
      }
    },
    meta: {
      layout: 'company',
      forProfile: 'company',
      pageMeta: {
        title: vm => `${vm.client.name} (#${vm.client.id})`
      }
    }
  }
}
