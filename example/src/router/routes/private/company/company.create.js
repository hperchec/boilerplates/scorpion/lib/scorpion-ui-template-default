import { Core } from '@hperchec/scorpion-ui'
// Router utils
import { redirectToPreviousRoute } from '../../../utils'
// Route component
import component from '@/components/private/views/company/create-company/Index'

/**
 * Private route:
 * - **Path**: `/company/create`
 * - **Params**: *none*
 */
export default {
  path: '/company/create',
  name: 'company.create',
  component: () => component,
  options: {
    // Meta fields
    meta: {
      layout: 'dynamic',
      requiresInit: false,
      // DON'T RESTRICT THIS ROUTE => see beforeEnter below
      // forProfile: 'company',
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.company.create.title'), 1)
      }
    },
    beforeEnter: async (to, from, next) => {
      // Get authenticated user
      const authUser = Core.service('store').getters['Auth/currentUser']
      // Redirect if user already has company profile (and company) defined
      if (authUser.companyProfile) {
        redirectToPreviousRoute(from, next)
      } else {
        next()
      }
    }
  }
}
