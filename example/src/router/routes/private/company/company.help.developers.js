import PageVueComponent from '@/components/private/views/company/help/developers/.templates/Page.vue'
// Route component
import MdComponent from '@/components/private/views/company/help/developers/Index.md'

/**
 * Private route:
 * - **Path**: `/company/help/developers`
 * - **Params**: *none*
 */
export default {
  path: '/company/help/developers',
  name: 'company.help.developers',
  component: () => ({
    template: `
<PageVueComponent>
  <MdComponent />
</PageVueComponent>
    `,
    components: {
      PageVueComponent,
      MdComponent
    }
  }),
  options: {
    meta: {
      layout: 'company',
      forProfile: 'company',
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.company.help.title'), 1)
      }
    }
  }
}
