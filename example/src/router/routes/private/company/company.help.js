// Route component
import component from '@/components/private/views/company/help/Index'

/**
 * Private route:
 * - **Path**: `/company/help`
 * - **Params**: *none*
 */
export default {
  path: '/company/help',
  name: 'company.help',
  component: () => component,
  options: {
    meta: {
      layout: 'company',
      forProfile: 'company',
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.company.help.title'), 1)
      }
    }
  }
}
