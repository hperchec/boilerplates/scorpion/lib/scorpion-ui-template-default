import { Core } from '@hperchec/scorpion-ui'
// Route component
import component from '@/components/private/views/company/Index'

/**
 * Private route:
 * - **Path**: `/company`
 * - **Params**: *none*
 */
export default {
  path: '/company',
  name: 'company',
  component: () => component,
  options: {
    // Meta fields
    meta: {
      layout: 'company',
      requiresVerifiedEmail: true,
      // DON'T RESTRICT THIS ROUTE => see beforeEnter below
      // forProfile: 'company',
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.company.index.title'), 1)
      }
    },
    beforeEnter: (to, from, next) => {
      const localStorageManager = Core.service('local-storage-manager')
      // ⚠ HERE THE VUE ROOT INSTANCE IS NOT YET MOUNTED
      // So we set mode user settings via local-storage-manager service
      localStorageManager.items['user-settings'].mode = 'company'
      next()
    }
  }
}
