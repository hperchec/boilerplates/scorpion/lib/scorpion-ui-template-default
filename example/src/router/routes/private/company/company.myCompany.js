// Route component
import component from '@/components/private/views/company/my-company/Index'

/**
 * Private route:
 * - **Path**: `/company/my-company`
 * - **Params**: *none*
 */
export default {
  path: '/company/my-company',
  name: 'company.myCompany',
  component: () => component,
  options: {
    query: {
      /**
       * (Optional) The current tab to set as default when page appears
       */
      current_tab: {
        type: String
      }
    },
    // Meta fields
    meta: {
      layout: 'company',
      forProfile: 'company',
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.company.myCompany.title'), 1)
      }
    }
  }
}
