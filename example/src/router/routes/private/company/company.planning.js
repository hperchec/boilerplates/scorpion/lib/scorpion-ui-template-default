// Route component
import component from '@/components/private/views/company/planning/Index'
/**
 * Private route:
 * - **Name**: CompanyPlanning
 * - **Path**: `/company/planning`
 * - **Params**: none
 */
export default {
  path: '/company/planning',
  name: 'company.planning',
  component: () => component,
  options: {
    query: {
      /**
       * (Optional) The unique ID of the site to target
       */
      site_id: {
        type: Array,
        cast: value => {
          if (value instanceof Array) {
            return value.map(val => Number(val))
          } else {
            return [ Number(value) ]
          }
        }
      },
      /**
       * (Optional) The unique ID of the agent to target
       */
      agent_id: {
        type: Array,
        cast: value => {
          if (value instanceof Array) {
            return value.map(val => Number(val))
          } else {
            return [ Number(value) ]
          }
        }
      },
      /**
       * (Optional) The agent shifts status to display
       */
      status: {
        type: Array
      },
      /**
       * (Optional) If show agent shifts without agent
       */
      without_agent: {
        type: Boolean
      }
    },
    meta: {
      layout: 'company',
      forProfile: 'company',
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.company.planning.Index.title'), 1)
      }
    }
  }
}
