import { Core } from '@hperchec/scorpion-ui'
// Route component
import component from '@/components/private/views/company/sites/add-agent/Index'

// Models
import Site from '#models/Site'

/**
 * Private route:
 * - **Path**: `/company/sites/:site/agents/add?[id=<Number>]&[responsible=<Boolean>]&[team_id=<Number>]`
 * - **Params**:
 *   - `site {Number}`: the unique site ID
 */
export default {
  path: '/company/sites/:site/agents/add',
  name: 'company.sites.addAgent',
  component: () => component,
  options: {
    modelBindings: {
      site: {
        model: Site,
        param: 'site',
        bind: (pkValue) => {
          // Le site a voir doit être un site lié à
          // l'entreprise de l'utilisateur authentifié
          const authenticatedUser = Core.service('store').getters['Auth/currentUser']
          return authenticatedUser.companyProfile.company.activeSites.find((site) => site.id === pkValue)
        }
      }
    },
    query: {
      /**
       * (Optional) The agent profile id
       */
      id: {
        type: Number
      },
      /**
       * (Optional) Defines if the agent is responsible
       */
      responsible: {
        type: Boolean
      },
      /**
       * (Optional) The target team id
       */
      team_id: {
        type: Number
      },
      /**
       * (Optional) The path from
       */
      from: {
        type: String
      }
    },
    meta: {
      layout: 'company',
      forProfile: 'company',
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.company.sites.addAgent.title'), 1)
      }
    }
  }
}
