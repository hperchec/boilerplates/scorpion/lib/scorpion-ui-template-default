// Route component
import component from '@/components/private/views/company/sites/create/Index'

/**
 * Private route:
 * - **Path**: `/company/sites/create?[client_id=<Number>]&[name=<String>]`
 * - **Params**: none
 */
export default {
  path: '/company/sites/create',
  name: 'company.sites.create',
  component: () => component,
  options: {
    query: {
      /**
       * (Optional) The unique ID of the client to target
       */
      client_id: {
        type: Number
      },
      /**
       * (Optional) The name of site to create
       */
      name: {
        type: String
      }
    },
    meta: {
      layout: 'company',
      forProfile: 'company',
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.company.sites.create.title'), 1)
      }
    }
  }
}
