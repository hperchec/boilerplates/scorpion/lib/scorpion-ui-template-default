import { Core } from '@hperchec/scorpion-ui'
// Route component
import component from '@/components/private/views/company/sites/instructions/create-instruction/Index'

// Models
import Site from '#models/Site'

/**
 * Private route:
 * - **Name**: company.sites.instructions.create
 * - **Path**: `/company/sites/:siteId/instructions/create`
 *   - `siteId {Number}`: the unique site ID
 * - **Query**: *none*
 */
export default {
  path: '/company/sites/:siteId/instructions/create',
  name: 'company.sites.instructions.create',
  component: () => component,
  options: {
    modelBindings: {
      site: {
        model: Site,
        param: 'siteId',
        bind: (pkValue) => {
          // Le site a voir doit être un site lié à
          // l'entreprise de l'utilisateur authentifié
          const authenticatedUser = Core.service('store').getters['Auth/currentUser']
          return authenticatedUser.companyProfile.company.sites.find((site) => site.id === pkValue)
        }
      }
    },
    meta: {
      layout: 'company',
      forProfile: 'company',
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.company.sites.instructions.create.title'), 1)
      }
    }
  }
}
