// Route component
import component from '@/components/private/views/company/sites/Index'

/**
 * Private route:
 * - **Path**: `/company/sites`
 * - **Params**: *none*
 */
export default {
  path: '/company/sites',
  name: 'company.sites',
  component: () => component,
  options: {
    query: {
      /**
       * (Optional) The current tab to set as default when page appears
       */
      current_tab: {
        type: String
      }
    },
    meta: {
      layout: 'company',
      forProfile: 'company',
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.company.sites.index.title'), 1)
      }
    }
  }
}
