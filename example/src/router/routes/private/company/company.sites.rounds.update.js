import { Core } from '@hperchec/scorpion-ui'
// Route component
import component from '@/components/private/views/company/sites/rounds/update/Index'

import Site from '#models/Site'
import RoundDef from '#models/RoundDef'

/**
 * Private route:
 * - **Path**: `/company/sites/:site/rounds/:round/update`
 * - **Params**:
 *   - `site {Number}`: the unique site ID
 *   - `round {Number}`: the unique round def ID
 */
export default {
  path: '/company/sites/:site/rounds/:round/update',
  name: 'company.sites.rounds.update',
  component: () => component,
  options: {
    modelBindings: {
      site: {
        model: Site,
        param: 'site',
        bind: (pkValue, props, route) => {
          // Le site a voir doit être un site lié à
          // l'entreprise de l'utilisateur authentifié
          const authenticatedUser = Core.service('store').getters['Auth/currentUser']
          return authenticatedUser.companyProfile.company.sites.find((site) => site.id === pkValue)
        }
      },
      roundDef: {
        model: RoundDef,
        param: 'round',
        bind: (pkValue, props, route) => {
          const site = props.site // Get parent prop value
          return site.roundDefs.find((roundDef) => roundDef.id === pkValue)
        }
      }
    },
    meta: {
      layout: 'company',
      forProfile: 'company',
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.company.sites.rounds.update.title'), 1)
      }
    }
  }
}
