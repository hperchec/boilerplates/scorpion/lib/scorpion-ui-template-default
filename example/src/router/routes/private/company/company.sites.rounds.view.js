import { Core } from '@hperchec/scorpion-ui'
// Route component
import component from '@/components/private/views/company/sites/rounds/view/Index'

// Models
import Site from '#models/Site'
import RoundDef from '#models/RoundDef'

/**
 * Private route:
 * - **Path**: `/company/sites/:site/rounds/:round`
 * - **Params**:
 *   - `site {Number}`: the unique site ID
 *   - `round {Number}`: the unique round def ID
 */
export default {
  path: '/company/sites/:site/rounds/:round',
  name: 'company.sites.rounds.view',
  component: () => component,
  options: {
    modelBindings: {
      site: {
        model: Site,
        param: 'site',
        bind: (pkValue) => {
          // Le site a voir doit être un site lié à
          // l'entreprise de l'utilisateur authentifié
          const authenticatedUser = Core.service('store').getters['Auth/currentUser']
          return authenticatedUser.companyProfile.company.activeSites.find((site) => site.id === pkValue)
        }
      },
      roundDef: {
        model: RoundDef,
        param: 'round',
        bind: (pkValue, props, route) => {
          const site = props.site // Get parent prop value
          return site.roundDefs.find((roundDef) => roundDef.id === pkValue)
        }
      }
    },
    meta: {
      layout: 'company',
      forProfile: 'company',
      pageMeta: {
        title: vm => vm.roundDef.name
      }
    }
  }
}
