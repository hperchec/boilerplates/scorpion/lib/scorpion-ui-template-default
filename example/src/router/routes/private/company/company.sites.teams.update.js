import { Core } from '@hperchec/scorpion-ui'
// Route component
import component from '@/components/private/views/company/sites/teams/update/Index'

import Site from '#models/Site'
import Team from '#models/Team'

/**
 * Private route:
 * - **Path**: `/company/sites/:site/teams/:team/update`
 * - **Params**:
 *   - `site {Number}`: the unique site ID
 *   - `team {Number}`: the unique team ID
 */
export default {
  path: '/company/sites/:site/teams/:team/update',
  name: 'company.sites.teams.update',
  component: () => component,
  options: {
    modelBindings: {
      site: {
        model: Site,
        param: 'site',
        bind: (pkValue, props, route) => {
          // Le site a voir doit être un site lié à
          // l'entreprise de l'utilisateur authentifié
          const authenticatedUser = Core.service('store').getters['Auth/currentUser']
          return authenticatedUser.companyProfile.company.sites.find((site) => site.id === pkValue)
        }
      },
      team: {
        model: Team,
        param: 'team',
        bind: (pkValue, props, route) => {
          const site = props.site // Get parent prop value
          return site.teams.find((team) => team.id === pkValue)
        }
      }
    },
    meta: {
      layout: 'company',
      forProfile: 'company',
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.company.sites.teams.update.title'), 1)
      }
    }
  }
}
