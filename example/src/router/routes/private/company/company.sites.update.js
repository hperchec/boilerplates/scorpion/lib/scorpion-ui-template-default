import { Core } from '@hperchec/scorpion-ui'
// Route component
import component from '@/components/private/views/company/sites/update/Index'

// Models
import Site from '#models/Site'

/**
 * Private route:
 * - **Path**: `/company/sites/:site/update`
 * - **Params**:
 *   - `site {Number}`: the unique site ID
 */
export default {
  path: '/company/sites/:site/update',
  name: 'company.sites.update',
  component: () => component,
  options: {
    modelBindings: {
      site: {
        model: Site,
        param: 'site',
        bind: (pkValue) => {
          // Le site doit être lié à l'entreprise de l'utilisateur authentifié
          const authenticatedUser = Core.service('store').getters['Auth/currentUser']
          return authenticatedUser.companyProfile.company.sites.find((site) => site.id === pkValue)
        }
      }
    },
    meta: {
      layout: 'company',
      forProfile: 'company',
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.company.sites.update.title'), 1)
      }
    }
  }
}
