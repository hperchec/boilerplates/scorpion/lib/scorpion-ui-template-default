import { Core } from '@hperchec/scorpion-ui'
// Route component
import component from '@/components/private/views/company/sites/view/Index'

// Models
import Site from '#models/Site'

/**
 * Private route:
 * - **Path**: `/company/sites/:site`
 * - **Params**:
 *   - `site {Number}`: the unique site ID
 */
export default {
  path: '/company/sites/:site',
  name: 'company.sites.view',
  component: () => component,
  options: {
    modelBindings: {
      site: {
        model: Site,
        param: 'site',
        bind: (pkValue) => {
          // Le site a voir doit être un site lié à
          // l'entreprise de l'utilisateur authentifié
          const authenticatedUser = Core.service('store').getters['Auth/currentUser']
          return authenticatedUser.companyProfile.company.activeSites.find((site) => site.id === pkValue)
        }
      }
    },
    meta: {
      layout: 'company',
      forProfile: 'company',
      pageMeta: {
        title: vm => `${vm.site.name} (#${vm.site.id})`
      }
    }
  }
}
