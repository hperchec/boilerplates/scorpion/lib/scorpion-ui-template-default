import { Core } from '@hperchec/scorpion-ui'

/**
 * Catch all requests for '/' and send to:
 * - '/agent' for account type 'agent'
 * - '/company' for account type 'company'
 *
 * - **Path**: `/`
 * - **Params**: *none*
 */
export default {
  path: '/',
  name: 'home',
  component: null,
  options: {
    meta: {
      requiresVerifiedEmail: false,
      requiresInit: false
    },
    // Before enter per-route guard
    beforeEnter: async (to, from, next) => {
      // Redirect depending on saved mode (company or agent)
      const targetPath = Core.service('local-storage-manager').items['user-settings'].mode
      next({
        path: `/${targetPath}`
      })
    }
  }
}
