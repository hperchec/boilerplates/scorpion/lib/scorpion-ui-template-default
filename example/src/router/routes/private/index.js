import Home from './home'
// Agent
import Agent from './agent/agent'
import AgentCompanies from './agent/agent.companies'
import AgentSites from './agent/agent.sites'
import AgentViewSite from './agent/agent.sites.view'
import AgentViewCompany from './agent/agent.companies.view'
import AgentSiteActivity from './agent/agent.sites.activity'
import AgentSitesRoundsCreate from './agent/agent.sites.rounds.create'
import AgentSitesRoundsView from './agent/agent.sites.rounds.view'
import AgentSitesRoundsUpdate from './agent/agent.sites.rounds.update'
import AgentSiteCreateInstruction from './agent/agent.sites.instructions.create'
import AgentFAQ from './agent/agent.faq'
// Company
import Company from './company/company'
import CompanyCreate from './company/company.create'
import CompanyMyCompany from './company/company.myCompany'
import CompanyClients from './company/company.clients'
import CompanyClientsCreate from './company/company.clients.create'
import CompanyClientsView from './company/company.clients.view'
import CompanyClientsUpdate from './company/company.clients.update'
import CompanySites from './company/company.sites'
import CompanySitesCreate from './company/company.sites.create'
import CompanySitesView from './company/company.sites.view'
import CompanySitesUpdate from './company/company.sites.update'
import CompanySitesAddAgent from './company/company.sites.addAgent'
import CompanySitesTeamsCreate from './company/company.sites.teams.create'
import CompanySitesTeamsView from './company/company.sites.teams.view'
import CompanySitesTeamsUpdate from './company/company.sites.teams.update'
import CompanySitesActivity from './company/company.sites.activity'
import CompanySitesRoundsCreate from './company/company.sites.rounds.create'
import CompanySitesRoundsView from './company/company.sites.rounds.view'
import CompanySitesRoundsUpdate from './company/company.sites.rounds.update'
import CompanySitesInstructionsCreate from './company/company.sites.instructions.create'
import CompanyAgents from './company/company.agents'
import CompanyAgentsAdd from './company/company.agents.add'
import CompanyAgentsView from './company/company.agents.view'
// Planning
import CompanyPlanning from './company/company.planning'
// Settings
import Settings from './settings/settings'
import SettingsEditAccount from './settings/settings.editAccount'
import SettingsVerifyEmail from './settings/settings.verifyEmail'
// Help
import CompanyHelp from './company/company.help'
import CompanyHelpDevelopers from './company/company.help.developers'
// Subscribe
import Subscribe from './subscribe'
import SubscribePaymentConfirmed from './subscribe.paymentConfirmed'

import InitProfile from './initProfile'

export default {
  Home,
  Agent,
  AgentCompanies,
  AgentSites,
  AgentViewSite,
  AgentSiteActivity,
  AgentSitesRoundsCreate,
  AgentSitesRoundsView,
  AgentSitesRoundsUpdate,
  AgentSiteCreateInstruction,
  AgentViewCompany,
  AgentFAQ,

  Company,
  CompanyCreate,
  CompanyMyCompany,

  CompanyClients,
  CompanyClientsCreate,
  CompanyClientsView,
  CompanyClientsUpdate,

  CompanySites,
  CompanySitesCreate,
  CompanySitesView,
  CompanySitesUpdate,
  CompanySitesAddAgent,
  CompanySitesTeamsCreate,
  CompanySitesTeamsView,
  CompanySitesTeamsUpdate,
  CompanySitesActivity,
  CompanySitesRoundsCreate,
  CompanySitesRoundsView,
  CompanySitesRoundsUpdate,
  CompanySitesInstructionsCreate,

  CompanyAgents,
  CompanyAgentsAdd,
  CompanyAgentsView,

  CompanyPlanning,

  Subscribe,
  SubscribePaymentConfirmed,

  Settings,
  SettingsEditAccount,
  SettingsVerifyEmail,

  CompanyHelp,
  CompanyHelpDevelopers,

  InitProfile
}
