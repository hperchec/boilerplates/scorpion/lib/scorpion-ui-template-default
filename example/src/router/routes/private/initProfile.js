import { Core } from '@hperchec/scorpion-ui'
// Router utils

// Layout
import DefaultPrivateLayout from '@/components/private/layouts/DefaultPrivateLayout'
// Route component
import component from '@/components/private/views/initProfile/Index'

/**
 * Private route:
 * - **Path**: `/init-profile`
 * - **Params**: *none*
 */
export default {
  path: '/init-profile',
  name: 'initProfile',
  component: () => component,
  options: {
    meta: {
      layout: DefaultPrivateLayout,
      requiresVerifiedEmail: true,
      requiresInit: false,
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.initProfile.title'), 1)
      }
    },
    beforeEnter: async (to, from, next) => {
      // Get authenticated user
      const authUser = Core.service('store').getters['Auth/currentUser']
      // Redirect if user is already initialized
      if (authUser.isInitialized()) {
        next({ name: 'home' })
      } else {
        next()
      }
    }
  }
}
