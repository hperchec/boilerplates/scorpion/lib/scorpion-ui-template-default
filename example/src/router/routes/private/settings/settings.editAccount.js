// Route component
import component from '@/components/private/views/settings/edit-account/Index'

/**
 * Private route:
 * - **Path**: `/settings/edit-account`
 * - **Params**: *none*
 */
export default {
  path: '/settings/edit-account',
  name: 'settings.editAccount',
  component: () => component,
  options: {
    meta: {
      layout: 'dynamic',
      requiresVerifiedEmail: false,
      requiresSubscription: false,
      requiresInit: false,
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.settings.editAccount.title'), 1)
      }
    }
  }
}
