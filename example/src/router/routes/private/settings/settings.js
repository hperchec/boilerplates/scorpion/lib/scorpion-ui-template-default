// Route component
import component from '@/components/private/views/settings/Index'

/**
 * Private route:
 * - **Path**: `/settings`
 * - **Params**: *none*
 */
export default {
  path: '/settings',
  name: 'settings',
  component: () => component,
  options: {
    meta: {
      layout: 'dynamic',
      requiresVerifiedEmail: false,
      requiresSubscription: false,
      requiresInit: false,
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.settings.index.title'), 1)
      }
    }
  }
}
