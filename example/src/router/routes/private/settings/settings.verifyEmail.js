import { Core, utils } from '@hperchec/scorpion-ui'
// Route component
import component from '@/components/private/views/settings/verify-email/Index'
/* eslint-disable-next-line dot-notation */
const Route = Core.context.services['router'].Route

const { isBlankString } = utils

/**
 * Private route:
 * - **Path**: `/settings/email/verify/:hash?expires=<String>&signature=<String>`
 * - **Params**:
 *   - `hash {String}`: The temporary hash
 */
export default {
  path: '/settings/email/verify/:hash',
  name: 'settings.verifyEmail',
  component: () => component,
  options: {
    props: (route) => {
      // Check if 'expires' and 'signature' query params are defined
      if (!route.query.expires || isBlankString(route.query.expires)) Route.redirectToNotFound()
      if (!route.query.signature || isBlankString(route.query.signature)) Route.redirectToNotFound()
      return {
        /**
         * 'hash' prop
         */
        hash: route.params.hash
      }
    },
    query: {
      /**
       * (Required) The link expiration param
       */
      expires: {
        type: String
      },
      /**
       * (Required) The link signature
       */
      signature: {
        type: String
      }
    },
    meta: {
      layout: 'dynamic',
      requiresVerifiedEmail: false,
      requiresSubscription: false,
      requiresInit: false,
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.settings.verifyEmail.title'), 1)
      }
    },
    beforeEnter: (to, from, next) => {
      const authenticatedUser = Core.service('store').getters['Auth/currentUser']
      if (authenticatedUser.emailVerifiedAt) {
        next({ name: 'notFound' })
      } else {
        next()
      }
    }
  }
}
