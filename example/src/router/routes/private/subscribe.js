// Route component
import component from '@/components/private/views/subscribe/Index'
// Layout
import DefaultPrivateLayout from '@/components/private/layouts/DefaultPrivateLayout'

/**
 * Private route:
 * - **Params**:
 *   - //
 */
export default {
  path: '/subscribe',
  name: 'subscribe',
  component: () => component,
  options: {
    meta: {
      layout: DefaultPrivateLayout,
      requiresVerifiedEmail: true,
      requiresSubscription: false,
      requiresInit: false,
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.subscribe.index.title'), 1)
      }
    }
  }
}
