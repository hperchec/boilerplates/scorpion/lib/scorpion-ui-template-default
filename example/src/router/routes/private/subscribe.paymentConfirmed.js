// Route component
import component from '@/components/private/views/subscribe/PaymentConfirmed'
// Layout
import DefaultPrivateLayout from '@/components/private/layouts/DefaultPrivateLayout'

/**
 * Private route:
 * - **Params**:
 *   - //
 */
export default {
  path: '/subscribe/payment-confirmed',
  name: 'subscribe.paymentConfirmed',
  component: () => component,
  options: {
    meta: {
      layout: DefaultPrivateLayout,
      requiresVerifiedEmail: true,
      requiresSubscription: false,
      requiresInit: false,
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.subscribe.paymentConfirmed.title'), 1)
      }
    }
  }
}
