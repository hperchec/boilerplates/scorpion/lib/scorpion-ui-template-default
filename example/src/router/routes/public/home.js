import { Core } from '@hperchec/scorpion-ui'

const homeRoute = Core.context.services.router.routes.public.Home

homeRoute.name = 'home'

export default homeRoute
