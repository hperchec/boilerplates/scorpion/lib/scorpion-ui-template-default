// See: https://github.com/stylelint/stylelint-config-standard/blob/08b6e948de94189c1d3ce376e50f2c42dc94580e/index.js#L102
// We add:
// - possible underscore '_' as first character for custom naming
// - accept '\@' in name for theme generated classes
const commonRulePattern = '^[_]?([\\@]?[a-z0-9]*)(-[\\@]?[a-z0-9]+)*$'

module.exports = {
  extends: [
    'stylelint-config-standard-scss',
    'stylelint-config-standard-vue',
    'stylelint-config-tailwindcss/scss'
  ],
  rules: {
    'at-rule-empty-line-before': [ 'always', {
      except: [ 'blockless-after-same-name-blockless', 'first-nested', 'inside-block' ],
      ignore: [ 'after-comment' ]
    } ],
    'declaration-empty-line-before': null,
    'at-rule-no-unknown': [
      true,
      {
        ignoreAtRules: [
          'tailwind',
          'extend',
          'apply',
          'variants',
          'responsive',
          'screen'
        ]
      }
    ],
    'scss/at-extend-no-missing-placeholder': null,
    'import-notation': 'string',
    'selector-id-pattern': [
      commonRulePattern,
      {
        message: (selector) => `Expected id selector "${selector}" to be kebab-case`
      }
    ],
    'selector-class-pattern': [
      commonRulePattern,
      {
        message: (selector) => `Expected class selector "${selector}" to be kebab-case`
      }
    ],
    'keyframes-name-pattern': [
      commonRulePattern,
      {
        message: (selector) => `Expected keyframe name "${selector}" to be kebab-case`
      }
    ]
  }
}
