// Lodash kebabcase
const kebabcase = require('lodash.kebabcase')
// Tailwind colors
const tailwindColors = require('tailwindcss/colors')
// Tailwind plugins
const multiThemePlugin = require('@hperchec/tailwindcss-multi-theme')

// Utils
const { generateColorVariants, flattenColors, getThemeIdentifier, getThemeVariants } = require('./utils')

const defaultColors = {
  transparent: 'transparent',
  current: 'currentColor',
  black: { DEFAULT: tailwindColors.black },
  white: { DEFAULT: tailwindColors.white },
  gray: { ...tailwindColors.gray, DEFAULT: tailwindColors.gray['500'] }
}

// Tailwind config
const defaultConfig = {
  // Theme
  theme: {
    // Themes for multi-theme plugin
    themeVariants: [], // empty by default, will be set in genConfig
    // Theme colors (will be set in genConfig)
    colors: defaultColors,
    // Theme breakpoints
    screens: {}, // empty by default, will be set in genConfig
    // Theme 'fill' classes (will be set in genConfig)
    fill: defaultColors,
    // Theme extending
    extend: {
      transitionDuration: {
        0: '0ms'
      }
    }
  },
  plugins: [
    // Multi-theme plugin
    multiThemePlugin({
      themeClassPrefix: '' // Default: 'theme-'
    })
  ]
}

/**
 * Deep merge default config with config passed by parameter
 * @param {object} config - The custom config
 * @param {object} [options] - The options object
 * @param {string} [options.themePrefix] - Option to pass to `getThemeIdentifier` util as *prefix*
 * @param {Function} [options.nameFormatter] - Option to pass to `getThemeIdentifier` util as *format*. It will be also used to generate color identifiers.
 * @param {object} [options.globals = undefined] - See globals file documentation
 * @param {boolean} [options.generateColorVariants = true] - Defines if it generates theme color variants.
 * See also `generateColorVariants` util documentation
 * @param {boolean} [options.flattenColors = true] - Defines if it must flatten colors. See also `flattenColors` util documentation
 * @returns {object}
 */
function genConfig (config, options = {}) {
  // Check if globals are provided
  if (options.globals !== undefined) {
    // Process theme variants
    if (options.globals.THEMES === undefined) throw new Error('Tailwind util "genConfig": options.globals is provided but "THEMES" property is undefined')
    defaultConfig.theme.themeVariants = defaultConfig.theme.themeVariants.concat(getThemeVariants(options.globals.THEMES, {
      prefix: options.themePrefix,
      format: options.nameFormatter
    }))
    // Process breakpoints
    if (options.globals.BREAKPOINTS === undefined) throw new Error('Tailwind util "genConfig": options.globals is provided but "BREAKPOINTS" property is undefined')
    for (const identifier in options.globals.BREAKPOINTS) {
      // Transform identifier to lowercase (ex: '2XL' => '2xl')
      defaultConfig.theme.screens[identifier.toLowerCase()] = options.globals.BREAKPOINTS[identifier]
    }
    // Inject theme colors
    for (const themeKey in options.globals.THEMES) {
      const themeColors = options.globals.THEMES[themeKey]
      for (const colorKey in themeColors) {
        // Get color identifier (ex: theme: 'LIGHT', color: 'PRIMARY' => '@light-primary')
        const prefix = options.themePrefix || '@' // default: '@'
        const formatter = options.nameFormatter || kebabcase // default: kebabcase
        // Get theme identifier without prefix
        const colorIdentifier = prefix + formatter(getThemeIdentifier(themeKey, { prefix: '' }) + '-' + colorKey)
        const colorValue = themeColors[colorKey]
        // Generate color variants
        if (options.generateColorVariants) {
          const colorVariants = generateColorVariants(colorIdentifier, colorValue)
          Object.assign(defaultConfig.theme.colors, colorVariants)
          Object.assign(defaultConfig.theme.fill, colorVariants)
        } else {
          defaultConfig.theme.colors[colorIdentifier] = colorValue
          defaultConfig.theme.fill[colorIdentifier] = colorValue
        }
      }
    }
  }
  if (options.flattenColors) {
    defaultConfig.theme.colors = flattenColors(defaultConfig.theme.colors)
    defaultConfig.theme.fill = flattenColors(defaultConfig.theme.fill)
  }
  const resolvedConfig = {
    ...config,
    presets: config.presets
      ? [ defaultConfig, ...config.presets ]
      : [ defaultConfig ]
  }
  return resolvedConfig
}

module.exports = {
  defaultConfig,
  genConfig
}
