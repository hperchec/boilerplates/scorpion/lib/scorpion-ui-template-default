/* eslint-disable key-spacing, dot-notation */

// Lodash kekab-case
const kebabcase = require('lodash.kebabcase')
// Color
const Color = require('color')

/**
 * Generate color variants (<color>-XXX from 100 to 900)
 * @param {string} name - Color name
 * @param {string} value - Color value
 * @return {Object}
 */
function generateColorVariants (name, value) {
  return {
    [name]: {
      DEFAULT          :    value, // Base color
      100              :    Color(value).lighten(0.4).hex(),
      200              :    Color(value).lighten(0.3).hex(),
      300              :    Color(value).lighten(0.2).hex(),
      400              :    Color(value).lighten(0.1).hex(),
      500              :    value, // Equivalent to base color
      600              :    Color(value).darken(0.1).hex(),
      700              :    Color(value).darken(0.2).hex(),
      800              :    Color(value).darken(0.3).hex(),
      900              :    Color(value).darken(0.4).hex()
    }
  }
}

/**
 * Flatten all given colors
 * @param {Object} colorsForTailwind - The colors (tailwind accepted format)
 * @return {Object}
 */
function flattenColors (colorsForTailwind) {
  const tmp = {}
  // Get colors
  for (const name in colorsForTailwind) {
    // If object format
    if (typeof colorsForTailwind[name] === 'object') {
      // Check if there is DEFAULT value
      if (!colorsForTailwind[name].DEFAULT) {
        throw new Error(`No DEFAULT value found for "${name}" color. Please define a default value for color in object format`)
      }
      // Get all color variants
      for (const variant in colorsForTailwind[name]) {
        tmp[(variant === 'DEFAULT' ? name : (name + '-' + variant))] = colorsForTailwind[name][variant]
      }
    } else { // else -> string
      tmp[name] = colorsForTailwind[name]
    }
  }
  return tmp
}

/**
 * Get theme identifier based on value passed in `themeKey` param.
 * @param {string} themeKey - The theme name string
 * @param {Object} [options] - Options object
 * @param {string} [options.prefix = '@'] - Prefix to add to theme identifier
 * @param {Function} [options.format] - Function to format string (default: `lodash.kebabcase`).
 * The function must take string as unique parameter and return the formatted string.
 * @return {Array}
 * @example
 * getThemeIdentifier('CUSTOM_THEME') // returns '@custom-theme'
 * // Custom prefix
 * getThemeIdentifier('DARK', { prefix: '_' }) // returns '_dark'
 * // Custom formatter (example with lodash.camelcase)
 * getThemeIdentifier('CUSTOM_THEME', { format: camelcase }) // returns '@customTheme'
 */
function getThemeIdentifier (themeKey, options = {}) {
  options.prefix = typeof options.prefix === 'string'
    ? options.prefix
    : '@'
  options.format = typeof options.format === 'function'
    ? options.format
    : kebabcase
  return options.prefix + options.format(themeKey) // Return formatted string
}

/**
 * Get theme variants for @hperchec/tailwindcss-multi-theme plugin
 * @param {Object} themes - Themes defined in globals
 * @param {Object} [options] - Options object for `getThemeIdentifier`
 * @param {string} [options.prefix = '@'] - Same as `getThemeIdentifier` option
 * @param {Function} [options.format] - Same as `getThemeIdentifier` option
 * @return {Array}
 * @example
 * getThemeVariants({
 *   LIGHT: { PRIMARY: '#0000FF', SECONDARY: '#FFFFFF', TERTIARY: '#FF0000'},
 *   DARK: { PRIMARY: '#FF0000', SECONDARY: '#FFFFFF', TERTIARY: '#0000FF'}
 * })
 * // returns [ '@light', '@dark' ]
 */
function getThemeVariants (themes, options = {}) {
  return Object.keys(themes).map((name) => {
    return getThemeIdentifier(name, {
      prefix: options.prefix,
      format: options.format
    })
  })
}

module.exports = {
  generateColorVariants,
  flattenColors,
  getThemeIdentifier,
  getThemeVariants
}

/* eslint-enable key-spacing, dot-notation */
