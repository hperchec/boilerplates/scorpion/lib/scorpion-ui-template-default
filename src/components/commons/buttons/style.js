import { VariantableOptions } from '@/mixins/variantable'
import TButton from '@/plugins/vue-tailwind/options/TButton'

const TButtonStyle = new VariantableOptions(TButton.props)

/* eslint quote-props: ["error", "consistent"] */

// Merge from vue-tailwind TButton component
export default VariantableOptions.extend(
  VariantableOptions.extend(TButtonStyle, (inheritsFromVariant, createVariantExtensions) => ({
    fixedClasses: [
      'transition',
      'duration-200',
      'ease-in-out',
      'focus:outline-none',
      'disabled:pointer-events-none',
      'border-2'
    ],
    classes: [
      // ...
    ],
    variants: {
      /**
       * Common variants
       */
      'primary': [
        'border-transparent',
        'px-3',
        'py-1',
        'leading-normal',
        'focus:shadow',
        'hover:shadow',
        'disabled:text-gray-200',
        '@light:bg-@light-action',
        '@light:text-white',
        '@light:hover:bg-@light-action-400',
        '@dark:bg-@dark-primary',
        '@dark:text-white',
        '@dark:hover:bg-@dark-primary-400'
      ],
      'border-primary': [
        'border-2',
        '@light:border-@light-primary',
        '@dark:border-@dark-primary',
        'px-3',
        'py-1',
        'leading-normal',
        'hover:shadow',
        'focus:shadow',
        'disabled:text-gray-200',
        'bg-transparent',
        '@light:text-@light-primary',
        '@dark:text-@dark-primary-200'
      ],
      'action': [
        'border-transparent',
        'px-3',
        'py-1',
        'leading-normal',
        'hover:shadow',
        'focus:shadow',
        'disabled:text-gray-400',
        '@light:bg-@light-action',
        '@light:text-white',
        '@light:hover:bg-@light-action-400',
        '@dark:bg-@dark-action',
        '@dark:text-white',
        '@dark:hover:bg-@dark-action-400'
      ],
      'border-action': [
        '@light:border-@light-action',
        '@dark:border-@dark-action',
        'px-3',
        'py-1',
        'leading-normal',
        'hover:shadow',
        'focus:shadow',
        'disabled:text-gray-400',
        'bg-transparent',
        '@light:text-@light-action',
        '@dark:text-@dark-action-200'
      ],
      'success': [
        'border-transparent',
        'text-white',
        '@light:bg-@light-success',
        '@dark:bg-@dark-success',
        'px-3',
        'py-1',
        'leading-normal',
        'hover:bg-@light-success-400',
        'hover:shadow',
        'focus:shadow',
        'disabled:text-gray-200'
      ],
      'border-success': [
        'border-2',
        '@light:border-@light-success',
        '@dark:border-@dark-success',
        'px-3',
        'py-1',
        'leading-normal',
        'hover:shadow',
        'focus:shadow',
        'disabled:text-gray-400',
        'bg-transparent',
        '@light:text-@light-success',
        '@dark:text-@dark-success-300'
      ],
      'warning': [
        'text-white',
        '@light:bg-@light-warning',
        '@dark:bg-@dark-warning',
        'border',
        'border-transparent',
        'px-3',
        'py-1',
        'leading-normal',
        'hover:bg-@light-warning-400',
        'hover:shadow',
        'focus:shadow',
        'disabled:text-gray-200'
      ],
      'border-warning': [
        'border-2',
        '@light:border-@light-warning',
        '@dark:border-@dark-warning',
        'px-3',
        'py-1',
        'leading-normal',
        'hover:shadow',
        'focus:shadow',
        'disabled:text-gray-400',
        'bg-transparent',
        '@light:text-@light-warning',
        '@dark:text-@dark-warning-300'
      ],
      'error': [
        'text-white',
        '@light:bg-@light-error',
        '@dark:bg-@dark-error',
        'border',
        'border-transparent',
        'px-3',
        'py-1',
        'leading-normal',
        '@light:hover:bg-@light-error-400',
        '@dark:hover:bg-@dark-error-400',
        'hover:shadow',
        'focus:shadow',
        'disabled:text-gray-200'
      ],
      'border-error': [
        'border-2',
        '@light:border-@light-error',
        '@dark:border-@dark-error',
        'px-3',
        'py-1',
        'leading-normal',
        'hover:shadow',
        'focus:shadow',
        'disabled:text-gray-400',
        'bg-transparent',
        '@light:text-@light-error',
        '@dark:text-@dark-error-300'
      ],
      /**
       * 'light' theme variants
       */
      'alabaster': [
        'border',
        'border-transparent',
        'px-3',
        'py-1',
        'leading-normal',
        'bg-@light-alabaster',
        'text-@light-text-primary',
        'disabled:text-gray-200',
        'hover:shadow',
        'hover:bg-@light-alabaster-400',
        'focus:shadow',
        'focus:bg-@light-alabaster-400'
      ],
      'white-smoke': [
        'border',
        'border-transparent',
        'px-3',
        'py-1',
        'leading-normal',
        'bg-@light-white-smoke',
        'text-@light-text-primary',
        'disabled:text-gray-200',
        'hover:shadow',
        'hover:bg-@light-white-smoke-400',
        'focus:shadow',
        'focus:bg-@light-white-smoke-400'
      ],
      /**
       * 'dark' theme variants
       */
      'haiti': [
        'border',
        'border-transparent',
        'px-3',
        'py-1',
        'leading-normal',
        'bg-@dark-haiti',
        'text-@dark-text-primary',
        'disabled:text-gray-200',
        'hover:shadow',
        'hover:bg-@dark-haiti-400',
        'focus:shadow',
        'focus:bg-@dark-haiti-400'
      ],
      'black-rock': [
        'border',
        'border-transparent',
        'px-3',
        'py-1',
        'leading-normal',
        'bg-@dark-black-rock',
        'text-@dark-text-primary',
        'disabled:text-gray-200',
        'hover:shadow',
        'hover:bg-@dark-black-rock-400',
        'focus:shadow',
        'focus:bg-@dark-black-rock-400'
      ],
      'mirage': [
        'border',
        'border-transparent',
        'px-3',
        'py-1',
        'leading-normal',
        'bg-@dark-mirage',
        'text-@dark-text-primary',
        'disabled:text-gray-200',
        'hover:shadow',
        'hover:bg-@dark-mirage-200',
        'focus:shadow',
        'focus:bg-@dark-mirage-200'
      ]
    }
  })),
  (inheritsFromVariant, createVariantExtensions) => ({
    fixedClasses: {},
    classes: {},
    variants: {
      // Generate variants for "xs" buttons
      ...createVariantExtensions((parentName) => `${parentName}-xs`, {
        'px-3': false,
        'py-1': false,
        'leading-normal': false,
        'leading-tight': true,
        'px-2.5': true,
        'py-0.5': true,
        'text-sm': true
      }),
      // Generate variants for "lg" buttons
      ...createVariantExtensions((parentName) => `${parentName}-lg`, {
        'px-3': false,
        'py-1': false,
        'px-8': true,
        'py-2': true
      })
    }
  })
)
