import { VariantableOptions } from '@/mixins/variantable'
import TDropdownButton from '@/plugins/vue-tailwind/options/TDropdownButton'

const TDropdownButtonStyle = new VariantableOptions(TDropdownButton.props)

/* eslint quote-props: ["error", "consistent"] */

// Merge from vue-tailwind TDropdownButton component
export default VariantableOptions.extend(TDropdownButtonStyle, (inheritsFromVariant, createVariantExtensions) => ({
  fixedClasses: [
    'transition',
    'duration-100',
    'ease-in-out',
    'text-left'
  ],
  classes: [
    'w-full'
  ],
  variants: {
    /**
     * Common variants
     */
    'primary': [
      'text-left',
      'px-4',
      'py-1',
      '@light:text-white',
      '@light:bg-@light-primary',
      '@light:hover:bg-@light-primary-400',
      '@light:hover:text-white',
      '@dark:text-@dark-text-primary',
      '@dark:bg-@dark-primary',
      '@dark:hover:bg-@dark-primary-400',
      '@dark:hover:text-@dark-text-primary'
    ],
    'tertiary': [
      'text-white',
      'bg-@light-tertiary',
      'hover:bg-@light-primary'
    ],
    'error': [
      'text-white',
      'bg-error',
      'hover:bg-error-700'
    ],
    'success': [
      'text-white',
      'bg-green-500',
      'hover:bg-green-600'
    ],
    'transparent': [
      'bg-transparent',
      'hover:bg-transparent'
    ],
    /**
     * 'light' theme variants
     */
    'alabaster': [
      'text-left',
      'px-4',
      'py-1',
      'text-@light-text-primary',
      'bg-@light-alabaster',
      'hover:bg-@light-primary',
      'hover:text-white'
    ],
    'white-smoke': [
      'text-left',
      'px-4',
      'py-1',
      'text-@light-text-primary',
      'bg-@light-white-smoke',
      'hover:bg-@light-primary',
      'hover:text-white'
    ],
    /**
     * 'dark' theme variants
     */
    'haiti': [
      'text-left',
      'px-4',
      'py-1',
      'text-@dark-text-primary',
      'bg-@dark-haiti',
      'hover:bg-@dark-primary'
    ],
    'mirage': [
      'px-3',
      'py-1',
      'leading-normal',
      'bg-@dark-mirage',
      'text-@dark-text-primary',
      'disabled:text-gray-200',
      'hover:shadow',
      'hover:bg-@dark-mirage-200',
      'focus:shadow',
      'focus:bg-@dark-mirage-200'
    ]
  }
}))
