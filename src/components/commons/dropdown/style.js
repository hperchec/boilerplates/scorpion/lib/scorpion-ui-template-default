import { VariantableOptions } from '@/mixins/variantable'
import TDropdown from '@/plugins/vue-tailwind/options/TDropdown'

const TDropdownStyle = new VariantableOptions(TDropdown.props)

/* eslint quote-props: ["error", "consistent"] */

// Merge from vue-tailwind TDropdown component
export default VariantableOptions.extend(TDropdownStyle, (inheritsFromVariant, createVariantExtensions) => ({
  fixedClasses: {
    button: [
      'focus:border-none',
      'focus:ring-none',
      'focus:outline-none',
      'disabled:opacity-50',
      'disabled:cursor-not-allowed'
    ],
    wrapper: [
      'relative',
      'inline-flex',
      'flex-col'
    ],
    dropdownWrapper: [
      'relative',
      '-top-1'
    ],
    dropdown: [
      'origin-top-left',
      'absolute',
      'top-0',
      'w-auto',
      'mt-1',
      'z-10'
    ],
    enterClass: [
      'opacity-0',
      'scale-95'
    ],
    enterActiveClass: [
      'transition',
      'transform',
      'ease-in-out',
      'duration-100'
    ],
    enterToClass: [
      'opacity-100',
      'scale-100'
    ],
    leaveClass: [
      'opacity-100',
      'scale-100'
    ],
    leaveActiveClass: [
      'transition',
      'transform',
      'ease-in-out',
      'duration-100'
    ],
    leaveToClass: [
      'opacity-0',
      'scale-95'
    ]
  },
  classes: {
    button: [
      'hover:bg-@light-tertiary'
    ],
    dropdown: [
      'bg-transparent',
      'shadow-none'
    ]
  },
  variants: {
    /**
     * Common variants
     */
    'primary': {
      button: [
        'w-full',
        'block',
        'px-3',
        'py-1',
        'rounded',
        'transition',
        'duration-200',
        'ease-in-out',
        'hover:shadow',
        'focus:shadow',
        '@light:text-white',
        '@light:bg-@light-primary',
        '@light:hover:bg-@light-primary-400',
        '@light:hover:text-white',
        '@light:focus:bg-@light-primary-400',
        '@light:focus:text-white',
        '@dark:text-@dark-text-primary',
        '@dark:bg-@dark-primary',
        '@dark:hover:bg-@dark-primary-400',
        '@dark:hover:text-@dark-text-primary',
        '@dark:focus:bg-@dark-primary-400',
        '@dark:focus:text-@dark-text-primary'
      ],
      dropdown: [
        'w-full',
        'flex',
        'flex-col',
        'rounded',
        'shadow',
        'overflow-hidden',
        '@dark:bg-@dark-primary',
        '@light:bg-@light-primary'
      ]
    },
    /**
     * 'light' theme variants
     */
    'alabaster': {
      button: [
        'w-full',
        'block',
        'px-3',
        'py-1',
        'rounded',
        'bg-@light-alabaster',
        'text-gray-500',
        'transition',
        'duration-200',
        'ease-in-out',
        'hover:bg-@light-primary',
        'hover:text-@light-text-primary',
        'hover:shadow',
        'focus:bg-@light-primary',
        'focus:text-@light-text-primary',
        'focus:shadow'
      ],
      dropdown: [
        'w-full',
        'flex',
        'flex-col',
        'rounded',
        'bg-@light-alabaster',
        'shadow',
        'overflow-hidden'
      ]
    },
    'white-smoke': {
      button: [
        'w-full',
        'block',
        'px-3',
        'py-1',
        'rounded',
        'bg-@light-white-smoke',
        'text-gray-500',
        'transition',
        'duration-200',
        'ease-in-out',
        'hover:bg-@light-primary',
        'hover:text-@light-text-primary',
        'hover:shadow',
        'focus:bg-@light-primary',
        'focus:text-@light-text-primary',
        'focus:shadow'
      ],
      dropdown: [
        'w-full',
        'flex',
        'flex-col',
        'rounded',
        'bg-@light-white-smoke',
        'shadow',
        'overflow-hidden'
      ]
    },
    /**
     * 'dark' theme variants
     */
    'haiti': {
      button: [
        'w-full',
        'block',
        'px-3',
        'py-1',
        'rounded',
        'bg-@dark-haiti',
        'text-gray-500',
        'transition',
        'duration-200',
        'ease-in-out',
        'hover:bg-@dark-primary',
        'hover:text-@dark-text-primary',
        'hover:shadow',
        'focus:bg-@dark-primary',
        'focus:text-@dark-text-primary',
        'focus:shadow'
      ],
      dropdown: [
        'w-full',
        'flex',
        'flex-col',
        'rounded',
        'bg-@dark-haiti',
        'shadow',
        'overflow-hidden'
      ]
    },
    'mirage': {
      button: [
        'w-full',
        'block',
        'px-3',
        'py-1',
        'rounded',
        'bg-@dark-mirage',
        'text-gray-500',
        'transition',
        'duration-200',
        'ease-in-out',
        'hover:bg-@dark-primary',
        'hover:text-@dark-text-primary',
        'hover:shadow',
        'focus:bg-@dark-primary',
        'focus:text-@dark-text-primary',
        'focus:shadow'
      ],
      dropdown: [
        'w-full',
        'flex',
        'flex-col',
        'rounded',
        'bg-@dark-mirage',
        'shadow',
        'overflow-hidden'
      ]
    }
  }
}))
