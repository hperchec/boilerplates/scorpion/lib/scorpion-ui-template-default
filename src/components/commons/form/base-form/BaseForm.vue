<template>
  <form />
</template>

<script>
import { Core, utils } from '@hperchec/scorpion-ui'

import VModelable from '@/mixins/v-modelable'
import parseFormOption, { generateFieldRelatedKeys } from './parse-form-option'

const RequestError = Core.context.support.errors.RequestError
const { isDef, isUndef, isNull, snakecase } = utils

/**
 * **BaseForm** component
 */
export default {
  name: 'BaseForm',
  mixins: [
    VModelable
  ],
  // VModelable options
  model: {
    // "sync" with values
    // ⚠ Now this.values is a prop related to this.vModel, instead of this.value
    prop: 'values'
  },
  props: {
    /**
     * Define default values prop
     */
    values: {
      type: Object,
      default () {
        return {}
      }
    }
  },
  data () {
    return {
      form: {
        /**
         * User must provide on object where each property is a FormFieldDescriptor (`{[key: string]: FormFieldDescriptor}`)
         * @type {object}
         */
        fields: {},
        /**
         * Init global form error to null
         * If object: { message: 'The error message', errorCode: 'eXXXX'}
         * @type {?object}
         */
        globalError: null
      }
    }
  },
  computed: {
    /**
     * isFormValid: define if form is valid
     */
    isFormValid: function () {
      for (const field in this.form.fields) {
        if (!this.isFieldValid(field)) return false // Break
      }
      return true
    },
    /**
     * Get changed fields (where value is different from default value)
     * @returns {string[]}
     */
    changedFields: function () {
      const fields = []
      for (const field in this.form.fields) {
        const computedFieldChangedName = this.$filters.camelcase(field + '_changed')
        // If '<field>Changed' computed returns true
        if (this[computedFieldChangedName]) fields.push(field)
      }
      return fields
    },
    /**
     * hasChanged method: define if a field has changed
     * @returns {Boolean}
     */
    hasChanged: function () {
      return this.changedFields.length
    }
  },
  watch: {
    values: {
      deep: true,
      handler: function (to, from) {
        /**
         * Emit "changed" event
         */
        this.$emit('changed', this.changedFields)
      }
    }
  },
  beforeCreate () {
    parseFormOption(this)
  },
  created () {
    this.initValues()
  },
  methods: {
    /**
     * Get field related keys
     * @param {string} field - The field key
     * @returns {object}
     */
    getFieldRelatedKeys (field) {
      return generateFieldRelatedKeys(this, field)
    },
    /**
     * Get the field data key (default is snakecased field key)
     * @param {string} field - The field key
     */
    getFieldDataKey (field) {
      return this.$options.form.fields[field].dataKey || snakecase(field)
    },
    /**
     * Get the field current value
     * @param {string} field - The field key
     * @returns {Function}
     */
    getFieldValue (field) {
      return this.values[field]
    },
    /**
     * Get the field default function
     * @param {string} field - The field key
     * @returns {Function}
     */
    getFieldDefaultFn (field) {
      const fieldDescriptors = this.$options.form.fields
      return fieldDescriptors[field].default.bind(this)
    },
    /**
     * Get the field default value
     * @param {string} field - The field key
     * @returns {*}
     */
    getFieldDefaultValue (field) {
      const fieldDefaultFunction = this.getFieldDefaultFn(field)
      return fieldDefaultFunction()
    },
    /**
     * Get the field changed trigger function
     * @param {string} field - The field key
     * @returns {Function} - The changed trigger function
     */
    getFieldChangedFn (field) {
      const fieldDescriptors = this.$options.form.fields
      return fieldDescriptors[field].changed.bind(this)
    },
    /**
     * Get the field changed value
     * @param {string} field - The field key
     * @returns {boolean}
     */
    getFieldChangedValue (field) {
      const fieldChangedFunction = this.getFieldChangedFn(field)
      const defaultValue = this.getFieldDefaultValue(field)
      const currentValue = this.getFieldValue(field)
      return fieldChangedFunction(defaultValue, currentValue)
    },
    /**
     * Get the field state function
     * @param {string} field - The field key
     * @returns {Function} - The state function
     */
    getFieldStateFn (field) {
      const fieldDescriptors = this.$options.form.fields
      return fieldDescriptors[field].state.bind(this)
    },
    /**
     * Get the field changed value
     * @param {string} field - The field key
     * @returns {boolean}
     */
    getFieldStateValue (field) {
      const fieldStateFunction = this.getFieldStateFn(field)
      const currentValue = this.getFieldValue(field)
      const isValid = this.getFieldIsValidValue(field)
      const errors = this.form.fields[field].errors
      return fieldStateFunction(currentValue, isValid, errors)
    },
    /**
     * Get the field validator function
     * @param {string} field - The field key
     * @returns {Function} - The validator function
     */
    getFieldValidatorFn (field) {
      const fieldDescriptors = this.$options.form.fields
      return fieldDescriptors[field].validator.bind(this)
    },
    /**
     * Get the field valid value
     * @param {string} field - The field key
     * @returns {boolean}
     */
    getFieldIsValidValue (field) {
      return this[this.getFieldRelatedKeys(field).isValid]
    },
    /**
     * Get the field related to the data key
     * @param {string} dataKey - The data key
     */
    getFieldNameFromDataKey (dataKey) {
      let found
      for (const field in this.$options.form.fields) {
        // Support for array fields (ex: dates.0, dates.1, ...)
        const splittedKey = dataKey.split('.')
        const key = splittedKey[0]
        if (this.getFieldDataKey(field) === key) {
          found = splittedKey.length > 1
            ? `${field}.${splittedKey[1]}` // ex: `agent_shift_dates.0` => `agentShiftDates.0`
            : field
          break
        }
      }
      if (!found) {
        throw new Error('No field found for data key: ' + dataKey)
      }
      return found
    },
    /**
     * Get the field data formatter (default returns unchanged value)
     * @param {string} field - The field key
     * @returns {Function} - The data formatter function
     */
    getFieldDataFormatter (field) {
      return this.$options.form.fields[field].dataFormatter || (value => value)
    },
    /**
     * Returns the field formatted value (result from dataFormatter)
     * @param {string} field - The field key
     * @returns {*} - The formatted field value
     */
    getFieldFormattedValue (field) {
      const dataFormatter = this.getFieldDataFormatter(field).bind(this)
      return dataFormatter(this.values[field])
    },
    /**
     * Get form field error message
     * @param {string} field - The field name
     * @param {Function|boolean} [cb = true] - The callback to pass. Can be boolean: true returns the error message as is and false returns empty string
     * @returns {string} The error message
     */
    getFieldErrorMessage: function (field, cb = true) {
      let message = ''
      // Is field in error?
      if (this.isFieldInError(field)) {
        const fieldOptions = this.$options.form.fields[field]
        const fieldRef = this.form.fields[field]
        const overiddenRuleMessages = this.rulesToErrorMessages({
          ...this.$options.form.validation.defaultRuleMessages.call(this),
          ...fieldOptions.failed.ruleToErrorCode
        })

        // First, validation errors
        if (!isNull(fieldRef.errors.validation)) {
          // Loop on validation errors
          for (const key in fieldRef.errors.validation) {
            // assign message on each iteration, so it will overrides each time
            message = overiddenRuleMessages[key] || fieldRef.errors.validation[key]
          }
        }
        // Then, custom errors
        if (!isNull(fieldRef.errors.custom)) {
          // Loop on custom errors
          for (const key in fieldRef.errors.custom) {
            // assign message on each iteration, so it will overrides each time
            message = overiddenRuleMessages[key] || fieldRef.errors.custom[key]
          }
        }
        // Process cb argument
        if (typeof cb === 'function') {
          return cb(fieldRef.errors, message)
        } else {
          return cb ? message : ''
        }
      }
      return message
    },
    /**
     * Validate form field and set error messages
     * @param {string} field - The field name
     * @returns {boolean}
     */
    validateField (field) {
      let validated = true
      // reset field errors
      this.resetFieldErrors(field, 'validation') // reset only validation errors
      // init auto named error index
      let autoGenIndex = 0
      // closure that can be called by user to fail
      const fail = (error = undefined) => {
        // If error is provided
        if (isDef(error)) {
          // it can be "rule:*"
          if (error.startsWith(('rule:'))) {
            this.setFieldValidationError(field, error) // empty string because it will map corresponding defined under failed.ruleToErrorCode
          } else {
            this.setFieldValidationError(field, String(autoGenIndex), error)
            autoGenIndex++
          }
        } else {
          this.setFieldValidationError(field, 'rule:Valid')
        }
        validated = false
        return validated
      }
      // validator: function (value: any, fail: Function)
      const fieldValidator = this.getFieldValidatorFn(field)
      const validResult = fieldValidator(this.getFieldValue(field), fail)
      // if validated is false, user has called "fail"
      if (!validated) {
        return false
      } else {
        // validator can return undefined (consider it's true)
        validated = isUndef(validResult) ? true : validResult
        // if validated is false, set field validation error
        if (!validated) fail()
        return validated
      }
    },
    /**
     * Set fields errors & form global error
     * @param {RequestError} requestError - The error parsed response returned by the server
     * @returns {RequestError}
     */
    setFormErrors (requestError) {
      this.form.globalError = {
        message: '',
        errorCode: '',
        requestError // save requestError original object
      }
      // If error => 'e0050' ('A field is invalid')
      if (requestError.errorCode === 'e0050' && isDef(requestError.response.payload.fields)) {
        // Set errors for each field
        for (const fieldDataKey in requestError.response.payload.fields) {
          const fieldName = this.getFieldNameFromDataKey(fieldDataKey)
          this.setFieldErrors(fieldName, requestError.response.payload.fields[fieldDataKey])
        }
        // Form global error
        this.form.globalError.message = this.$t('errors.e0050')
        this.form.globalError.errorCode = 'e0050'
      } else if (requestError.errorCode === 'e0052') {
        // Else if error => 'e0052' ('The field {field} is not valid')
        this.form.globalError.message = requestError.errorMessage[this.$app.currentLocale()] || this.$t('errors.e0052')
        this.form.globalError.errorCode = 'e0052'
      } else {
        // Form global error
        this.form.globalError.message = this.$t(`errors.${requestError.errorCode}`)
        this.form.globalError.errorCode = requestError.errorCode
      }
      // Return the request error
      return requestError
    },
    /**
     * Set field validation errors
     * @param {string} field - The target field
     * @param {string} key - An error key
     * @param {string} [message = ''] - An error message
     */
    setFieldValidationError (field, key, message = '') {
      // First, check if field is defined
      this.ensureFieldDefined(field)
      const fieldOptions = this.$options.form.fields[field]
      const fieldRef = this.form.fields[field]
      // Ensure field errors is not null
      if (isNull(fieldRef.errors.validation)) fieldRef.errors.validation = {}
      // Check if rule and message is overridden
      if (key.startsWith('rule:')) {
        const overiddenRuleMessages = this.rulesToErrorMessages({
          ...this.$options.form.validation.defaultRuleMessages.call(this),
          ...fieldOptions.failed.ruleToErrorCode
        })
        fieldRef.errors.validation[key] = overiddenRuleMessages[key] || message
      } else {
        fieldRef.errors.validation[key] = message
      }
    },
    /**
     * Set field validation errors
     * @param {string} field - The target field
     * @param {object} errors - An object of errors
     */
    setFieldValidationErrors (field, errors) {
      for (const key in errors) {
        this.setFieldValidationError(field, key, errors[key])
      }
    },
    /**
     * Set field errors
     * @param {string} field - The target field
     * @param {string} key - An error key
     * @param {string} [message = ''] - An error message
     */
    setFieldError (field, key, message = '') {
      // Support for array fields (ex: dates.0, dates.1, ...)
      const fieldPath = field.split('.')
      // First, check if field is defined
      this.ensureFieldDefined(fieldPath[0])
      const fieldOptions = this.$options.form.fields[fieldPath[0]]
      let fieldRef = this.form.fields[fieldPath[0]]
      // Ensure field errors is not null
      if (isNull(fieldRef.errors.custom)) {
        fieldRef.errors.custom = {}
      }
      // If array field: children
      if (fieldPath.length > 1) {
        // Ensure children prop is defined
        if (!fieldRef.children) {
          fieldRef.children = []
        }
        // If no custom errors, set parent default to e0151
        if (!Object.keys(fieldRef.errors.custom).length) {
          fieldRef.errors.custom['rule:Valid'] = 'e0051' // This field is invalid
        }
        // Field ref targets the child
        fieldRef = fieldRef.children[Number(fieldPath[1])] = {
          errors: {
            validation: null,
            custom: {}
          }
        }
      }
      // Check if rule and message is overridden
      if (key.startsWith('rule:')) {
        const overiddenRuleMessages = this.rulesToErrorMessages({
          ...this.$options.form.validation.defaultRuleMessages.call(this),
          ...fieldOptions.failed.ruleToErrorCode
        })
        fieldRef.errors.custom[key] = overiddenRuleMessages[key] || message
      } else {
        fieldRef.errors.custom[key] = message
      }
    },
    /**
     * Set field errors
     * @param {string} field - The target field
     * @param {object} errors - An object of errors
     */
    setFieldErrors (field, errors) {
      for (const key in errors) {
        this.setFieldError(field, key, errors[key])
      }
    },
    /**
     * Reset field errors
     * @param {string} field - The target field
     * @param {?string} [type] - The type, can be [ 'validation', 'custom', '*', undefined, null ]
     * @returns {void}
     */
    resetFieldErrors (field, type) {
      // First, check if field is defined
      this.ensureFieldDefined(field)
      const toReset = [
        this.form.fields[field].errors,
        ...(this.form.fields[field].errors.children || [])
      ]
      for (const errObj of toReset) {
        // Process type arg
        if (type === 'validation') {
          errObj.validation = null
        } else if (type === 'custom') {
          errObj.custom = null
        } else {
          errObj.validation = null
          errObj.custom = null
        }
      }
    },
    /**
     * Reset errors
     * @returns {void}
     */
    resetErrors () {
      for (const field in this.form.fields) {
        this.resetFieldErrors(field) // reset all errors
      }
      this.form.globalError = null
    },
    /**
     * Throw an error if the field is not defined
     * @param {string} field - The target field
     */
    ensureFieldDefined (field) {
      // Check if field is defined
      if (!this.form.fields[field]) {
        throw new Error('Form component: field ""' + field + '"" is not defined')
      }
      return true
    },
    /**
     * Is form field in error
     * @param {string} field - The field name
     * @returns {boolean}
     */
    isFieldInError: function (field) {
      // First, check if field is defined
      this.ensureFieldDefined(field)
      const fieldRef = this.form.fields[field]
      const validationErrors = Boolean(fieldRef.errors.validation)
      const customErrors = Boolean(fieldRef.errors.custom)
      return validationErrors || customErrors
    },
    /**
     * Is form field valid
     * @param {string} field - The field name
     * @returns {boolean}
     */
    isFieldValid: function (field) {
      return this.getFieldIsValidValue(field)
    },
    /**
     * Returns an object with translated error messages
     * @param {object} rules - The rules object like: { 'rule:Unique': 'e00XX' }
     * @returns {object} An object like: { 'rule:Unique': 'The error message' }
     */
    rulesToErrorMessages (rules) {
      const result = {}
      for (const rule in rules) {
        const errorCode = rules[rule]
        const translationPath = `errors.${errorCode}`
        result[rule] = this.$te(translationPath) // 'te' check if path exists
          ? this.$t(translationPath)
          : errorCode
      }
      return result
    },
    /**
     * Init values to ensure each field value is reactive property
     */
    initValues () {
      // Loop on each field and set missing keys
      for (const field in this.form.fields) {
        // Check if property is already defined (can be set as undefined)
        if (!Object.prototype.hasOwnProperty.call(this.values, field)) {
          // Set reactive property with default value
          this.$set(this.values, field, this.$options.form.fields[field].default.call(this))
        }
      }
    },
    /**
     * Returns the form values serialized for data
     * @returns {object} - A data formatted object
     */
    valuesToData () {
      return Object.keys(this.values).reduce((data, field) => {
        const dataKey = this.getFieldDataKey(field)
        data[dataKey] = this.getFieldFormattedValue(field)
        return data
      }, {})
    },
    /**
     * Returns the form values as FormData instance for multipart/form-data type
     * @returns {FormData} - The FormData instance
     */
    valuesToFormData () {
      const formData = new FormData()
      const valuesToData = this.valuesToData()
      for (const key in valuesToData) {
        formData.append(key, valuesToData[key])
      }
      return formData
    },
    /**
     * Reset form with default values
     * @returns {void}
     */
    resetForm () {
      // Loop on each field and set default value (defined in this.$options.form)
      for (const field in this.form.fields) {
        this.values[field] = this.$options.form.fields[field].default.call(this) // eslint-disable-line vue/no-mutating-props
      }
    }
  }
}
</script>

<style lang="scss">
// ...
</style>

<docs lang="md">
This button is amazing, use it responsibly.

## Examples

</docs>
