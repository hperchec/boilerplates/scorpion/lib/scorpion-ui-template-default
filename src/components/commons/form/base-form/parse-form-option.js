import { utils } from '@hperchec/scorpion-ui'

const { deepMerge, isDef, camelcase, isEmptyString } = utils

/**
 * @typedef {object} FormFieldDescriptor
 * @property {string} [dataKey] - The field key in data object
 * @property {Function} [default] - A function that returns the default value: `function (): void`
 * @property {Function} [state] - The function that returns the field state `function (value: any, isValid: Boolean, errors: Object): Boolean`
 * @property {Function} [validator] - The function to define if the field is valid: `function (value: any, fail: Function): Boolean`
 * @property {Function} [dataFormatter] - The function to format field value when form.valuesToData() is called: `function (value: any): any`
 * @property {object} [ruleToErrorCode] - An object to map server returned error rules to error custom code: `Object<[key: String]: String>`
 */

/**
 * Generate field related keys based on form options
 * @param {Vue} vm - The vue model
 * @param {string} field - The target field
 * @returns {object} - The field related keys
 */
export function generateFieldRelatedKeys (vm, field) {
  const fieldPropertyNames = vm.$options.form.fieldPropertyNames
  return Object.keys(fieldPropertyNames).reduce((accumulator, key) => {
    accumulator[key] = fieldPropertyNames[key](field)
    return accumulator
  }, {})
}

/**
 * Returns a magic computed for the target field
 * @param {string} field - The field key
 * @returns {object} A magic computed for the target field
 */
function createFieldValueComputed (field) {
  return {
    get () {
      return this.values[field]
    },
    set (value) {
      this.values[field] = value
    }
  }
}

/**
 * Returns computed for the target field
 * @param {string} field - The field key
 * @returns {Function} Computed for the target field
 */
function createFieldChangedComputed (field) {
  return function () {
    return this.getFieldChangedValue(field)
  }
}

/**
 * Returns computed for the target field
 * @param {string} field - The field key
 * @returns {Function} Computed for the target field
 */
function createFieldStateComputed (field) {
  return function () {
    return this.getFieldStateValue(field)
  }
}

/**
 * Returns computed for the target field
 * @param {string} field - The field key
 * @returns {Function} Computed for the target field
 */
function createFieldIsValidComputed (field) {
  return function () {
    // return this.getFieldIsValidValue(field)
    return this.validateField(field)
  }
}

/**
 * Defaults
 */
const defaults = {
  fieldPropertyNames: {
    value: (field) => camelcase(field + '_value'),
    state: (field) => camelcase(field + '_state'),
    isValid: (field) => camelcase('is_' + field + '_valid'),
    changed: (field) => camelcase(field + '_changed')
  },
  validation: {
    defaultRuleMessages: function () {
      return {
        'rule:Email': 'e0032', // Email does not match the required format
        'rule:PasswordPolicy': 'e0030', // Password policy
        'rule:Required': 'e0053', // This field is required
        'rule:Valid': 'e0051' // This field is invalid
      }
    },
    defaultErrorMessage: function () {
      return this.$t('errors.e0051') // This field is invalid
    }
  },
  fieldDefault: function () {
    return undefined
  },
  fieldState: function (value, isValid, errors) {
    return !isEmptyString(value) // Filled ?
      ? !(Boolean(errors.validation) || Boolean(errors.custom)) // No errors ?
        ? isValid // Valid ?
        : false // else -> errors
      : true
  },
  fieldChanged: function (defaultValue, currentValue) {
    return currentValue !== defaultValue
  },
  fieldValidator: function (value, fail) {
    return true
  }
}

/**
 * Private local function to parse form option
 * -> "this" is the vue instance (bound in beforeCreate hook, see below)
 * @param {Vue} vm - The vue model
 */
export default function parseFormOption (vm) {
  // Check if form option is defined
  const formOptions = vm.$options.form
  if (!formOptions) {
    const errorMessage = 'Component extending BaseForm must have \'form\' option.'
    vm.$app.log(errorMessage, { type: 'error' })
    throw new Error(errorMessage)
  }

  const fieldDescriptors = formOptions.fields
  // Get form.fields
  if (!fieldDescriptors) {
    const errorMessage = 'Component extending BaseForm: no field properties found in \'form.fields\' option.'
    vm.$app.log(errorMessage, { type: 'error' })
    throw new Error(errorMessage)
  }

  // Other form option: fieldPropertyNames
  if (formOptions.fieldPropertyNames) {
    /**
     * fieldPropertyNames must be an object like: {
     *   value: (field: string) => string,
     *   state: (field: string) => string,
     *   isValid: (field: string) => string,
     *   changed: (field: string) => string
     * }
     */
    formOptions.fieldPropertyNames = deepMerge(defaults.fieldPropertyNames, formOptions.fieldPropertyNames)
  } else {
    // Set default
    formOptions.fieldPropertyNames = defaults.fieldPropertyNames
  }
  // Other form option: validation
  if (formOptions.validation) {
    formOptions.validation = deepMerge(defaults.validation, formOptions.validation)
  } else {
    // Set default
    formOptions.validation = defaults.validation
  }

  // Get data
  const dataOptions = vm.$options.data()
  // Get computed
  const computedOptions = vm.$options.computed

  // Loop on each field
  for (const field in fieldDescriptors) {
    const propertyKeys = generateFieldRelatedKeys(vm, field)

    // Add field to data
    dataOptions.form.fields[field] = {
      value: null, // hydrate value at "created" hook, see below
      errors: {
        validation: null, // validation errors
        custom: null // custom (manually set) errors
      }
    }

    // Field value computed
    computedOptions[propertyKeys.value] = createFieldValueComputed(field)

    // Check if 'default' property is set
    const fieldDefault = fieldDescriptors[field].default
    // If default not defined
    if (!isDef(fieldDefault)) {
      fieldDescriptors[field].default = defaults.fieldDefault
    }

    // Check if 'changed' property is set
    const fieldChanged = fieldDescriptors[field].changed
    // If changed not defined
    if (!isDef(fieldChanged)) {
      fieldDescriptors[field].changed = defaults.fieldChanged
    }

    // Field changed computed
    computedOptions[propertyKeys.changed] = createFieldChangedComputed(field)

    // Check if 'state' property is set
    const fieldState = fieldDescriptors[field].state
    // If state not defined
    if (!isDef(fieldState)) {
      fieldDescriptors[field].state = defaults.fieldState
    }

    // Field state computed
    computedOptions[propertyKeys.state] = createFieldStateComputed(field)

    // Check if 'validator' property is set
    const fieldValidator = fieldDescriptors[field].validator
    // Prepare computed
    if (!isDef(fieldValidator)) {
      fieldDescriptors[field].validator = defaults.fieldValidator
    }

    // Field isValid computed
    computedOptions[propertyKeys.isValid] = createFieldIsValidComputed(field)

    // Check if 'failed' property is set
    // Ensure it's defined
    if (!isDef(fieldDescriptors[field].failed)) {
      fieldDescriptors[field].failed = {} // empty object, see below
    }
    const fieldFailed = fieldDescriptors[field].failed
    if (!isDef(fieldFailed.ruleToErrorCode)) {
      fieldFailed.ruleToErrorCode = {}
    }
  }

  // Assign data
  vm.$options.data = function () {
    return dataOptions
  }
}
