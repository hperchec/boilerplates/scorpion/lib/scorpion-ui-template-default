export default {
  // Fixed classes
  fixedClasses: {
    wrapper: {
      'cursor-pointer': true,
      flex: true,
      'items-center': true,
      'gap-3': true,
      'py-2': true,
      'px-2': true,
      rounded: true,
      transition: true,
      'ease-in-out': true
    },
    input: {
      flex: true,
      'items-center': true,
      'justify-center': true,
      'w-5': true,
      'h-5': true,
      rounded: true
    }
  },
  // Default classes
  classes: {
    wrapper: {
      'bg-transparent': true
    },
    input: {
      // ...
    }
  },
  // Variants
  variants: {
    /**
     * common variants
     */
    transparent: {
      wrapper: {
        '@light:bg-transparent': true,
        '@dark:bg-transparent': true,
        '@light:if-checked:text-@light-text-primary': true,
        '@dark:if-checked:text-@dark-text-primary': true
      },
      input: {
        'border-2': true,
        'border-gray-500': true,
        'if-checked:border-0': true,
        '@light:if-checked:bg-@light-success': true,
        '@light:bg-transparent': true,
        '@dark:if-checked:bg-@dark-success': true,
        '@dark:bg-transparent': true
      }
    },
    /**
     * light theme variants
     */
    alabaster: {
      wrapper: {
        '@light:bg-@light-alabaster': true
      },
      input: {
        '@dark:if-checked:bg-@dark-success': true,
        '@dark:bg-@dark-black-rock': true
      }
    },
    /**
     * dark theme variants
     */
    haiti: {
      wrapper: {
        '@dark:if-checked:bg-@dark-primary': true,
        '@light:if-checked:bg-@light-primary': true
      },
      input: {
        '@dark:if-checked:bg-@dark-success': true,
        '@dark:bg-@dark-black-rock': true
      }
    }
  }
}
