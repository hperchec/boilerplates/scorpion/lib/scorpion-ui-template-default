import VInputStyle from '../style'
import { VariantableOptions } from '@/mixins/variantable/'

// Merge from custom VInput style
export default VariantableOptions.extend(VInputStyle, (inheritsFromVariant, createVariantExtensions) => ({
  fixedClasses: {
    visibleIcon: {
      absolute: true,
      'top-4': true,
      'right-4': true,
      'cursor-pointer': true
    },
    hiddenIcon: {
      absolute: true,
      'top-4': true,
      'right-4': true,
      'cursor-pointer': true
    }
  },
  classes: {
    visibleIcon: {},
    hiddenIcon: {}
  },
  variants: {}
}))
