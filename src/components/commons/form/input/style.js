import { VariantableOptions } from '@/mixins/variantable'
import TInput from '@/plugins/vue-tailwind/options/TInput'

const originalTInputStyle = TInput.props

// vue-tailwind t-input style is one-level object, so we have to wrap it
const TInputStyle = new VariantableOptions({
  fixedClasses: { input: originalTInputStyle.fixedClasses },
  classes: { input: originalTInputStyle.classes },
  variants: Object.keys(originalTInputStyle.variants).reduce((obj, variant) => {
    obj[variant] = {
      input: originalTInputStyle.variants[variant]
    }
    return obj
  }, {})
})

/* eslint quote-props: ["error", "consistent"] */

// Merge from vue-tailwind TInput component
export default VariantableOptions.extend(
  VariantableOptions.extend(TInputStyle, (inheritsFromVariant, createVariantExtensions) => ({
    fixedClasses: {
      wrapper: {
        'block': true,
        'mb-4': true,
        'overflow-auto': true
      },
      label: {
        'flex': true,
        'items-center': true,
        'mb-2': true
      },
      input: [
        'w-full',
        'mb-2',
        'transition',
        'duration-200',
        'ease-in-out',
        'focus:outline-none',
        'disabled:pointer-events-none'
      ],
      description: {
        'mb-2': true,
        'text-sm': true,
        'text-gray-400': true
      },
      error: {
        'mb-2': true,
        'text-sm': true,
        '@light:text-@light-error': true,
        '@dark:text-@dark-error': true
      }
    },
    classes: {
      wrapper: {},
      label: {},
      input: [
        'bg-@light-tertiary',
        'text-white',
        'border-2',
        'border-transparent',
        'px-3',
        'py-2',
        'rounded',
        'hover:border-primary',
        'focus:border-primary',
        'disabled:text-gray'
      ],
      description: {},
      error: {}
    },
    variants: {
      // 'tertiary' variant
      'tertiary': {
        wrapper: {},
        label: {},
        input: [
          'border-2',
          'border-transparent',
          'px-3',
          'py-2',
          'rounded',
          'disabled:text-gray',
          '@light:bg-@light-tertiary',
          '@light:text-@light-text-primary',
          '@light:hover:border-@light-primary',
          '@light:focus:border-@light-primary',
          '@dark:bg-@dark-tertiary',
          '@dark:text-@dark-text-primary',
          '@dark:hover:border-@dark-primary',
          '@dark:focus:border-@dark-primary'
        ],
        description: {},
        error: {}
      },
      // 'tertiary' variant
      'tertiary-error': {
        wrapper: {},
        label: {},
        input: [
          'border-2',
          'px-3',
          'py-2',
          'rounded',
          'disabled:text-gray',
          '@light:bg-@light-tertiary',
          '@light:text-@light-text-primary',
          '@light:border-@light-error',
          '@dark:bg-@dark-tertiary',
          '@dark:text-@dark-text-primary',
          '@dark:border-@dark-error'
        ],
        description: {},
        error: {}
      },
      /**
       * Dark variants
       */
      'haiti': {
        wrapper: {},
        label: {},
        input: [
          'border-2',
          'border-transparent',
          'px-3',
          'py-2',
          'rounded',
          'disabled:text-gray',
          '@dark:bg-@dark-haiti',
          '@dark:text-@dark-text-primary',
          '@dark:hover:border-@dark-primary',
          '@dark:focus:border-@dark-primary'
        ],
        description: {},
        error: {}
      },
      'haiti-error': {
        wrapper: {},
        label: {},
        input: [
          'border-2',
          'px-3',
          'py-2',
          'rounded',
          'disabled:text-gray',
          '@dark:bg-@dark-haiti',
          '@dark:text-@dark-text-primary',
          '@dark:border-@dark-error'
        ],
        description: {},
        error: {}
      }
    }
  })),
  (inheritsFromVariant, createVariantExtensions) => ({
    fixedClasses: {},
    classes: {},
    variants: {
      // Generate variants for "fluid" feature
      ...createVariantExtensions((parentName) => `${parentName}-fluid`, {
        wrapper: {
          'w-full': true
        },
        label: {
          'w-full': true
        },
        input: {
          'w-full': true
        },
        description: {
          'w-full': true
        },
        error: {
          'w-full': true
        }
      })
    }
  })
)
