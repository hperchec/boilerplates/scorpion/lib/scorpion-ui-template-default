import TRichSelect from '#components/tailwind/TRichSelect'
import { VariantableOptions } from '#mixins/variantable/'

const TRichSelectStyle = new VariantableOptions(TRichSelect.props)

// Merge from vue-tailwind TRichSelect component
export default VariantableOptions.extend(TRichSelectStyle, (inheritsFromVariant, createVariantExtensions) => ({
  fixedClasses: {
    richSelectContainer: {
      'mb-6': true
    },
    richSelectLabel: {
      'mb-2': true
    },
    richSelectDescription: {
      'mb-2': true,
      'text-sm': true,
      'text-gray-300': true
    },
    richSelectError: {
      'mb-2': true,
      'text-sm': true,
      '@light:text-@light-error': true,
      '@dark:text-@dark-error': true
    },
    dropdownDown: {
      'p-2': true
    }
  },
  classes: {
    richSelectContainer: {},
    richSelectLabel: {},
    richSelectDescription: {},
    richSelectError: {},
    dropdownDown: {}
  },
  variants: {
    // Generate variants for "fluid" feature
    ...createVariantExtensions((parentName) => `${parentName}-fluid`, {
      richSelectContainer: {
        'w-full': true
      },
      richSelectLabel: {
        'w-full': true
      },
      richSelectDescription: {
        'w-full': true
      },
      richSelectError: {
        'w-full': true
      },
      wrapper: {
        'w-full': true
      },
      buttonWrapper: {
        'w-full': true,
        'w-auto': false
      },
      selectButton: {
        'w-full': true,
        'w-auto': false
      }
    }),
    // Generate variants for "fluid-mobile" feature
    ...createVariantExtensions((parentName) => `${parentName}-fluid-mobile`, {
      richSelectContainer: {
        'w-full': true,
        'md:w-auto': true
      },
      richSelectLabel: {
        'w-full': true,
        'md:w-auto': true
      },
      richSelectDescription: {
        'w-full': true,
        'md:w-auto': true
      },
      richSelectError: {
        'w-full': true,
        'md:w-auto': true
      },
      wrapper: {
        'w-full': true,
        'md:w-auto': true
      },
      buttonWrapper: {
        'w-full': true,
        'w-auto': false
      },
      selectButton: {
        'w-full': true,
        'w-auto': false
      }
    })
  }
}))
