import TSelect from '#components/tailwind/TSelect'
import { VariantableOptions } from '#mixins/variantable/'

const originalTSelectStyle = TSelect.props

// vue-tailwind t-select style is one-level object, so we have to wrap it
const TSelectStyle = new VariantableOptions({
  fixedClasses: { select: originalTSelectStyle.fixedClasses },
  classes: { select: originalTSelectStyle.classes },
  variants: Object.keys(originalTSelectStyle.variants).reduce((obj, variant) => {
    obj[variant] = {
      select: originalTSelectStyle.variants[variant]
    }
    return obj
  }, {})
})

// Merge from vue-tailwind TSelect component
export default VariantableOptions.extend(TSelectStyle, (inheritsFromVariant, createVariantExtensions) => ({
  fixedClasses: {
    container: {
      'mb-6': true
    },
    label: {
      'mb-2': true
    },
    description: {
      'mb-2': true,
      'text-sm': true,
      'text-gray-400': true
    },
    error: {
      'mb-2': true,
      'text-sm': true,
      '@light:text-@light-error': true,
      '@dark:text-@dark-error': true
    },
    select: {}
  },
  classes: {
    container: {},
    label: {},
    description: {},
    error: {},
    select: {}
  },
  variants: {
    // Generate variants for "fluid" feature
    ...createVariantExtensions((parentName) => `${parentName}-fluid`, {
      container: {
        'w-full': true
      },
      label: {
        'w-full': true
      },
      description: {
        'w-full': true
      },
      error: {
        'w-full': true
      },
      select: {
        'w-full': true
      }
    }),
    // Generate variants for "fluid-mobile" feature
    ...createVariantExtensions((parentName) => `${parentName}-fluid-mobile`, {
      container: {
        'w-full': true,
        'md:w-auto': true
      },
      label: {
        'w-full': true,
        'md:w-auto': true
      },
      description: {
        'w-full': true,
        'md:w-auto': true
      },
      error: {
        'w-full': true,
        'md:w-auto': true
      },
      select: {
        'w-full': true,
        'md:w-auto': true
      }
    })
  }
}))
