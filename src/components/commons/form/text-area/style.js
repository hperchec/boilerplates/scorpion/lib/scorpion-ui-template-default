import TTextArea from '#components/tailwind/TTextArea'
import { VariantableOptions } from '#mixins/variantable/'

const originalTTextAreaStyle = TTextArea.props

// vue-tailwind t-textarea style is one-level object, so we have to wrap it
const TTextAreaStyle = new VariantableOptions({
  fixedClasses: { textarea: originalTTextAreaStyle.fixedClasses },
  classes: { textarea: originalTTextAreaStyle.classes },
  variants: Object.keys(originalTTextAreaStyle.variants).reduce((obj, variant) => {
    obj[variant] = {
      textarea: originalTTextAreaStyle.variants[variant]
    }
    return obj
  }, {})
})

// Merge from vue-tailwind TTextArea component
export default VariantableOptions.extend(TTextAreaStyle, (inheritsFromVariant, createVariantExtensions) => ({
  fixedClasses: {
    container: {},
    label: {
      'mb-2': true
    },
    textarea: {},
    description: {
      'mb-2': true,
      'text-sm': true,
      'text-gray-400': true
    },
    error: {
      'mb-2': true,
      'text-sm': true
    }
  },
  classes: {
    container: {},
    label: {},
    textarea: {},
    description: {},
    error: {}
  },
  variants: {
    // Generate variants for "fluid" feature
    ...createVariantExtensions((parentName) => `${parentName}-fluid`, {
      container: {
        'w-full': true
      },
      label: {
        'w-full': true
      },
      textarea: {
        'w-full': true
      },
      description: {
        'w-full': true
      },
      error: {
        'w-full': true
      }
    })
  }
}))
