import Page from './Page.vue'
import BaseForm from './form/base-form/BaseForm.vue'
import BaseInput from './form/BaseInput.vue'
import VInput from './form/input/VInput.vue'
import VInputPassword from './form/input/password/VInputPassword.vue'
import VButton from './buttons/VButton.vue'
import VDropdown from './dropdown/VDropdown.vue'
import VDropdownButton from './dropdown-button/VDropdownButton.vue'
import I18nFlagIcon from './icons/I18nFlagIcon.vue'
import VLink from './links/VLink.vue'
import VToggle from './toggle/VToggle.vue'
import VWrappedToggle from './wrapped-toggle/VWrappedToggle.vue'

export default {
  Page,
  BaseForm,
  BaseInput,
  VInput,
  VInputPassword,
  VButton,
  VDropdown,
  VDropdownButton,
  I18nFlagIcon,
  VLink,
  VToggle,
  VWrappedToggle
}
