import { VariantableOptions } from '@/mixins/variantable'
import TToggle from '@/plugins/vue-tailwind/options/TToggle'

const TToggleStyle = new VariantableOptions(TToggle.props)

/* eslint quote-props: ["error", "consistent"] */

// Merge from vue-tailwind TToggle component
export default VariantableOptions.extend(TToggleStyle, (inheritsFromVariant, createVariantExtensions) => ({
  fixedClasses: {
    wrapper: [
      'relative',
      'rounded-full',
      'inline-flex',
      'flex-shrink-0',
      'cursor-pointer',
      'transition-colors',
      'ease-in-out',
      'duration-200',
      'border-2',
      'border-transparent',
      'group',
      'not-checked'
    ],
    wrapperChecked: [
      'relative',
      'rounded-full',
      'inline-flex',
      'flex-shrink-0',
      'cursor-pointer',
      'transition-colors',
      'ease-in-out',
      'duration-200',
      'border-2',
      'border-transparent',
      'group',
      'is-checked'
    ],
    wrapperDisabled: [
      'relative',
      'inline-flex',
      'flex-shrink-0',
      'cursor-pointer',
      'transition-colors',
      'ease-in-out',
      'duration-200',
      'opacity-50',
      'cursor-not-allowed'
    ],
    wrapperCheckedDisabled: [
      'relative',
      'inline-flex',
      'flex-shrink-0',
      'cursor-pointer',
      'transition-colors',
      'ease-in-out',
      'duration-200',
      'opacity-50',
      'cursor-not-allowed',
      'border-2',
      'border-transparent',
      'focus:ring-2',
      'focus:ring-blue-500',
      'focus:outline-none',
      'focus:ring-opacity-50'
    ],
    button: [
      'inline-block',
      'absolute',
      'transform',
      'translate-x-0',
      'transition',
      'ease-in-out',
      'duration-200'
    ],
    buttonChecked: [
      'inline-block',
      'absolute',
      'transform',
      'translate-x-full',
      'transition',
      'ease-in-out',
      'duration-200'
    ],
    checkedPlaceholder: [
      'inline-block',
      'transition-colors',
      'ease-in-out',
      'duration-200'
    ],
    uncheckedPlaceholder: [
      'inline-block',
      'transition-colors',
      'ease-in-out',
      'duration-200'
    ]
  },
  classes: {
    checkedPlaceholder: [
      'rounded-full',
      'w-5',
      'h-5',
      'flex',
      'items-center',
      'justify-center',
      'text-gray-400 text-xs'
    ],
    uncheckedPlaceholder: [
      'rounded-full',
      'w-5',
      'h-5',
      'flex',
      'items-center',
      'justify-center',
      'text-gray-400',
      'text-xs'
    ]
  },
  variants: {
    /**
     * Common variants
     */
    'primary': {
      wrapper: [
        '@light:bg-@light-primary',
        '@light:hover:bg-@light-primary-400',
        '@dark:bg-@dark-primary',
        '@dark:hover:bg-@dark-primary-400'
      ],
      wrapperChecked: [
        '@light:bg-@light-success',
        '@dark:bg-@dark-success'
      ],
      button: [
        'h-[18px]',
        'w-[18px]',
        'top-px',
        'ml-px',
        'rounded-full',
        'shadow',
        'bg-white'
      ],
      buttonChecked: [
        'h-[18px]',
        'w-[18px]',
        'top-px',
        'ml-0.5',
        'rounded-full',
        'shadow',
        'bg-white'
      ]
    },
    'border-primary': {
      wrapper: [
        'bg-transparent',
        '@light:border-@light-primary',
        '@light:hover:border-@light-primary-400',
        '@dark:border-@dark-primary',
        '@dark:hover:border-@dark-primary-400'
      ],
      wrapperChecked: [
        '@light:border-@light-primary',
        '@light:hover:border-@light-primary-400',
        '@dark:border-@dark-primary',
        '@dark:hover:border-@dark-primary-400'
      ],
      button: [
        'h-4',
        'w-4',
        'top-0.5',
        'ml-0.5',
        'rounded-full',
        'shadow',
        '@light:bg-@light-primary',
        '@dark:bg-@dark-primary'
      ],
      buttonChecked: [
        'h-4',
        'w-4',
        'top-0.5',
        'ml-1.5',
        'rounded-full',
        'shadow',
        'bg-white'
      ],
      checkedPlaceholder: [
        'rounded-l-full',
        'm-0.5',
        'mr-0',
        'w-[18px]',
        'h-4',
        'bg-transparent',
        '@light:group-[.is-checked]:bg-@light-primary-300',
        '@dark:group-[.is-checked]:bg-@dark-primary-300'
      ],
      uncheckedPlaceholder: [
        'rounded-r-full',
        'm-0.5',
        'ml-0',
        'w-[18px]',
        'h-4',
        'bg-transparent',
        '@light:group-[.is-checked]:bg-@light-primary-300',
        '@dark:group-[.is-checked]:bg-@dark-primary-300'
      ]
    },
    /**
     * 'light' theme variants
     */
    'alabaster': {
      wrapper: [
        'bg-@light-alabaster',
        'hover:bg-@light-alabaster-400'
      ],
      wrapperChecked: [
        'bg-@light-success'
      ],
      button: [
        'h-[18px]',
        'w-[18px]',
        'top-px',
        'ml-px',
        'rounded-full',
        'shadow',
        'bg-white'
      ],
      buttonChecked: [
        'h-[18px]',
        'w-[18px]',
        'top-px',
        'ml-0.5',
        'rounded-full',
        'shadow',
        'bg-white'
      ]
    },
    'border-alabaster': {
      wrapper: [
        'bg-transparent',
        'border-@light-alabaster',
        'hover:border-@light-alabaster-400'
      ],
      wrapperChecked: [
        'border-@light-alabaster',
        'hover:border-@light-alabaster-400'
      ],
      button: [
        'h-4',
        'w-4',
        'top-0.5',
        'ml-0.5',
        'rounded-full',
        'shadow',
        'bg-@light-alabaster'
      ],
      buttonChecked: [
        'h-4',
        'w-4',
        'top-0.5',
        'ml-1.5',
        'rounded-full',
        'shadow',
        'bg-white'
      ],
      checkedPlaceholder: [
        'rounded-l-full',
        'm-0.5',
        'mr-0',
        'w-[18px]',
        'h-4',
        'bg-transparent',
        'group-[.is-checked]:bg-@light-alabaster-300'
      ],
      uncheckedPlaceholder: [
        'rounded-r-full',
        'm-0.5',
        'ml-0',
        'w-[18px]',
        'h-4',
        'bg-transparent',
        'group-[.is-checked]:bg-@light-alabaster-300'
      ]
    },
    /**
     * 'dark' theme variants
     */
    'haiti': {
      wrapper: [
        'bg-@dark-haiti',
        'hover:bg-@dark-haiti-400'
      ],
      wrapperChecked: [
        'bg-@dark-success'
      ],
      button: [
        'h-[18px]',
        'w-[18px]',
        'top-px',
        'ml-px',
        'rounded-full',
        'shadow',
        'bg-@dark-primary'
      ],
      buttonChecked: [
        'h-[18px]',
        'w-[18px]',
        'top-px',
        'ml-0.5',
        'rounded-full',
        'shadow',
        'bg-white'
      ]
    },
    'border-haiti': {
      wrapper: [
        'bg-transparent',
        'border-@dark-haiti',
        'hover:border-@dark-haiti-400'
      ],
      wrapperChecked: [
        'border-@dark-haiti',
        'hover:border-@dark-haiti-400'
      ],
      button: [
        'h-4',
        'w-4',
        'top-0.5',
        'ml-0.5',
        'rounded-full',
        'shadow',
        'bg-@dark-haiti'
      ],
      buttonChecked: [
        'h-4',
        'w-4',
        'top-0.5',
        'ml-1.5',
        'rounded-full',
        'shadow',
        'bg-white'
      ],
      checkedPlaceholder: [
        'rounded-l-full',
        'm-0.5',
        'mr-0',
        'w-[18px]',
        'h-4',
        'bg-transparent',
        'group-[.is-checked]:bg-@dark-haiti-300'
      ],
      uncheckedPlaceholder: [
        'rounded-r-full',
        'm-0.5',
        'ml-0',
        'w-[18px]',
        'h-4',
        'bg-transparent',
        'group-[.is-checked]:bg-@dark-haiti-300'
      ]
    },
    'mirage': {
      wrapper: [
        'bg-@dark-mirage',
        'hover:bg-@dark-mirage-400'
      ],
      wrapperChecked: [
        'bg-@dark-success'
      ],
      button: [
        'h-[18px]',
        'w-[18px]',
        'top-px',
        'ml-px',
        'rounded-full',
        'shadow',
        'bg-white'
      ],
      buttonChecked: [
        'h-[18px]',
        'w-[18px]',
        'top-px',
        'ml-0.5',
        'rounded-full',
        'shadow',
        'bg-white'
      ]
    },
    'border-mirage': {
      wrapper: [
        'bg-transparent',
        'border-@dark-mirage',
        'hover:border-@dark-mirage-400'
      ],
      wrapperChecked: [
        'border-@dark-mirage',
        'hover:border-@dark-mirage-400'
      ],
      button: [
        'h-4',
        'w-4',
        'top-0.5',
        'ml-0.5',
        'rounded-full',
        'shadow',
        'bg-@dark-mirage'
      ],
      buttonChecked: [
        'h-4',
        'w-4',
        'top-0.5',
        'ml-1.5',
        'rounded-full',
        'shadow',
        'bg-white'
      ],
      checkedPlaceholder: [
        'rounded-l-full',
        'm-0.5',
        'mr-0',
        'w-[18px]',
        'h-4',
        'bg-transparent',
        'group-[.is-checked]:bg-@dark-mirage-300'
      ],
      uncheckedPlaceholder: [
        'rounded-r-full',
        'm-0.5',
        'ml-0',
        'w-[18px]',
        'h-4',
        'bg-transparent',
        'group-[.is-checked]:bg-@dark-mirage-300'
      ]
    }
  }
}))
