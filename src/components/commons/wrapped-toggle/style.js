import { VariantableOptions } from '@/mixins/variantable'
import TToggle from '@/plugins/vue-tailwind/options/TToggle'

const TToggleStyle = new VariantableOptions(TToggle.props)

/* eslint quote-props: ["error", "consistent"] */

// Merge from vue-tailwind TToggle component
export default VariantableOptions.extend(TToggleStyle, (inheritsFromVariant, createVariantExtensions) => ({
  fixedClasses: {
    topWrapper: {
      'border': true,
      '@dark:border-transparent': true,
      'flex': true,
      'justify-start': true,
      'items-center': true,
      'gap-2': true,
      'px-4': true,
      'py-2': true,
      'cursor-pointer': true,
      'rounded': true,
      'select-none': true
    }
  },
  classes: {
    topWrapper: {
      // ...
    }
  },
  variants: {
    'transparent': {
      topWrapper: {
        'bg-transparent': true,
        '@dark:hover:bg-@dark-haiti-100': true,
        '@light:hover:bg-@light-alabaster-600': true
      },
      wrapper: {
        'bg-@dark-haiti': true
      },
      wrapperChecked: {
        'bg-@dark-success': true
      }
    },
    'primary': {
      topWrapper: {
        '@dark:bg-@dark-primary': true,
        '@dark:hover:bg-@dark-primary-400': true
      },
      wrapper: {
        'bg-@dark-black-rock': true
      }
    },
    'alabaster': {
      topWrapper: {
        '@light:bg-@light-alabaster': true,
        'hover:bg-@light-primary-100': true,
        'shadow-md': true
      },
      wrapper: {
        'bg-@light-black-rock': true
      },
      wrapperChecked: {
        'bg-@light-success': true
      }
    },
    'white-smoke': {
      topWrapper: {
        'bg-@light-white-smoke': true,
        'hover:bg-@light-white-smoke': true,
        'border-@light-white-smoke-600': true,
        'hover:border-@light-primary': true
      },
      wrapper: {
        'bg-@light-white-smoke-600': true
      },
      wrapperChecked: {
        'bg-@dark-success': true
      }
    },
    'white': {
      topWrapper: {
        'bg-white': true,
        'border-@light-white-smoke-600': true,
        'hover:border-@light-primary': true
        // 'border-dashed': true
      },
      wrapper: {
        'bg-@light-white-smoke-500': true
      },
      wrapperChecked: {
        'bg-@dark-success': true
      }
    },
    'haiti': {
      topWrapper: {
        'bg-@dark-mirage': true,
        'hover:bg-@dark-mirage-400': true
      },
      wrapper: {
        'bg-@dark-black-rock': true
      }
    },
    'black-rock': {
      topWrapper: {
        'bg-@dark-black-rock': true,
        'hover:bg-@dark-black-rock-600': true
      },
      wrapper: {
        'bg-@dark-haiti': true
      },
      wrapperChecked: {
        'bg-@dark-success': true
      }
    },
    'mirage': {
      topWrapper: {
        'bg-@dark-mirage': true,
        'hover:bg-@dark-primary-500': true
      },
      wrapper: {
        'bg-@dark-haiti': true
      },
      wrapperChecked: {
        'bg-@dark-success': true
      }
    },
    'mini-haiti': inheritsFromVariant('haiti', {
      topWrapper: {
        'bg-@dark-mirage': false,
        'bg-@dark-black-rock-200': true
      }
    })
  }
}))
