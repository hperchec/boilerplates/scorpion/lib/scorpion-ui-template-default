import { Core, toolbox } from '@hperchec/scorpion-ui'

import App from './App.vue'
import commons from './commons'
import layouts from './layouts'
import shared from './shared'
import publicComponents from './public'
import privateComponents from './private'
import './modals'
import './vendor/vue-awesome'

const { mergeContext } = toolbox.template.utils

const components = {
  App,
  commons,
  layouts,
  shared,
  public: publicComponents,
  private: privateComponents
}

mergeContext(Core.context.vue, 'components', components)

export default components
