import publicLayout from './public'
import privateLayout from './private'
import defaultLayout from './default'

export default {
  default: defaultLayout,
  public: publicLayout,
  private: privateLayout
}
