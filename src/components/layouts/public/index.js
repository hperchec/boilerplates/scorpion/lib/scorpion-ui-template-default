import partials from './partials'
import Layout from './Layout.vue'

export default {
  partials,
  Layout
}
