import header from './header'
import sideBar from './side-bar'

export default {
  header,
  'side-bar': sideBar
}
