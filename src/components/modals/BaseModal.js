import { Core } from '@hperchec/scorpion-ui'

/**
 * **BaseModal** component
 */
export default {
  ...Core.context.vue.components.modals.BaseModal
}
