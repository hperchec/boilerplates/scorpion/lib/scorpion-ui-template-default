import { Core } from '@hperchec/scorpion-ui'

import BaseModal from './BaseModal'
import VModal from './VModal.vue'
// import GlobalErrorModal from './global-error/GlobalErrorModal.vue'
// import ConfirmationModal from './confirmation/ConfirmationModal.vue'
import AboutModal from './about/AboutModal.vue'

// Define useTransition option
Core.context.services['modal-manager'].options.useTransition = {
  mode: 'in-out',
  enterActiveClass: '_veryfast animated fadeIn',
  leaveActiveClass: '_veryfast animated fadeOut'
}

console.log('Core.context.services[modal-manager].options.useTransition', Core.context.services['modal-manager'].options.useTransition)
// Modals components
Core.context.vue.components.modals.BaseModal = BaseModal
Core.context.vue.components.modals.Modal = VModal

const Modal = Core.context.services['modal-manager'].Modal

// Register modals
Core.context.services['modal-manager'].options.modals.push(new Modal('AboutModal', AboutModal))
// Core.context.services['modal-manager'].options.modals.push(new Modal('ConfirmationModal', ConfirmationModal, { layer: 3 }))
// Core.context.services['modal-manager'].options.modals.push(new Modal('GlobalErrorModal', GlobalErrorModal, { layer: 'foreground' }))
