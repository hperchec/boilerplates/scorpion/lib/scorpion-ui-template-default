import { VariantableOptions } from '@/mixins/variantable'
import TModal from '@/plugins/vue-tailwind/options/TModal'

const TModalStyle = new VariantableOptions(TModal.props)

/* eslint quote-props: ["error", "consistent"] */

// Merge from vue-tailwind TModal component
export default VariantableOptions.extend(TModalStyle, (inheritsFromVariant, createVariantExtensions) => ({
  fixedClasses: {
    overlay: [], // No classes for overlay -> see Modal component
    wrapper: [
      'relative',
      'mx-auto',
      'max-w-lg',
      'px-3',
      'py-12'
    ],
    modal: [
      'relative',
      'rounded',
      'overflow-visible'
    ],
    body: [
      'p-6'
    ],
    header: [
      'p-6',
      'border-b',
      'rounded-t'
    ],
    footer: [],
    close: [
      'flex',
      'items-center',
      'justify-center',
      'absolute',
      'right-0',
      'top-0',
      'h-8',
      'w-8',
      'transition',
      'duration-100',
      'ease-in-out',
      'focus:ring-2',
      'focus:ring-blue-500',
      'focus:outline-none',
      'focus:ring-opacity-50'
    ]
  },
  classes: {
    overlay: [
      'bg-black'
    ],
    wrapper: [],
    modal: [
      '@light:bg-@light-alabaster',
      '@light:text-@light-text-primary',
      '@dark:bg-@dark-mirage',
      '@dark:text-@dark-text-primary',
      'shadow'
    ],
    header: [
      'border-gray-100'
    ],
    footer: [
      'pb-6',
      'px-6',
      'rounded-b'
    ],
    close: [
      '@light:bg-@light-white-smoke',
      '@light:hover:bg-@light-white-smoke-400',
      '@dark:bg-@dark-mirage',
      '@dark:hover:bg-@dark-mirage-400'
    ],
    closeIcon: [
      'h-4',
      'w-4',
      'fill-current'
    ],
    overlayEnterClass: [
      'opacity-0'
    ],
    overlayEnterActiveClass: [
      'transition',
      'ease-out',
      'duration-100'
    ],
    overlayEnterToClass: [
      'opacity-100'
    ],
    overlayLeaveClass: [
      'opacity-100'
    ],
    overlayLeaveActiveClass: [
      'transition',
      'ease-in',
      'duration-75'
    ],
    overlayLeaveToClass: [
      'opacity-0'
    ],
    enterClass: [],
    enterActiveClass: [],
    enterToClass: [],
    leaveClass: [],
    leaveActiveClass: [],
    leaveToClass: []
  },
  variants: {
    small: {
      wrapper: [
        'relative',
        'mx-auto',
        'lg:max-w-2xl',
        'lg:px-3',
        'lg:py-12'
      ]
    },
    right: {
      overlay: [
        '@dark:bg-@dark-baltic-sea'
      ],
      wrapper: [
        'relative',
        'mx-auto',
        'lg:max-w-2xl',
        'lg:px-3',
        'lg:pb-12',
        'lg:pt-6'
      ],
      modal: [
        'p-4'
      ]
    }
  }
}))
