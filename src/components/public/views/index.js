import components from './components'
import home from './home'
import login from './login'

export default {
  components,
  home,
  login
}
