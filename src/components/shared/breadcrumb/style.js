export default {
  fixedClasses: {
    container: [
      // ...
    ],
    item: [
      // ...
    ],
    itemText: [
      // ...
    ],
    separator: [
      // ...
    ]
  },
  classes: {
    container: [
      'flex',
      'w-full',
      'pt-2',
      'pb-3',
      'text-sm',
      'border-b',
      '@dark:border-@dark-mirage',
      '@light:border-@light-primary-900'
    ],
    item: [
      'flex'
    ],
    itemText: [
      'pr-2',
      'whitespace-nowrap'
    ],
    separator: [
      'select-none',
      'pr-2'
    ]
  },
  variants: {
    // ...
  }
}
