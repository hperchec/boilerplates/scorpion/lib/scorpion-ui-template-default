import Breadcrumb from './breadcrumb/Breadcrumb.vue'
import LangDropdown from './lang-dropdown/LangDropdown.vue'
import ThemeDropdown from './theme-dropdown/ThemeDropdown.vue'
import ThemeToggle from './theme-toggle/ThemeToggle.vue'

export default {
  Breadcrumb,
  LangDropdown,
  ThemeDropdown,
  ThemeToggle
}
