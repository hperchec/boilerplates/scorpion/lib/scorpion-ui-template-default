import { Core } from '@hperchec/scorpion-ui'
// vue-awesome
import Icon from 'vue-awesome/components/Icon'
import 'vue-awesome/icons/cube'
import 'vue-awesome/icons/eye'
import 'vue-awesome/icons/home'
import 'vue-awesome/icons/moon'
import 'vue-awesome/icons/paint-brush'
import 'vue-awesome/icons/plus'
import 'vue-awesome/icons/plus-circle'
import 'vue-awesome/icons/question'
import 'vue-awesome/icons/sort-down'
import 'vue-awesome/icons/sort-up'
import 'vue-awesome/icons/sun'

const Vue = Core.Vue

/**
 * Global Vue component
 */
Vue.component('v-icon', Icon)
