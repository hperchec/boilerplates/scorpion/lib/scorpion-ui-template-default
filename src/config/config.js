import globals from './globals.json'

export default {
  globals,
  /**
   * System
   */
  system: {
    /**
     * Default theme
     * @type {string}
     * @default 'auto'
     * @description Can be 'light', 'dark', 'auto', or any app theme identifier (kebabcase).
     * See also ->
     */
    defaultTheme: 'dark', // 'light', 'dark' or 'auto'
    /**
     * Fallback theme
     * @type {string}
     * @default 'light''
     * @description Fallback theme (!! Cannot be 'auto' !!)
     * (based on available themes defined in the globals.config.json file)
     */
    fallbackTheme: 'light' // '<theme>' (in kebab-case format)
  },
  /**
   * Authentication
   */
  auth: {
    /**
     * oAuth access token identifier (in browser LocalStorage)
     * @default 'app_access_token'
     * @type {string}
     */
    accessTokenKey: 'app_access_token',
    /**
     * oAuth refresh token identifier (in browser LocalStorage)
     * @default 'app_refresh_token'
     * @type {string}
     */
    refreshTokenKey: 'app_refresh_token'
  },
  /**
   * Tailwind
   */
  tailwind: {
    /**
     * Tailwind configuration file
     * See also tailwind documentation (https://tailwindcss.com/docs/configuration)
     * @type {object}
     * @default undefined
     */
    config: undefined
  }
}
