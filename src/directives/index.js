import { Core, toolbox } from '@hperchec/scorpion-ui'
import vueRippleDirective from './vue-ripple-directive'

const { mergeContext } = toolbox.template.utils

const directives = {
  vueRippleDirective
}

// Directives
mergeContext(Core.context.vue, 'directives', directives)

export default directives
