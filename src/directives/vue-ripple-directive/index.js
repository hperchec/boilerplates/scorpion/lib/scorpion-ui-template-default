import Ripple from 'vue-ripple-directive'
import options from './options'

export default {
  /**
   * Function to create directive
   * @param {object} options - Options
   * @returns {Function} Returns the directive constructor
   */
  createDirective: function (options) {
    // Apply options
    Ripple.color = options.color
    Ripple.zIndex = options.zIndex
    // Return ripple
    return Ripple
  },
  /**
   * createDirective options
   */
  options
}
