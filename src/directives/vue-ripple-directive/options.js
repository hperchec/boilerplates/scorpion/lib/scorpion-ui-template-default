/**
 * vue-ripple-directive global options
 * See also: https://www.npmjs.com/package/vue-ripple-directive
 */
export default {
  /**
   * The directive id. Will be => 'v-ripple'
   */
  id: 'ripple',
  /**
   * vue-ripple-directive option
   */
  color: 'rgba(255, 255, 255, 0.35)', // Default
  zIndex: 55 // Default
}
