import auth from './auth.json'
import global from './global.json'
import models from './models.json'
import views from './views.json'

export default {
  ...auth,
  ...global,
  ...models,
  ...views
}
