import { Core } from '@hperchec/scorpion-ui'

// Internationalization
import './listeners'
// Internationalization
import './i18n'
// Router configuration
import './router'
// Vue components
import './components'
// Vue directives
import './directives'
// Vue mixins
import './mixins'
// Vue plugins
import './plugins'
// Root instance extension
import './root'
// Style
import './styles/main.scss'

// Config
import config from './config/config'

/**
 * Custom configuration keys
 */
Core.configure(config)
