import { Core } from '@hperchec/scorpion-ui'

/**
 * EventBus Service is created now. Do logic...
 */
Core.onServiceCreated('event-bus', (eventBus) => {
  /**
   * Init Router/ROUTER_VIEW_UPDATED event listener
   * @param {object} payload
   * @param {object} payload.ref - The <router-view> component ref
   */
  eventBus.on('Router/ROUTER_VIEW_UPDATED', ({ ref }) => {
    // ...
  })

  /**
   * Init Router/ROUTING_TRANSITION_START event listener
   * @param {object} payload
   * @param {object} payload.from - The "from" route
   * @param {object} payload.to - The "to" route
   * @param {Element} payload.element - The DOM element on which transition event is fired
   */
  eventBus.on('Router/ROUTING_TRANSITION_START', ({ from, to, element }) => {
    // ...
  })

  /**
   * Init Router/ROUTING_TRANSITION_END event listener
   * @param {object} payload
   * @param {object} payload.from - The "from" route
   * @param {object} payload.to - The "to" route
   * @param {Element} payload.element - The DOM element on which transition event is fired
   */
  eventBus.on('Router/ROUTING_TRANSITION_END', ({ from, to, event }) => {
    // ...
  })
})
