import { toolbox } from '@hperchec/scorpion-ui'

const { CSSClasses } = toolbox.css

/**
 * **ExtractsCSSClasses** mixin
 *
 * Add a method to component that extracts root element CSS classes
 * (from this.$vnode.data.staticClass & this.$vnode.data.class)
 */
export const ExtractsCSSClasses = {
  methods: {
    /**
     * Get root element CSS classes
     * @returns {object} - A class attribute compatible object
     */
    getRootElementCSSClasses () {
      const staticClasses = new CSSClasses(this.$vnode.data.staticClass || []).toObject()
      const classes = new CSSClasses(this.$vnode.data.class || []).toObject()
      return {
        ...staticClasses,
        ...classes
      }
    }
  }
}

export default ExtractsCSSClasses
