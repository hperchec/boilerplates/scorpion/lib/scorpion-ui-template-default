import { Core, toolbox } from '@hperchec/scorpion-ui'

import ExtractsCSSClasses from './extracts-css-classes'
import { Variantable, VariantableOptions } from './variantable'
import VModelable from './v-modelable'

const { mergeContext } = toolbox.template.utils

const mixins = {
  'extracts-css-classes': ExtractsCSSClasses,
  variantable: { Variantable, VariantableOptions },
  'v-modelable': VModelable
}

// Define custom mixins
mergeContext(Core.context.vue, 'mixins', mixins)
