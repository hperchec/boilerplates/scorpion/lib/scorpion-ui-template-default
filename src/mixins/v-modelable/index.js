// Default model option
const defaultModelOption = {
  prop: 'value', // Default prop: value
  event: 'input', // Default event: 'input'
  mirrorEvents: [] // Custom prop. Default empty array
}

/**
 * **VModelable** mixin
 *
 * Make a component 'v-modelable': simulate the Vue *v-model* syntaxic sugar feature
 * (see also [Vue v-model documentation](https://fr.vuejs.org/v2/guide/forms.html)
 * & [how to customize component v-model](https://v2.vuejs.org/v2/guide/components-custom-events.html#Customizing-Component-v-model))
 */
export default {
  props: {
    /**
     * formatter method
     */
    formatter: {
      type: Function,
      default: function (value) {
        return value // default returns value itself
      }
    }
  },
  beforeCreate () {
    // Init component props
    this.$options.props = this.$options.props || {}
    // Merge model option
    this.$options.model = {
      ...defaultModelOption,
      ...this.$options.model
    }
    // Get model prop
    const modelProp = this.$options.model.prop
    // Set model prop
    this.$options.props[modelProp] = this.$options.props[modelProp] || modelProp // Set prop with its own prop name
    // Get model event
    const modelEvent = this.$options.model.event
    // Get mirror events
    const mirrorEvents = this.$options.model.mirrorEvents
    // Set computed vModel
    this.$options.computed.vModel = {
      get () {
        return this[modelProp]
      },
      set (val) {
        // Format value
        const formattedValue = val
        // Emit event for top level (v-model)
        this.$emit(modelEvent, formattedValue)
        // If mirror events, loop on event names
        for (const eventName of mirrorEvents) {
          this.$emit(eventName, formattedValue)
        }
      }
    }
  }
}
