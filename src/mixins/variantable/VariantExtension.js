import { parseStyleDescriptor, mergeStyleDescriptors } from './parse-style-descriptor'

/**
 * @classdesc
 * VariantExtension class for variantable mixin
 */
class VariantExtension {
  /**
   * Creates new instance
   * @param {string} parentName - The parent variant name
   * @param {object} overrides - An object to override parent variant
   */
  constructor (parentName, overrides) {
    this.parentName = parentName
    this.overrides = parseStyleDescriptor(overrides).parsed
  }

  /**
   * Returns an object that is the result of parent object merged with this.overrides
   * @param {object} parent - The parent variant object
   * @param {string} [format = 'object'] - The format
   * @returns {object} The merged variant object
   */
  extendParent (parent, format = 'object') {
    return mergeStyleDescriptors(parent, this.overrides, format)
  }
}

export default VariantExtension
