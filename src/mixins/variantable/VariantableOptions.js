import { utils, toolbox } from '@hperchec/scorpion-ui'

import parseStyleDescriptor from './parse-style-descriptor'
import VariantExtension from './VariantExtension'

const { deepMerge, isPlainObject, isDef, isNull } = utils
const { CCSClasses } = toolbox.css

/**
 * @classdesc
 * VariantableOptions class for variantable mixin
 */
class VariantableOptions {
  /**
   * Creates new instance if obj are valid
   * @param {object} obj - Options
   * @param {string|object} obj.fixedClasses - Fixed classes
   * @param {string|object} obj.classes - Default classes
   * @param {object} [obj.variants = {}] - Variants
   * @param {object} [options = {}] - Options
   * @param {string} [options.cssClassesFormat = object] - The format for css classes: can be 'string', 'array' or 'object'
   */
  constructor (obj, options = {}) {
    const defaultOptions = {
      cssClassesFormat: 'object'
    }
    options = {
      ...defaultOptions,
      ...options
    }
    /**
     * Check if fixedClasses property is defined
     */
    if (isDef(obj.fixedClasses)) {
      const { parsed: parsedFixedClasses, elements: fixedClassesElements } = parseStyleDescriptor(obj.fixedClasses, options.cssClassesFormat)
      // Check to match required schema
      if (!parsedFixedClasses) {
        throw new Error('VariantableOptions "fixedClasses" property does not match the required schema.')
      } else {
        this.fixedClasses = parsedFixedClasses
        // Add element keys to local cache
        if (!isNull(fixedClassesElements)) {
          for (const elementKey of fixedClassesElements) this.#elementKeys.add(elementKey)
        }
      }
    // Else, fixedClasses not defined
    } else {
      throw new Error('VariantableOptions must have "fixedClasses" property defined.')
    }
    /**
     * Check if classes property is defined
     */
    if (isDef(obj.classes)) {
      const { parsed: parsedClasses, elements: classesElements } = parseStyleDescriptor(obj.classes, options.cssClassesFormat)
      // Check to match required schema
      if (!parsedClasses) {
        throw new Error('VariantableOptions "classes" property does not match the required schema.')
      } else {
        this.classes = parsedClasses
        // Add element keys to local cache
        if (!isNull(classesElements)) {
          for (const elementKey of classesElements) this.#elementKeys.add(elementKey)
        }
      }
    // Else, set as empty object
    } else {
      this.classes = {}
    }
    /**
     * Check if variants property is Object if defined
     */
    if (isDef(obj.variants)) {
      if (typeof obj.variants !== 'object') {
        throw new Error('VariantableOptions "variants" property must be an object.')
      } else {
        const variants = {}
        const variantExtensions = {}
        // Loop on variants object properties
        for (const variantName in obj.variants) {
          const variantObj = obj.variants[variantName]
          // variantObj can be a function, so we can do `extend('parent-variant', { ... })`
          if (variantObj instanceof VariantExtension) {
            // Save extension to set after processing base variants
            variantExtensions[variantName] = variantObj
          } else {
            const { parsed: parsedVariantObj, elements: variantObjElements } = parseStyleDescriptor(variantObj, options.cssClassesFormat)
            // Check to match required schema
            if (!parsedVariantObj) {
              throw new Error(`VariantableOptions "variants" property: variant object for "${variantName}" does not match the required schema.`)
            } else {
              variants[variantName] = parsedVariantObj
              // Add element keys to local cache
              if (!isNull(variantObjElements)) {
                for (const elementKey of variantObjElements) this.#elementKeys.add(elementKey)
              }
            }
          }
        }
        this.variants = variants
        this.variantExtensions = variantExtensions
      }
    } else {
      this.variants = {}
      this.variantExtensions = {}
    }
    this.syncVariantExtensions(options.cssClassesFormat)
  }

  /**
   * Public properties
   */

  /**
   * fixedClasses
   * @category properties
   * @description Fixed classes
   * @type {?object}
   */
  fixedClasses = undefined

  /**
   * classes
   * @category properties
   * @description Classes
   * @type {?object}
   */
  classes = undefined

  /**
   * variants
   * @category properties
   * @description Variants
   * @type {?object}
   */
  variants = undefined

  /**
   * variantExtensions
   * @category properties
   * @description Variant extensions
   * @type {?object}
   */
  variantExtensions = undefined

  /**
   * Private properties
   */

  /**
   * elementKeys
   * @category properties
   * @description Element keys
   * @type {Set}
   */
  #elementKeys = new Set()

  /**
   * Public methods
   */

  /**
   * Process variant extensions and injects it into this.variants
   * @param {string} [format = object] - The optional format default object
   */
  syncVariantExtensions (format = 'object') {
    // Then process variant extensions
    for (const variantName in this.variantExtensions) {
      const extension = this.variantExtensions[variantName]
      this.variants[variantName] = extension.extendParent(this.variants[extension.parentName], format)
    }
  }

  /**
   * Returns array of variant names
   * @returns {string[]} A list of variants
   */
  getVariants () {
    return VariantableOptions.getVariantsFromOptions(this)
  }

  /**
   * Returns true if variant exists
   * @param {string} variant - The expected variant
   * @returns {boolean} True if variant exists
   */
  hasVariant (variant) {
    return this.getVariants().includes(variant)
  }

  /**
   * Returns true if has at least one element key set
   * @returns {string[]} The element keys
   */
  getElementKeys () {
    return [ ...this.#elementKeys ]
  }

  /**
   * Returns true if has at least one element key set
   * @returns {boolean} True if has at least one element key set
   */
  hasNestedElements () {
    return Boolean(this.#elementKeys.size)
  }

  /**
   * Returns css classes
   * @param {string} [element = undefined] - The element name
   * @param {string} [variant = undefined] - The variant. If not specified, returns default classes
   * @returns {CCSClasses} An instance of CSSClasses
   */
  getElementCssClass (element, variant) {
    return VariantableOptions.getElementCssClassFromOptions(this, element, variant)
  }

  /**
   * Static methods
   */

  /**
   * Returns the variant names as array
   * @param {VariantableOptions} options - Options
   * @returns {string[]} The variant names as array
   */
  static getVariantsFromOptions (options) {
    if (!(options instanceof this)) throw new Error('VariantableOptions "getVariantsFromOptions" static method requires an instance as first parameter')
    return Object.keys(options.variants)
  }

  /**
   * Get css classes as string
   * @param {VariantableOptions} options - Options
   * @param {string} [element = undefined] - The element name
   * @param {string} [variant = undefined] - The variant. If not specified, returns default classes
   * @returns {string} The classes as string
   */
  static getElementCssClassFromOptions (options, element, variant) {
    if (!(options instanceof this)) throw new Error('VariantableOptions "getElementCssClassFromOptions" static method requires an instance as first parameter')
    if (element && typeof element !== 'string') throw new Error('VariantableOptions "getElementCssClassFromOptions" static method requires string as second parameter ("element")')
    if (variant && typeof variant !== 'string') throw new Error('VariantableOptions "getElementCssClassFromOptions" static method requires string as third parameter ("variant")')
    // Check if variant exists
    if (variant && !options.hasVariant(variant)) {
      throw new Error(`VariantableOptions "variants" property does not include "${variant}".`)
    }
    const fixedClasses = element
      ? options.fixedClasses[element]
      : options.fixedClasses
    const defaultClasses = variant ? options.variants[variant] : options.classes
    const classes = element
      ? defaultClasses[element]
      : defaultClasses
    return fixedClasses + ' ' + classes
  }

  /**
   * Merge two instance of VariantableOptions
   * @param {VariantableOptions} a - From value
   * @param {VariantableOptions} b - To value
   * @returns {VariantableOptions} A new instance of VariantableOptions
   */
  static merge (a, b) {
    return new this(deepMerge(a, b, {
      isMergeableObject: isPlainObject // merge only plain object because variants can be VariantExtension typed
    }))
  }

  /**
   * Extend an instance of VariantableOptions
   * @param {VariantableOptions} varOptions - The variantable options to extend
   * @param {Function} processor - A processor function
   * @returns {VariantableOptions} A new instance of VariantableOptions
   */
  static extend (varOptions, processor) {
    // @ts-ignore
    const inheritsFromVariant = (...args) => new VariantExtension(...args)
    const createVariantExtensions = (nameBuilder, overrides) => {
      const generatedVariants = {}
      for (const variantName of varOptions.getVariants()) {
        const name = nameBuilder(variantName)
        if (name === variantName) {
          throw new Error('createVariantExtensions: name cannot be the same as parent variant')
        }
        generatedVariants[name] = new VariantExtension(variantName, overrides)
      }
      return generatedVariants
    }
    // const defaultProcessor = (inheritsFromVariant, createVariantExtensions) => ({}) // Returns empty object
    return this.merge(varOptions, processor(inheritsFromVariant, createVariantExtensions))
  }
}

export default VariantableOptions
