import { utils, toolbox } from '@hperchec/scorpion-ui'
import VariantableOptions from './VariantableOptions'

export { VariantableOptions }

const { isDef } = utils
const { CSSClasses } = toolbox.css

/**
 * **Variantable** mixin
 *
 * Make a component 'variantable': it means that the component can be stylized with variants
 * This feature is similar to [vue-tailwind](https://www.vue-tailwind.com/) components
 */
export const Variantable = {
  style: {
    fixedClasses: {},
    classes: {},
    variants: {}
  },
  props: {
    /**
     * Variant
     */
    variant: {
      type: String,
      default: ''
      // Validator to check if variant key is set
    },
    /**
     * Overrides variantable style
     * @type {?object|Function}
     * @example
     * :v-style="{
     *   wrapper: (classes, { add, del, replace }) => {
     *     classes['mb-2'] = false
     *   }
     * }"
     */
    vStyle: {
      type: [ Object, Function ],
      default: undefined
    }
  },
  computed: {
    /**
     * Returns the overidden style based on this.style and "vStyle" prop
     * @returns {VariantableOptions} - The VariantableOptions instance
     */
    overriddenStyle: function () {
      const elementKeys = this.style.getElementKeys()
      // Function to visit each classes object
      const visitor = (classes) => {
        let _classes = classes // The classes to pass to vStyle function
        const classVisitorUtils = {
          add: (_class) => { _classes[_class] = true },
          del: (_class) => { _classes[_class] = false },
          replace: (from, to) => { _classes[from] = false; _classes[to] = true }
        }
        // First, apply style overrides
        if (this.vStyle) {
          // If object
          if (this.style.hasNestedElements()) {
            for (const elementKey in this.vStyle) {
              if (!classes[elementKey]) {
                // Check if element is defined
                if (!elementKeys.includes(elementKey)) throw new Error(`Variantable mixin: the element "${elementKey}" is not defined`)
                // Set empty object by default
                classes[elementKey] = {}
              }
              _classes = classes[elementKey]
              this.vStyle[elementKey](_classes, classVisitorUtils)
            }
          } else {
            // Else -> vStyle is function
            this.vStyle(_classes, classVisitorUtils)
          }
        }
        return classes
      }

      const result = new VariantableOptions(this.style)

      visitor(result.fixedClasses)
      visitor(result.classes)
      for (const variant of result.getVariants()) {
        visitor(result.variants[variant])
      }

      return result
    }
  },
  methods: {
    /**
     * getElementCssClass
     * @public
     * @param {string} element - The element (optional, null/undefined is root element)
     * @param {object} [options = {}] - The options
     * @param {string} [options.format = 'object'] - The returned format: can be 'string', 'array' or 'object' (default: 'object')
     * @param {Function} [options.visit] - A visitor function
     * @returns {string|string[]|object} Can be 'string', 'array' or 'object' depending on options.format
     * @description
     * Get classes for an element.
     *
     * If the component does not have children elements:
     *
     * ```js
     * style: {
     *   fixedClasses: 'p-1 m-1',
     *   classes: 'text-white',
     *   variants: {
     *     myVariant: 'text-gray'
     *   }
     * }
     * ```
     *
     * Assum we didn't have specify variant, call method without argument:
     *
     * ```js
     * this.getElementCssClass() // => 'p-1 m-1 text-white'
     * ```
     *
     * With children elements:
     *
     * ```js
     * style: {
     *   fixedClasses: { wrapper: 'p-1 m-1' },
     *   classes: { wrapper: 'text-white' },
     *   variants: {
     *     myVariant: { wrapper: 'text-gray' }
     *   }
     * }
     * ```
     *
     * In that case, call method by specifying element name:
     *
     * ```js
     * this.getElementCssClass('wrapper') // => 'p-1 m-1 text-white'
     * this.getElementCssClass() // => error =(
     * ```
     */
    getElementCssClass (element, options) {
      const defaultOptions = {
        format: 'object',
        visit: val => val
      }
      options = {
        ...defaultOptions,
        ...options
      }

      const visitor = options.visit

      // Get overridden style
      const style = this.overriddenStyle

      const variant = style.variants[this.variant]
      const fixedClasses = element ? style.fixedClasses[element] : style.fixedClasses
      const defaultClasses = style.classes
      const classes = element ? (variant || defaultClasses)[element] : (variant || style.classes)

      const mergedClasses = CSSClasses.mergeCssClasses(fixedClasses, classes)

      const finalClasses = visitor(mergedClasses)
      // Return as target format
      return options.format === 'string'
        ? new CSSClasses(finalClasses).toString()
        : options.format === 'array'
          ? new CSSClasses(finalClasses).toArray()
          : finalClasses
    }
  },
  beforeCreate () {
    // Check if style is defined
    if (!isDef(this.$options.style)) {
      this.$app.log(`Component '${this.$options.name}' using Variantable mixin must have 'style' option defined.`, { type: 'error' })
    }
    // Style option can be a function
    const componentInstanceStyle = new VariantableOptions(this.$options.style)
    // Get data option
    if (isDef(this.$options.data)) {
      // Check if data.style is not set to prevent conflicts
      if (isDef(this.$options.data().style)) {
        this.$app.log(`Component '${this.$options.name}' using Variantable mixin has data property 'style' that conflicts with the mixin.`, { type: 'error' })
      }
    }
    const dataOption = this.$options.data
      ? this.$options.data()
      : {}
    dataOption.style = componentInstanceStyle
    // Set data property
    this.$options.data = function () {
      return dataOption
    }
  }
}

export default Variantable
