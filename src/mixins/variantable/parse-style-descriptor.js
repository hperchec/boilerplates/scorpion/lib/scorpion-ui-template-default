import { utils, toolbox } from '@hperchec/scorpion-ui'

const { deepMerge } = utils
const { CSSClasses } = toolbox.css

/**
 * @param {string|string[]|object} value - The target to check
 * @param {string} [format = object] - The format for css classes: can be 'string', 'array' or 'object'
 * @returns {object} - An object containing the parsed classes and the each elements found
 * @description
 * Parse function for Variantable. Can parse properties:
 *
 * - fixedClasses
 * - classes
 * - variants.*
 */
export const parseStyleDescriptor = (value, format = 'object') => {
  const formatMethod = format === 'string'
    ? 'toString'
    : format === 'array'
      ? 'toArray'
      : 'toObject'
  let parsed
  let elements = null // save the elements in case of one-level object
  let result
  // First, try to parse css class value
  try {
    parsed = new CSSClasses(value)
    result = parsed[formatMethod]()
  } catch (err) {
    // Can be an one-level object
    if (typeof value === 'object') {
      const obj = {}
      elements = new Set()
      for (const key in value) {
        elements.add(key)
        try {
          obj[key] = new CSSClasses(value[key])[formatMethod]()
        } catch (_err) {
          result = false
        }
      }
      result = obj
    } else {
      result = false
    }
  }
  return {
    parsed: result,
    elements
  }
}

/**
 * Merge style descriptors
 * @param {string|string[]|object} a - From value
 * @param {string|string[]|object} b - To value
 * @param {string} [format = object] - The format for css classes: can be 'string', 'array' or 'object'
 * @returns {string|string[]|object} See **parseStyleDescriptor**
 */
export const mergeStyleDescriptors = (a, b, format = 'object') => {
  const _a = parseStyleDescriptor(a).parsed // get object!
  const _b = parseStyleDescriptor(b).parsed // get object!
  return parseStyleDescriptor(deepMerge(_a, _b), format).parsed
}

export default parseStyleDescriptor
