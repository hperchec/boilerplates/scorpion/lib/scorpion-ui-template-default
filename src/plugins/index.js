import { Core } from '@hperchec/scorpion-ui'
import VueTailwind from './vue-tailwind'

const plugins = {
  VueTailwind
}

// Expose plugins options to make customizing possible
Core.registerVuePlugin('VueTailwind', plugins.VueTailwind.plugin, plugins.VueTailwind.options)

export default plugins
