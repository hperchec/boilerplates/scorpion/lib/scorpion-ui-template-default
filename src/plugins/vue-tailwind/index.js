import plugin from './plugin'
import options from './options'

export default {
  plugin,
  options
}
