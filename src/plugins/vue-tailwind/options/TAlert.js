// Vue-tailwind component
import { TAlert } from 'vue-tailwind/dist/components'

export default {
  component: TAlert,
  props: {
    fixedClasses: {
      wrapper: `
        flex items-center
        relative
        p-4
        border-l-4
        rounded
        shadow-sm
      `,
      body: `
        flex-grow
      `,
      close: `
        flex flex-shrink-0 items-center justify-center
        relative
        w-6 h-6
        ml-4
        rounded
        transition duration-100 ease-in-out
        focus:ring-2
        focus:ring-blue-500
        focus:outline-none
        focus:ring-opacity-50
      `,
      closeIcon: `
        h-4 w-4
        fill-current
      `
    },
    classes: {
      wrapper: `
        bg-blue-50
        border-blue-500
      `,
      body: `
        text-blue-700
      `,
      close: `
        text-blue-500
        hover:bg-blue-200
      `
    },
    variants: {
      /**
       * Common variants
       */
      error: {},
      info: {},
      /**
       * 'light' theme variants
       */
      'alabaster-error': {},
      /**
       * 'dark' theme variants
       */
      'haiti-error': {
        wrapper: `
          bg-@dark-haiti
          border-@dark-error
        `,
        body: `
          text-@dark-error-400
        `,
        close: `
          text-@dark-error
          hover:bg-@dark-error-400
        `
      }
    }
  }
}
