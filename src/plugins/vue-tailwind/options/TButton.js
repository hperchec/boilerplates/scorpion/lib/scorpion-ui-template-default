// Vue-tailwind component
import { TButton } from 'vue-tailwind/dist/components'

export default {
  component: TButton,
  props: {
    // Fixed
    fixedClasses: [
      // ...
    ],
    // Default
    classes: [
      // ...
    ],
    // Variants
    variants: {
      // ...
    }
  }
}
