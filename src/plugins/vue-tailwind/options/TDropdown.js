// Vue-tailwind component
import { TDropdown } from 'vue-tailwind/dist/components'

/* eslint quote-props: ["error", "consistent"] */
export default {
  component: TDropdown,
  props: {
    fixedClasses: {
      // ...
    },
    classes: {
      // ...
    },
    variants: {
      // ...
    }
  }
}
