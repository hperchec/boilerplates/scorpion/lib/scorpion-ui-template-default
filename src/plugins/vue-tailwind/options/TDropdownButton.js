// Vue-tailwind component
import { TButton } from 'vue-tailwind/dist/components'

/* eslint quote-props: ["error", "consistent"] */
export default {
  component: TButton,
  props: {
    fixedClasses: [
      // ...
    ],
    classes: [
      // ...
    ],
    variants: {
      // ...
    }
  }
}
