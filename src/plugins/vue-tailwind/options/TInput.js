// Vue-tailwind component
import { TInput } from 'vue-tailwind/dist/components'

export default {
  component: TInput,
  props: {
    fixedClasses: [
      // ...
    ],
    classes: [
      // ...
    ],
    variants: {
      // ...
    }
  }
}
