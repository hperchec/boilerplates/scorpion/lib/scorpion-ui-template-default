// Vue-tailwind component
import { TModal } from 'vue-tailwind/dist/components'

export default {
  component: TModal,
  props: {
    fixedClasses: {
      // ...
    },
    classes: {
      // ...
    },
    variants: {
      // ...
    }
  }
}
