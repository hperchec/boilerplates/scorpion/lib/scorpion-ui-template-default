// Vue-tailwind component
import { TRichSelect } from 'vue-tailwind/dist/components'

/* eslint quote-props: ["error", "consistent"] */
export default {
  component: TRichSelect,
  props: {
    fixedClasses: {
      wrapper: `
        w-full
        mb-2
        relative
      `,
      buttonWrapper: `
        inline-block
        relative
        w-full
      `,
      selectButton: `
        flex justify-between items-center
        w-full
        px-3 py-2
        text-left
        border-2 border-transparent
        cursor-pointer
        rounded
        transition duration-100 ease-in-out
        focus:outline-none
        disabled:opacity-50
        disabled:cursor-not-allowed
      `,
      selectButtonLabel: `
        block
        truncate
      `,
      selectButtonTagWrapper: `
        max-w-full
        -mb-1
      `,
      selectButtonTag: `
        flex items-center
        max-w-full
        mr-1 mb-1
        pr-2
        text-sm
        rounded
        shadow-sm
        white-space-no
        overflow-hidden
        transition duration-100 ease-in-out
        focus:outline-none
        disabled:cursor-not-allowed
        disabled:opacity-50
      `,
      selectButtonTagText: `
        block
        px-3 py-2
        text-left
      `,
      selectButtonTagDeleteButton: `
        items-center
      `,
      selectButtonTagDeleteButtonIcon: `
        w-6
        h-6
        p-1
        rounded
        transition
      `,
      selectButtonPlaceholder: `
        block
        truncate
      `,
      selectButtonIcon: `
        absolute
        right-0
        top-0
        w-4
        h-4
        m-3 mt-3.5
        fill-current
      `,
      selectButtonClearButton: `
        flex flex-shrink-0 items-center justify-center
        absolute
        right-0
        top-0
        w-6
        h-6
        m-2
        rounded
        transition duration-100 ease-in-out
      `,
      selectButtonClearIcon: `
        w-3
        h-3
        fill-current
      `,
      dropdown: `
        absolute
        w-full
        z-10
        -mt-1
        rounded-b
        shadow-sm
      `,
      dropdownFeedback: '',
      loadingMoreResults: '',
      optionsList: `
        overflow-auto
      `,
      searchWrapper: `
        inline-block
        w-full
      `,
      searchBox: `
        inline-block
        w-full
      `,
      optgroup: '',
      option: `
        cursor-pointer
      `,
      disabledOption: `
        opacity-50
        cursor-not-allowed
      `,
      highlightedOption: `
        cursor-pointer
      `,
      selectedOption: `
        cursor-pointer
      `,
      selectedHighlightedOption: `
        cursor-pointer
      `,
      optionContent: '',
      optionLabel: `
        block
        truncate
        @dark:text-white
      `,
      selectedIcon: `
        w-4
        h-4
        fill-current
      `,
      enterClass: `
        opacity-0
      `,
      enterActiveClass: `
        transition ease-out duration-100
      `,
      enterToClass: `
        opacity-100
      `,
      leaveClass: `
        opacity-100
      `,
      leaveActiveClass: `
        transition ease-in duration-75
      `,
      leaveToClass: `
        opacity-0
      `
    },
    classes: {
      wrapper: '',
      buttonWrapper: '',
      selectButton: `
        pl-3 pr-8 py-2
        bg-@light-tertiary
        text-black
        rounded
        shadow-sm
        transition duration-100 ease-in-out
        focus:border-blue-500
        focus:ring-2
        focus:ring-blue-500
        focus:ring-opacity-50
        focus:outline-none
        disabled:opacity-50
        disabled:cursor-not-allowed
      `,
      selectButtonLabel: '',
      selectButtonTagWrapper: `
        max-w-full
        -mb-1
      `,
      selectButtonTag: `
        flex items-center
        max-w-full
        mr-1 mb-1
        pr-2
        bg-@light-secondary
        text-sm
        text-white
        rounded
        shadow-sm
        white-space-no
        overflow-hidden
        transition duration-100 ease-in-out
        focus:outline-none
        disabled:cursor-not-allowed
        disabled:opacity-50
      `,
      selectButtonTagText: `
        block
        px-3 py-2
        text-left
      `,
      selectButtonTagDeleteButton: `
        items-center
      `,
      selectButtonTagDeleteButtonIcon: `
        w-6
        h-6
        p-1
        rounded
        transition
        hover:bg-@light-primary
        hover:shadow-sm
      `,
      selectButtonPlaceholder: `
        text-gray-200
      `,
      selectButtonIcon: `
        text-gray-600
      `,
      selectButtonClearButton: `
        text-white
        rounded
        transition duration-100 ease-in-out
        hover:bg-@light-primary
        hover:text-white
      `,
      selectButtonClearIcon: '',
      dropdown: `
        -mt-1
        bg-@light-tertiary
        rounded-b
        shadow-md
      `,
      dropdownFeedback: `
        pb-2 px-3
        text-sm
        text-gray-200
      `,
      loadingMoreResults: `
        pb-2 px-3
        text-sm
        text-gray-200
      `,
      optionsList: '',
      searchWrapper: `
        p-2
        placeholder-gray-400
      `,
      searchBox: `
        px-3 py-2
        bg-@light-secondary
        text-sm
        border-2 border-primary
        rounded
        focus:outline-none
        focus:shadow-outline
      `,
      optgroup: `
        py-1 px-2
        text-xs
        text-gray-200
        uppercase
        font-semibold
      `,
      option: '',
      disabledOption: '',
      highlightedOption: `
        @light:bg-@light-primary
        @dark:bg-@dark-primary
      `,
      selectedOption: `
        @light:bg-@light-primary
        @dark:bg-@dark-primary
        text-white
        font-semibold
      `,
      selectedHighlightedOption: `
        font-semibold
        @light:bg-@light-primary
        @light:text-@light-text-primary
        @dark:bg-@dark-primary
        @dark:text-@dark-text-primary
      `,
      optionContent: `
        flex justify-between items-center
        px-3 py-2
      `,
      optionLabel: '',
      selectedIcon: '',
      enterClass: '',
      enterActiveClass: '',
      enterToClass: '',
      leaveClass: '',
      leaveActiveClass: '',
      leaveToClass: ''
    },
    variants: {
      /**
       * Common variants
       */
      // 'primary' variant
      'primary': {},
      // 'secondary' variant
      'secondary': {
        selectButton: `
          pl-3 pr-8 py-2
          bg-@light-secondary
          text-black
          rounded
          shadow
          transition duration-100 ease-in-out
          focus:border-blue-500
          focus:ring-2
          focus:ring-blue-500
          focus:ring-opacity-50
          focus:outline-none
          disabled:opacity-50
          disabled:cursor-not-allowed
        `,
        selectButtonTag: `
          flex items-center
          max-w-full
          mr-1 mb-1
          pr-2
          bg-@light-tertiary
          text-sm
          text-white
          rounded
          shadow-sm
          transition duration-100 ease-in-out
          white-space-no
          overflow-hidden
          focus:outline-none
          disabled:cursor-not-allowed
          disabled:opacity-50
        `,
        dropdown: `
          -mt-1
          bg-@light-secondary
          rounded-b
          shadow
        `,
        searchBox: `
          px-3 py-2
          bg-@light-tertiary
          text-sm
          border-2 border-primary
          rounded
          focus:outline-none
          focus:shadow-outline
        `
      },
      // 'tertiary' variant
      'tertiary': {},
      /**
       * 'light' theme variants
       */
      // 'alabaster' variant
      'alabaster': {},
      // 'white-smoke' variant
      'white-smoke': {},
      /**
       * 'dark' theme variants
       */
      // 'haiti' variant
      'haiti': {
        wrapper: '',
        buttonWrapper: '',
        selectButton: `
          bg-@dark-haiti
          text-@dark-text-primary
          hover:bg-@dark-haiti-400
        `,
        selectButtonLabel: '',
        selectButtonTagWrapper: '',
        selectButtonTag: `
          bg-@dark-haiti-400
          text-@dark-text-primary
        `,
        selectButtonTagText: `
          block
          px-3 py-2
          text-left
        `,
        selectButtonTagDeleteButton: `
          items-center
        `,
        selectButtonTagDeleteButtonIcon: `
          w-6
          h-6
          p-1
          rounded
          transition
          hover:bg-@dark-primary
          hover:shadow-sm
        `,
        selectButtonPlaceholder: `
          text-gray-400
        `,
        selectButtonIcon: `
          text-gray-600
        `,
        selectButtonClearButton: `
          text-white
          rounded
          transition duration-100 ease-in-out
          hover:bg-@dark-primary
          hover:text-white
        `,
        selectButtonClearIcon: '',
        dropdown: `
          bg-@dark-haiti
        `,
        dropdownFeedback: `
          pb-2 px-3
          text-sm
          text-gray-400
        `,
        loadingMoreResults: `
          pb-2 px-3
          text-sm
          text-gray-400
        `,
        optionsList: '',
        searchWrapper: `
          p-2
          placeholder-gray-400
        `,
        searchBox: `
          px-3 py-2
          text-sm
          bg-transparent
          border-2 border-@dark-primary
          rounded
          focus:outline-none
          focus:shadow-outline
        `,
        optgroup: `
          py-1 px-2
          text-xs
          text-gray-200
          uppercase
          font-semibold
        `,
        option: '',
        disabledOption: '',
        highlightedOption: `
          bg-@dark-primary
        `,
        selectedOption: `
          bg-@dark-primary
          text-white
          font-semibold
        `,
        selectedHighlightedOption: `
          font-semibold
          bg-@dark-primary
          text-@dark-text-primary
        `,
        optionContent: `
          flex justify-between items-center
          px-3 py-2
        `,
        optionLabel: '',
        selectedIcon: '',
        enterClass: '',
        enterActiveClass: '',
        enterToClass: '',
        leaveClass: '',
        leaveActiveClass: '',
        leaveToClass: ''
      },
      // 'mirage' variant
      'mirage': {
        wrapper: '',
        buttonWrapper: '',
        selectButton: `
          bg-@dark-mirage
          text-@dark-text-primary
          hover:bg-@dark-mirage-400
        `,
        selectButtonLabel: '',
        selectButtonTagWrapper: '',
        selectButtonTag: `
          bg-@dark-mirage-400
          text-@dark-text-primary
        `,
        selectButtonTagText: `
          block
          px-3 py-2
          text-left
        `,
        selectButtonTagDeleteButton: `
          items-center
        `,
        selectButtonTagDeleteButtonIcon: `
          w-6
          h-6
          p-1
          rounded
          transition
          hover:bg-@dark-primary
          hover:shadow-sm
        `,
        selectButtonPlaceholder: `
          text-gray-400
        `,
        selectButtonIcon: `
          text-gray-600
        `,
        selectButtonClearButton: `
          text-white
          rounded
          transition duration-100 ease-in-out
          hover:bg-@dark-primary
          hover:text-white
        `,
        selectButtonClearIcon: '',
        dropdown: `
          bg-@dark-mirage
        `,
        dropdownFeedback: `
          pb-2 px-3
          text-sm
          text-gray-400
        `,
        loadingMoreResults: `
          pb-2 px-3
          text-sm
          text-gray-400
        `,
        optionsList: '',
        searchWrapper: `
          p-2
          placeholder-gray-400
        `,
        searchBox: `
          px-3 py-2
          text-sm
          bg-transparent
          border-2 border-@dark-primary
          rounded
          focus:outline-none
          focus:shadow-outline
        `,
        optgroup: `
          py-1 px-2
          text-xs
          text-gray-200
          uppercase
          font-semibold
        `,
        option: '',
        disabledOption: '',
        highlightedOption: `
          bg-@dark-primary
        `,
        selectedOption: `
          bg-@dark-primary
          text-white
          font-semibold
        `,
        selectedHighlightedOption: `
          font-semibold
          bg-@dark-primary
          text-@dark-text-primary
        `,
        optionContent: `
          flex justify-between items-center
          px-3 py-2
        `,
        optionLabel: '',
        selectedIcon: '',
        enterClass: '',
        enterActiveClass: '',
        enterToClass: '',
        leaveClass: '',
        leaveActiveClass: '',
        leaveToClass: ''
      }
    }
  }
}
