// Vue-tailwind component
import { TToggle } from 'vue-tailwind/dist/components'

/* eslint quote-props: ["error", "consistent"] */
export default {
  component: TToggle,
  props: {
    fixedClasses: {
      // ...
    },
    classes: {
      // ...
    },
    variants: {
      // ...
    }
  }
}
