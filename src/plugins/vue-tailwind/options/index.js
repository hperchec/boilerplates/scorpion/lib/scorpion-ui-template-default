import TAlert from './TAlert'
import TButton from './TButton'
import TDropdown from './TDropdown'
import TDropdownButton from './TDropdownButton'
import TInput from './TInput'
import TModal from './TModal'
import TRadio from './TRadio'
import TRichSelect from './TRichSelect'
import TTextarea from './TTextArea'
import TToggle from './TToggle'

// Tailwind components settings
export default {
  't-alert': TAlert,
  't-button': TButton,
  't-dropdown': TDropdown,
  't-dropdown-button': TDropdownButton,
  't-input': TInput,
  't-modal': TModal,
  't-radio': TRadio,
  't-rich-select': TRichSelect,
  't-textarea': TTextarea,
  't-toggle': TToggle
}
