import { Core, toolbox } from '@hperchec/scorpion-ui'

import prototype from './prototype'
import rootMixin from './root-mixin'

const { mergeContext } = toolbox.template.utils

// Add root instance mixin
Core.context.vue.root.setOptions(rootMixin)
// Define vue root instance prototype in '$app' namespace
mergeContext(Core.context.vue.root.prototype, 'app', prototype.app)

export default {
  prototype,
  rootMixin
}
