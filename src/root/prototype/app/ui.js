/**
 * Vue: 'ui' prototype
 */

import { Core, utils } from '@hperchec/scorpion-ui'

const { isDef, kebabcase } = utils

const parseBreakpoints = (bpts) => {
  const _tmp = []
  for (const name in bpts) {
    const value = bpts[name]
    _tmp.push({
      originalKey: name,
      name: name.toLowerCase(),
      width: value
    })
  }
  _tmp.sort((a, b) => {
    return a.width - b.width
  })
  return _tmp
}

/**
 * $app.ui.getBreakpointForWidth
 * @param {number} width - Width
 * @returns {?object} The breakpoint object
 * @description
 * Compute breakpoint for given width
 */
function getBreakpointForWidth (width) {
  const root = this // 'this' is the Vue root instance
  const breakpoints = parseBreakpoints(root.shared.GLOBALS.BREAKPOINTS)
  // Loop on sorted breakpoints
  for (const bp of breakpoints) {
    // If we match breakpoint break loop and return
    if (width >= bp.width) return bp
  }
  // Else, no breakpoint
  return { name: null, width: 0 }
}

/**
 * $app.ui.currentBreakpoint
 * @description Return current breakpoint based on window.innerWidth
 * @returns {?string} '2xl', 'xl', 'lg', 'md', 'sm', or null
 */
function currentBreakpoint () {
  const root = this // 'this' is the Vue root instance
  return root.currentBreakpoint
}

/**
 * $app.ui.getThemes
 * @returns {string[]} - The app themes
 * @description
 * Returns the app theme names as array
 */
function getThemes () {
  const root = this // 'this' is the Vue root instance
  // Get all themes
  const themes = Object.keys(root.shared.GLOBALS.THEMES).map((t) => {
    return kebabcase(t)
  })
  return themes
}

/**
 * $app.ui.setTheme
 * @param {string} theme - Theme name (kebab-case) or 'auto'
 * @returns {void}
 * @description
 * Set theme setting and its value in Local storage
 */
function setTheme (theme) {
  const root = this // 'this' is the Vue root instance
  // Get current (old) theme
  const currentTheme = root.currentTheme
  // Get all themes
  const themes = root._app.ui.getThemes()
  // Check if target theme exists or is 'auto'
  if (!(themes.includes(theme) || theme === 'auto')) {
    root._app.log('warning', `Unknown theme '${theme}'. Setting to default theme '${Core.config.system.defaultTheme}'...`)
    theme = Core.config.system.defaultTheme
  }
  // Final theme to apply (different than 'auto')
  let themeToApply = Core.config.system.fallbackTheme
  // If theme is 'auto'
  if (theme === 'auto') {
    root._app.log('Set theme automatically based on user browser preferences...', { type: 'system' })
    // 'dark' theme case based on prefers-color-scheme
    if (window.matchMedia('(prefers-color-scheme: dark)').matches) {
      root._app.log('User theme preference: "dark"', { type: 'system' })
      // Check if theme 'dark' is defined
      if (!themes.includes('dark')) {
        root._app.log('Theme "dark" not defined. Skipping...', { type: 'warning' })
      } else {
        themeToApply = 'dark'
      }
    // 'light' theme case based on prefers-color-scheme
    } else if (window.matchMedia('(prefers-color-scheme: light)').matches) {
      root._app.log('User theme preference: "light"', { type: 'system' })
      // Check if theme 'light' is defined
      if (!themes.includes('light')) {
        root._app.log('Theme "light" not defined. Skipping...', { type: 'warning' })
      } else {
        themeToApply = 'light'
      }
    // Else, prefers-color-scheme different than "light" or "dark"
    } else {
      root._app.log('Unknown prefers-color-scheme value. Skipping...', { type: 'warning' })
    }
  // Else, theme is defined
  } else {
    root._app.log(`Set theme: "${theme}"`, { type: 'system' })
    themeToApply = theme
  }
  // Remove old class if set
  if (currentTheme) {
    // Set themeToApplyClass
    const currentThemeClass = `@${currentTheme}`
    // Remove class from HTML element
    document.documentElement.classList.remove(currentThemeClass)
  }
  // Set in user settings
  root.userSettings.theme = theme // can be 'auto'
  // Set theme in ROOT INSTANCE data
  root.currentTheme = themeToApply // !! CANNOT BE 'auto'
  // Set themeToApplyClass
  const themeToApplyClass = `@${themeToApply}`
  // Set theme by class name in HTML element
  document.documentElement.classList.add(themeToApplyClass)
}

/**
 * currentTheme
 * @alias module:ui.currentTheme
 * @description get current theme defined in Local storage
 * @returns {string} Theme name (kebab-case) or 'auto'
 */
function currentTheme () {
  const root = this // 'this' is the Vue root instance
  return root.currentTheme
}

/**
 * Shortcut to return a value depending on theme applied ($app.ui.currentTheme())
 * @param {object} map - An object in format { <theme1>: <returnValue>, <theme2>: <returnValue>} where <returnValue> can be anything you want
 * @param {*} defaultValue - The default value to return
 * @returns {*} Returns theme based value
 */
function isTheme (map, defaultValue) {
  const root = this // 'this' is the Vue root instance
  return isDef(map[root.currentTheme])
    ? map[root.currentTheme]
    : defaultValue
}

export default {
  getBreakpointForWidth,
  currentBreakpoint,
  getThemes,
  setTheme,
  currentTheme,
  isTheme
}
