import { Core } from '@hperchec/scorpion-ui'
// Tailwind resolveConfig
import resolveTailwindConfig from 'tailwindcss/resolveConfig'

export default {
  data () {
    return {
      shared: {
        /**
         * @alias options.data.shared.TAILWIND
         * @type {object}
         * @description The tailwind data and utils to share (i.e. config)
         */
        TAILWIND: {
          config: function () {
            return resolveTailwindConfig(Core.config.tailwind.config)
          },
          getColors: function () {
            return this.config.theme.colors
          },
          getColor: function (name) {
            return this.config.theme.colors[name]
          }
        }
      },
      userSettings: {
        theme: Core.config.system.defaultTheme
      },
      /**
       * @alias options.data.currentBreakpoint
       * @type {?object}
       * @description
       * The current breakPoint. Default to null (will be set in created hook)
       */
      currentBreakpoint: null,
      /**
       * @alias options.data.currentTheme
       * @type {?string}
       * @description
       * The current theme. Default to null (will be set in created hook)
       */
      currentTheme: null,
      /**
       * If data is ready
       */
      dataReady: false
    }
  },
  // Created hook
  async created () {
    // Set theme
    this.$app.ui.setTheme(this.userSettings.theme)
    // Breakpoint & window resize observer
    this.currentBreakpoint = this.$app.ui.getBreakpointForWidth(window.innerWidth)
    window.addEventListener('resize', () => {
      if (this.currentBreakpoint.name !== this.$app.ui.getBreakpointForWidth(window.innerWidth).name) {
        const fromBreakpoint = this.currentBreakpoint
        this.currentBreakpoint = this.$app.ui.getBreakpointForWidth(window.innerWidth)
        this.$app.emit('App/BREAKPOINT_CHANGED', { from: fromBreakpoint, to: this.currentBreakpoint })
      }
    })
  }
}
