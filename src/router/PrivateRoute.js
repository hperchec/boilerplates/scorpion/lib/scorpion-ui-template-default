import { Core } from '@hperchec/scorpion-ui'

/* eslint-disable-next-line dot-notation */
const _PrivateRoute = Core.context.services['router'].PrivateRoute

/**
 * Custom PrivateRoute class
 */
export class PrivateRoute extends _PrivateRoute {
  /**
   * Creates a new PrivateRoute instance
   * @param {string} path - The route path
   * @param {string} name - The route name
   * @param {Function|null} component - A function that returns the route Vue component (can be null)
   * @param {object} [options = {}] - Object that contains other the vue-router route object properties
   * @see Core.context.services.router.Route
   */
  constructor (path, name, component, options = {}) { // eslint-disable-line
    const isLayoutSet = options.meta && Object.prototype.hasOwnProperty.call(options.meta, 'layout')
    // Call parent constructor
    super(path, name, component, options)
    // Set private layout if not set (! CAN BE UNDEFINED)
    if (!isLayoutSet) {
      this.setMeta({
        layout: 'private'
      })
    }
  }
}

export default PrivateRoute
