import { Core } from '@hperchec/scorpion-ui'

/* eslint-disable-next-line dot-notation */
const _PublicRoute = Core.context.services['router'].PublicRoute

/**
 * Custom PublicRoute class
 */
export class PublicRoute extends _PublicRoute {
  /**
   * Creates a new PublicRoute instance
   * @param {string} path - The route path
   * @param {string} name - The route name
   * @param {Function|null} component - A function that returns the route Vue component (can be null)
   * @param {object} [options = {}] - Object that contains other the vue-router route object properties
   * @see Core.context.services.router.Route
   */
  constructor (path, name, component, options = {}) {
    const isLayoutSet = options.meta && Object.prototype.hasOwnProperty.call(options.meta, 'layout')
    // Call parent constructor
    super(path, name, component, options)
    // Set public layout if not set (! CAN BE UNDEFINED)
    if (!isLayoutSet) {
      this.setMeta({
        layout: 'public'
      })
    }
  }
}

export default PublicRoute
