import { Core } from '@hperchec/scorpion-ui'

/* eslint-disable-next-line dot-notation */
const _Route = Core.context.services['router'].Route

/**
 * Custom Route class
 */
export class Route extends _Route {
  /**
   * Get the breadcrumbs for the given route
   * @param {object} route - The target route
   * @param {object} routeComponentInstance - The route component instance
   * @returns {object[]} - The breadcrumbs
   */
  static getBreadcrumb (route, routeComponentInstance) {
    let breadcrumbs = []
    if (route.meta.breadcrumb && route.meta.breadcrumb.schema !== null) {
      // Get route meta
      const schema = route.meta.breadcrumb.schema
      if (routeComponentInstance) {
        // Loop on array
        breadcrumbs = schema.reduce((array, current) => {
          // If find "[someroute]" -> Link to named route
          const re = /^\[(.*)\]$/
          if (re.test(current)) {
            const found = current.match(re)[1]
            const namedRoute = Core.service('router').getRoutes().find((_route) => {
              return _route.name === found
            })
            array.push({
              text: namedRoute.meta.breadcrumb.title(routeComponentInstance),
              to: namedRoute.meta.breadcrumb.to
                ? namedRoute.meta.breadcrumb.to(routeComponentInstance)
                : namedRoute.path === ''
                  ? '/'
                  : namedRoute.path // prevent Home link
            })
          }
          // If find "<this>" -> title of this route
          if (current === '<this>') {
            array.push({
              text: route.meta.breadcrumb.title(routeComponentInstance)
            })
          }
          return array
        }, [])
      }
    }
    return breadcrumbs
  }
}

export default Route
