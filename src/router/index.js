import { Core, toolbox } from '@hperchec/scorpion-ui'

import routes from './routes'
import Route from './Route'
import PublicRoute from './PublicRoute'
import PrivateRoute from './PrivateRoute'

/* eslint-disable dot-notation */

const { mergeContext } = toolbox.template.utils

// Remove default public "Home" route
delete Core.context.services['router'].routes.public.Home
// Override Route & PublicRoute & PrivateRoute classes
Core.context.services['router'].Route = Route
Core.context.services['router'].PublicRoute = PublicRoute
Core.context.services['router'].PrivateRoute = PrivateRoute
// Merge routes
mergeContext(Core.context.services['router'], 'routes', routes)
