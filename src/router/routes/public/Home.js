import { Core } from '@hperchec/scorpion-ui'

const homeRoute = Core.context.services.router.routes.public.Home

homeRoute.name = 'home'
homeRoute.options.meta.breadcrumb = {
  schema: null,
  title: vm => vm.$filters.capitalize(vm.$t('views.Home.Index.title'), 1)
}
homeRoute.options.meta.layoutProps = {
  // Hide the Sidebar
  showSidebar: false
}

export default homeRoute
