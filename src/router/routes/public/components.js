// Route component
// import component from '@/components/public/views/components/Index.vue'
import { Core } from '@hperchec/scorpion-ui'

/**
 * Public route:
 * - **Path**: `/components`
 */
export default {
  path: '/components',
  name: 'components',
  component: () => Core.context.vue.components.public.views.components.Index,
  options: {
    meta: {
      breadcrumb: {
        schema: [ '[home]', '<this>' ],
        title: vm => vm.$filters.capitalize(vm.$t('views.components.index.title'), 1)
      },
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.components.index.title'), 1)
      }
    }
  }
}
