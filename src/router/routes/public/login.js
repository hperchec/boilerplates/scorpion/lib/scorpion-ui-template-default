import { Core } from '@hperchec/scorpion-ui'

/**
 * Public route:
 * - **Path**: `/login`
 */
export default {
  path: '/login',
  name: 'login',
  component: () => Core.context.vue.components.public.views.login.Index,
  options: {
    meta: {
      breadcrumb: {
        schema: [ '[home]', '<this>' ],
        title: vm => vm.$filters.capitalize(vm.$t('views.login.index.title'), 1)
      },
      pageMeta: {
        title: vm => vm.$filters.capitalize(vm.$t('views.login.index.title'), 1)
      }
    }
  }
}
