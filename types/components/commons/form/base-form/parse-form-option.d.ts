export function generateFieldRelatedKeys(vm: Vue, field: string): object;
export default function parseFormOption(vm: Vue): void;
export type FormFieldDescriptor = {
    dataKey?: string;
    default?: Function;
    state?: Function;
    validator?: Function;
    dataFormatter?: Function;
    ruleToErrorCode?: object;
};
//# sourceMappingURL=parse-form-option.d.ts.map