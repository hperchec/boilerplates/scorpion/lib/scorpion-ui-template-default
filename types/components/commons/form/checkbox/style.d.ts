declare namespace _default {
    namespace fixedClasses {
        let wrapper: {
            'cursor-pointer': boolean;
            flex: boolean;
            'items-center': boolean;
            'gap-3': boolean;
            'py-2': boolean;
            'px-2': boolean;
            rounded: boolean;
            transition: boolean;
            'ease-in-out': boolean;
        };
        let input: {
            flex: boolean;
            'items-center': boolean;
            'justify-center': boolean;
            'w-5': boolean;
            'h-5': boolean;
            rounded: boolean;
        };
    }
    namespace classes {
        let wrapper_1: {
            'bg-transparent': boolean;
        };
        export { wrapper_1 as wrapper };
        let input_1: {};
        export { input_1 as input };
    }
    namespace variants {
        namespace transparent {
            let wrapper_2: {
                '@light:bg-transparent': boolean;
                '@dark:bg-transparent': boolean;
                '@light:if-checked:text-@light-text-primary': boolean;
                '@dark:if-checked:text-@dark-text-primary': boolean;
            };
            export { wrapper_2 as wrapper };
            let input_2: {
                'border-2': boolean;
                'border-gray-500': boolean;
                'if-checked:border-0': boolean;
                '@light:if-checked:bg-@light-success': boolean;
                '@light:bg-transparent': boolean;
                '@dark:if-checked:bg-@dark-success': boolean;
                '@dark:bg-transparent': boolean;
            };
            export { input_2 as input };
        }
        namespace alabaster {
            let wrapper_3: {
                '@light:bg-@light-alabaster': boolean;
            };
            export { wrapper_3 as wrapper };
            let input_3: {
                '@dark:if-checked:bg-@dark-success': boolean;
                '@dark:bg-@dark-black-rock': boolean;
            };
            export { input_3 as input };
        }
        namespace haiti {
            let wrapper_4: {
                '@dark:if-checked:bg-@dark-primary': boolean;
                '@light:if-checked:bg-@light-primary': boolean;
            };
            export { wrapper_4 as wrapper };
            let input_4: {
                '@dark:if-checked:bg-@dark-success': boolean;
                '@dark:bg-@dark-black-rock': boolean;
            };
            export { input_4 as input };
        }
    }
}
export default _default;
//# sourceMappingURL=style.d.ts.map