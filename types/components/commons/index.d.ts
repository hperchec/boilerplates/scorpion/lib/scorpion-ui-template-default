declare namespace _default {
    export { Page };
    export { VButton };
    export { VDropdown };
    export { VDropdownButton };
    export { I18nFlagIcon };
    export { VLink };
    export { VToggle };
    export { VWrappedToggle };
}
export default _default;
import Page from './Page.vue';
import VButton from './buttons/VButton.vue';
import VDropdown from './dropdown/VDropdown.vue';
import VDropdownButton from './dropdown-button/VDropdownButton.vue';
import I18nFlagIcon from './icons/I18nFlagIcon.vue';
import VLink from './links/VLink.vue';
import VToggle from './toggle/VToggle.vue';
import VWrappedToggle from './wrapped-toggle/VWrappedToggle.vue';
//# sourceMappingURL=index.d.ts.map