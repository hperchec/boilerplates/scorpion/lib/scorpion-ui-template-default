export default components;
declare namespace components {
    export { App };
    export { commons };
    export { layouts };
    export { shared };
    export { publicComponents as public };
    export { privateComponents as private };
}
import App from './App.vue';
import commons from './commons';
import layouts from './layouts';
import shared from './shared';
import publicComponents from './public';
import privateComponents from './private';
//# sourceMappingURL=index.d.ts.map