declare namespace _default {
    namespace fixedClasses {
        let container: any[];
        let item: any[];
        let itemText: any[];
        let separator: any[];
    }
    namespace classes {
        let container_1: string[];
        export { container_1 as container };
        let item_1: string[];
        export { item_1 as item };
        let itemText_1: string[];
        export { itemText_1 as itemText };
        let separator_1: string[];
        export { separator_1 as separator };
    }
    let variants: {};
}
export default _default;
//# sourceMappingURL=style.d.ts.map