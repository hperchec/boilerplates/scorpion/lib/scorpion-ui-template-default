declare namespace _default {
    export { Breadcrumb };
    export { LangDropdown };
    export { ThemeDropdown };
    export { ThemeToggle };
}
export default _default;
import Breadcrumb from './breadcrumb/Breadcrumb.vue';
import LangDropdown from './lang-dropdown/LangDropdown.vue';
import ThemeDropdown from './theme-dropdown/ThemeDropdown.vue';
import ThemeToggle from './theme-toggle/ThemeToggle.vue';
//# sourceMappingURL=index.d.ts.map