declare namespace _default {
    export { globals };
    export namespace system {
        let defaultTheme: string;
        let fallbackTheme: string;
    }
    export namespace auth {
        let accessTokenKey: string;
        let refreshTokenKey: string;
    }
    export namespace tailwind {
        let config: object;
    }
}
export default _default;
import globals from './globals.json';
//# sourceMappingURL=config.d.ts.map