declare const _default: {
    views: {
        Home: {
            Index: {
                subtitle: string;
            };
        };
        components: {
            index: {
                title: string;
            };
        };
    };
    models: {
        User: {
            singular: string;
            plural: string;
            username: string;
            email: string;
            name: string;
            firstname: string;
            lastname: string;
            password: string;
            phoneNumber: string;
            birthDate: string;
            thumbnail: string;
            editUser: string;
            actions: {
                getCurrentUserData: {
                    loading: string;
                };
                create: {
                    loading: string;
                };
                update: {
                    loading: string;
                };
                updateThumbnail: {
                    loading: string;
                };
                updateEmail: {
                    title: string;
                    loading: string;
                };
                verifyEmail: {
                    loading: string;
                };
            };
        };
    };
    global: {
        darkTheme: string;
        importPictures: string;
        limitFiles: string;
        none: string;
        none_unisex: string;
        preview: string;
        select: string;
        selectAll: string;
        selected: {
            singular: string;
            singular_unisex: string;
            plural: string;
            plural_female: string;
            plural_unisex: string;
        };
    };
};
export default _default;
//# sourceMappingURL=index.d.ts.map