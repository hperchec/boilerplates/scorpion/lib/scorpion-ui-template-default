export namespace ExtractsCSSClasses {
    namespace methods {
        function getRootElementCSSClasses(): object;
    }
}
export default ExtractsCSSClasses;
//# sourceMappingURL=index.d.ts.map