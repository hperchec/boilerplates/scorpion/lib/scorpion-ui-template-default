declare namespace _default {
    namespace props {
        namespace formatter {
            export let type: FunctionConstructor;
            function _default(value: any): any;
            export { _default as default };
        }
    }
    function beforeCreate(): void;
}
export default _default;
//# sourceMappingURL=index.d.ts.map