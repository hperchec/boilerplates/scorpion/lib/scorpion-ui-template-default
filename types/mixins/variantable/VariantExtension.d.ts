export default VariantExtension;
declare class VariantExtension {
    constructor(parentName: string, overrides: object);
    parentName: string;
    overrides: any;
    extendParent(parent: object, format?: string): object;
}
//# sourceMappingURL=VariantExtension.d.ts.map