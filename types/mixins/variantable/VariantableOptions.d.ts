export default VariantableOptions;
declare class VariantableOptions {
    static getVariantsFromOptions(options: VariantableOptions): string[];
    static getElementCssClassFromOptions(options: VariantableOptions, element?: string, variant?: string): string;
    static merge(a: VariantableOptions, b: VariantableOptions): VariantableOptions;
    static extend(varOptions: VariantableOptions, processor: Function): VariantableOptions;
    constructor(obj: {
        fixedClasses: string | object;
        classes: string | object;
        variants?: object;
    }, options?: {
        cssClassesFormat?: string;
    });
    fixedClasses: object | null;
    classes: object | null;
    variants: object | null;
    variantExtensions: object | null;
    syncVariantExtensions(format?: string): void;
    getVariants(): string[];
    hasVariant(variant: string): boolean;
    getElementKeys(): string[];
    hasNestedElements(): boolean;
    getElementCssClass(element?: string, variant?: string): any;
    #private;
}
//# sourceMappingURL=VariantableOptions.d.ts.map