export { VariantableOptions };
export namespace Variantable {
    namespace style {
        let fixedClasses: {};
        let classes: {};
        let variants: {};
    }
    namespace props {
        namespace variant {
            export let type: StringConstructor;
            let _default: string;
            export { _default as default };
        }
        let vStyle: (object | Function) | null;
    }
    namespace computed {
        function overriddenStyle(): VariantableOptions;
    }
    namespace methods {
        function getElementCssClass(element: string, options?: {
            format?: string;
            visit?: Function;
        }): string | string[] | object;
    }
    function beforeCreate(): void;
}
export default Variantable;
import VariantableOptions from './VariantableOptions';
//# sourceMappingURL=index.d.ts.map