declare namespace _default {
    export { TAlert as component };
    export namespace props {
        namespace fixedClasses {
            let wrapper: string;
            let body: string;
            let close: string;
            let closeIcon: string;
        }
        namespace classes {
            let wrapper_1: string;
            export { wrapper_1 as wrapper };
            let body_1: string;
            export { body_1 as body };
            let close_1: string;
            export { close_1 as close };
        }
        let variants: {
            error: {};
            info: {};
            'alabaster-error': {};
            'haiti-error': {
                wrapper: string;
                body: string;
                close: string;
            };
        };
    }
}
export default _default;
import { TAlert } from 'vue-tailwind/dist/components';
//# sourceMappingURL=TAlert.d.ts.map