declare namespace _default {
    export { TInput as component };
    export namespace props {
        let fixedClasses: string;
        let classes: string;
        let variants: {
            haiti: string;
            'haiti-error': string;
            tertiary: string;
            'tertiary-error': string;
        };
    }
}
export default _default;
import { TInput } from 'vue-tailwind/dist/components';
//# sourceMappingURL=TInput.d.ts.map