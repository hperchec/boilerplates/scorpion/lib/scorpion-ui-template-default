declare namespace _default {
    export { TModal as component };
    export namespace props {
        namespace fixedClasses {
            let overlay: string;
            let wrapper: string;
            let modal: string;
            let body: string;
            let header: string;
            let footer: string;
            let close: string;
        }
        namespace classes {
            let overlay_1: string;
            export { overlay_1 as overlay };
            let wrapper_1: string;
            export { wrapper_1 as wrapper };
            let modal_1: string;
            export { modal_1 as modal };
            let header_1: string;
            export { header_1 as header };
            let footer_1: string;
            export { footer_1 as footer };
            let close_1: string;
            export { close_1 as close };
            export let closeIcon: string;
            export let overlayEnterClass: string;
            export let overlayEnterActiveClass: string;
            export let overlayEnterToClass: string;
            export let overlayLeaveClass: string;
            export let overlayLeaveActiveClass: string;
            export let overlayLeaveToClass: string;
            export let enterClass: string;
            export let enterActiveClass: string;
            export let enterToClass: string;
            export let leaveClass: string;
            export let leaveActiveClass: string;
            export let leaveToClass: string;
        }
        let variants: {};
    }
}
export default _default;
import { TModal } from 'vue-tailwind/dist/components';
//# sourceMappingURL=TModal.d.ts.map