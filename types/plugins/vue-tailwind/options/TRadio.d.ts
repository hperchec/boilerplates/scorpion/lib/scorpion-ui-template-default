declare namespace _default {
    export { TRadio as component };
    export namespace props {
        let fixedClasses: string;
        let classes: string;
        namespace variants {
            let Custom: string;
        }
    }
}
export default _default;
import { TRadio } from 'vue-tailwind/dist/components';
//# sourceMappingURL=TRadio.d.ts.map