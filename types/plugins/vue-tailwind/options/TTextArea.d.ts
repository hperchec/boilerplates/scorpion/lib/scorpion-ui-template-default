declare namespace _default {
    export { TTextarea as component };
    export namespace props {
        let fixedClasses: string;
        let classes: string;
        namespace variants {
            let danger: string;
            let success: string;
        }
    }
}
export default _default;
import { TTextarea } from 'vue-tailwind/dist/components';
//# sourceMappingURL=TTextArea.d.ts.map