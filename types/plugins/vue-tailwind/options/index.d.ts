declare const _default: {
    't-alert': {
        component: import("vue/types/vue").ExtendedVue<{
            getElementCssClass(elementName?: string | undefined, defaultClasses?: import("vue-tailwind/dist/types/CssClass").default): import("vue-tailwind/dist/types/CssClass").default;
        } & {
            componentClass: import("vue-tailwind/dist/types/CssClass").default;
            activeVariant: string | undefined;
        } & {
            classes: any;
            fixedClasses: any;
            variants: any;
            variant: any;
        } & import("vue").default<Record<string, any>, Record<string, any>, never, never, (event: string, ...args: any[]) => import("vue").default>, {
            localShow: boolean;
        }, {
            render(createElement: import("vue").CreateElement): import("vue").VNode;
            initTimeout(): void;
            hide(): void;
        }, unknown, {
            tagName: string;
            bodyTagName: string;
            dismissible: boolean;
            show: boolean;
            timeout: number;
            classes: any;
        }, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin>;
        props: {
            fixedClasses: {
                wrapper: string;
                body: string;
                close: string;
                closeIcon: string;
            };
            classes: {
                wrapper: string;
                body: string;
                close: string;
            };
            variants: {
                error: {};
                info: {};
                'alabaster-error': {};
                'haiti-error': {
                    wrapper: string;
                    body: string;
                    close: string;
                };
            };
        };
    };
    't-button': {
        component: import("vue/types/vue").ExtendedVue<{
            getListeners(listeners: {
                [key: string]: Function | Function[];
            } | undefined): {
                [key: string]: Function | Function[];
            } | undefined;
        } & {
            id: string;
            name: string;
            disabled: boolean;
            readonly: boolean;
            autofocus: boolean;
            required: boolean;
            tabindex: string | number;
        } & {
            getElementCssClass(elementName?: string | undefined, defaultClasses?: import("vue-tailwind/dist/types/CssClass").default): import("vue-tailwind/dist/types/CssClass").default;
        } & {
            componentClass: import("vue-tailwind/dist/types/CssClass").default;
            activeVariant: string | undefined;
        } & {
            classes: any;
            fixedClasses: any;
            variants: any;
            variant: any;
        } & import("vue").default<Record<string, any>, Record<string, any>, never, never, (event: string, ...args: any[]) => import("vue").default>, unknown, {
            blurHandler(e: FocusEvent): void;
            focusHandler(e: FocusEvent): void;
            clickHandler(e: MouseEvent): void;
            keydownHandler(e: MouseEvent): void;
            mousedownHandler(e: MouseEvent): void;
            blur(): void;
            focus(): void;
            inertiaLinkAttributes(): {
                data: any;
                href: string;
                method: string;
                replace: boolean;
                preserveScroll: boolean;
                preserveState: boolean;
                only: unknown[];
                id: string;
                value: string | number;
                autofocus: boolean;
                disabled: boolean;
                name: string;
                type: string;
            };
            routerLinkAttributes(): {
                to: any;
                replace: boolean;
                append: boolean;
                tag: string;
                activeClass: string;
                exact: boolean;
                event: string | unknown[];
                exactActiveClass: string;
                id: string;
                value: string | number;
                autofocus: boolean;
                disabled: boolean;
                name: string;
                type: string;
            };
            getAttributes(): {
                data: any;
                href: string;
                method: string;
                replace: boolean;
                preserveScroll: boolean;
                preserveState: boolean;
                only: unknown[];
                id: string;
                value: string | number;
                autofocus: boolean;
                disabled: boolean;
                name: string;
                type: string;
            } | {
                to: any;
                replace: boolean;
                append: boolean;
                tag: string;
                activeClass: string;
                exact: boolean;
                event: string | unknown[];
                exactActiveClass: string;
                id: string;
                value: string | number;
                autofocus: boolean;
                disabled: boolean;
                name: string;
                type: string;
            } | {
                id: string;
                value: string | number;
                autofocus: boolean;
                disabled: boolean;
                name: string;
                href: string;
                type: string;
            };
            render(createElement: import("vue").CreateElement): import("vue").VNode;
        }, {
            isInertiaLinkComponentAvailable: boolean;
            isRouterLinkComponentAvailable: boolean;
            isARouterLink: boolean;
            isAnIntertiaLink: boolean;
            componentToRender: string | import("vue").VueConstructor<import("vue").default> | import("vue").FunctionalComponentOptions<any, import("vue/types/options").PropsDefinition<any>> | import("vue").ComponentOptions<never, any, any, any, any, Record<string, any>> | import("vue/types/options").AsyncComponentPromise<any, any, any, any> | import("vue/types/options").AsyncComponentFactory<any, any, any, any> | undefined;
        }, {
            value: string | number;
            text: string;
            tagName: string;
            type: string;
            href: string;
            to: any;
            append: boolean;
            activeClass: string;
            exact: boolean;
            exactActiveClass: string;
            event: string | unknown[];
            data: any;
            method: string;
            replace: boolean;
            preserveScroll: boolean;
            preserveState: boolean;
            only: unknown[];
            native: boolean;
            classes: any;
        }, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin>;
        props: {
            fixedClasses: any[];
            classes: any[];
            variants: {};
        };
    };
    't-dropdown': {
        component: import("vue/types/vue").ExtendedVue<{
            getElementCssClass(elementName?: string | undefined, defaultClasses?: import("vue-tailwind/dist/types/CssClass").default): import("vue-tailwind/dist/types/CssClass").default;
        } & {
            componentClass: import("vue-tailwind/dist/types/CssClass").default;
            activeVariant: string | undefined;
        } & {
            classes: any;
            fixedClasses: any;
            variants: any;
            variant: any;
        } & import("vue").default<Record<string, any>, Record<string, any>, never, never, (event: string, ...args: any[]) => import("vue").default>, {
            localShow: boolean;
            hasFocus: boolean;
            hideOnLeaveTimeoutHolder: NodeJS.Timeout | null;
        }, {
            render(createElement: import("vue").CreateElement): import("vue").VNode;
            blurEventTargetIsChild(e: FocusEvent): boolean;
            focusEventTargetIsChild(e: FocusEvent): boolean;
            escapeHandler(): void;
            mousedownHandler(): void;
            focusHandler(e: FocusEvent): void;
            blurHandler(e: FocusEvent): void;
            keydownHandler(e: KeyboardEvent): void;
            mouseleaveHandler(): void;
            mouseoverHandler(): void;
            doHide(): void;
            hideIfFocusOutside(e: FocusEvent): void;
            doShow(): void;
            doToggle(): void;
            blur(): void;
            focus(options?: FocusOptions | undefined): void;
        }, unknown, {
            text: string;
            disabled: boolean;
            tagName: string;
            dropdownWrapperTagName: string;
            dropdownTagName: string;
            toggleOnFocus: boolean;
            toggleOnClick: boolean;
            toggleOnHover: boolean;
            hideOnLeaveTimeout: number;
            show: boolean;
            classes: any;
        }, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin>;
        props: {
            fixedClasses: {};
            classes: {};
            variants: {};
        };
    };
    't-dropdown-button': {
        component: import("vue/types/vue").ExtendedVue<{
            getListeners(listeners: {
                [key: string]: Function | Function[];
            } | undefined): {
                [key: string]: Function | Function[];
            } | undefined;
        } & {
            id: string;
            name: string;
            disabled: boolean;
            readonly: boolean;
            autofocus: boolean;
            required: boolean;
            tabindex: string | number;
        } & {
            getElementCssClass(elementName?: string | undefined, defaultClasses?: import("vue-tailwind/dist/types/CssClass").default): import("vue-tailwind/dist/types/CssClass").default;
        } & {
            componentClass: import("vue-tailwind/dist/types/CssClass").default;
            activeVariant: string | undefined;
        } & {
            classes: any;
            fixedClasses: any;
            variants: any;
            variant: any;
        } & import("vue").default<Record<string, any>, Record<string, any>, never, never, (event: string, ...args: any[]) => import("vue").default>, unknown, {
            blurHandler(e: FocusEvent): void;
            focusHandler(e: FocusEvent): void;
            clickHandler(e: MouseEvent): void;
            keydownHandler(e: MouseEvent): void;
            mousedownHandler(e: MouseEvent): void;
            blur(): void;
            focus(): void;
            inertiaLinkAttributes(): {
                data: any;
                href: string;
                method: string;
                replace: boolean;
                preserveScroll: boolean;
                preserveState: boolean;
                only: unknown[];
                id: string;
                value: string | number;
                autofocus: boolean;
                disabled: boolean;
                name: string;
                type: string;
            };
            routerLinkAttributes(): {
                to: any;
                replace: boolean;
                append: boolean;
                tag: string;
                activeClass: string;
                exact: boolean;
                event: string | unknown[];
                exactActiveClass: string;
                id: string;
                value: string | number;
                autofocus: boolean;
                disabled: boolean;
                name: string;
                type: string;
            };
            getAttributes(): {
                data: any;
                href: string;
                method: string;
                replace: boolean;
                preserveScroll: boolean;
                preserveState: boolean;
                only: unknown[];
                id: string;
                value: string | number;
                autofocus: boolean;
                disabled: boolean;
                name: string;
                type: string;
            } | {
                to: any;
                replace: boolean;
                append: boolean;
                tag: string;
                activeClass: string;
                exact: boolean;
                event: string | unknown[];
                exactActiveClass: string;
                id: string;
                value: string | number;
                autofocus: boolean;
                disabled: boolean;
                name: string;
                type: string;
            } | {
                id: string;
                value: string | number;
                autofocus: boolean;
                disabled: boolean;
                name: string;
                href: string;
                type: string;
            };
            render(createElement: import("vue").CreateElement): import("vue").VNode;
        }, {
            isInertiaLinkComponentAvailable: boolean;
            isRouterLinkComponentAvailable: boolean;
            isARouterLink: boolean;
            isAnIntertiaLink: boolean;
            componentToRender: string | import("vue").VueConstructor<import("vue").default> | import("vue").FunctionalComponentOptions<any, import("vue/types/options").PropsDefinition<any>> | import("vue").ComponentOptions<never, any, any, any, any, Record<string, any>> | import("vue/types/options").AsyncComponentPromise<any, any, any, any> | import("vue/types/options").AsyncComponentFactory<any, any, any, any> | undefined;
        }, {
            value: string | number;
            text: string;
            tagName: string;
            type: string;
            href: string;
            to: any;
            append: boolean;
            activeClass: string;
            exact: boolean;
            exactActiveClass: string;
            event: string | unknown[];
            data: any;
            method: string;
            replace: boolean;
            preserveScroll: boolean;
            preserveState: boolean;
            only: unknown[];
            native: boolean;
            classes: any;
        }, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin>;
        props: {
            fixedClasses: any[];
            classes: any[];
            variants: {};
        };
    };
    't-input': {
        component: import("vue/types/vue").ExtendedVue<{
            localValue: string | number | null;
            valueWhenFocus: string | number | null;
        } & {
            blurHandler(e: FocusEvent): void;
            focusHandler(e: FocusEvent): void;
            keyupHandler(e: KeyboardEvent): void;
            keydownHandler(e: KeyboardEvent): void;
            blur(): void;
            click(): void;
            focus(options?: FocusOptions | undefined): void;
            select(): void;
            setSelectionRange(start: number, end: number, direction?: "forward" | "backward" | "none" | undefined): void;
            setRangeText(replacement: string): void;
        } & {
            value: string | number;
            autocomplete: string;
            maxlength: string | number;
            minlength: string | number;
            multiple: boolean;
            pattern: string;
            placeholder: string;
            classes: any;
        } & {
            getListeners(listeners: {
                [key: string]: Function | Function[];
            } | undefined): {
                [key: string]: Function | Function[];
            } | undefined;
        } & {
            id: string;
            name: string;
            disabled: boolean;
            readonly: boolean;
            autofocus: boolean;
            required: boolean;
            tabindex: string | number;
        } & {
            getElementCssClass(elementName?: string | undefined, defaultClasses?: import("vue-tailwind/dist/types/CssClass").default): import("vue-tailwind/dist/types/CssClass").default;
        } & {
            componentClass: import("vue-tailwind/dist/types/CssClass").default;
            activeVariant: string | undefined;
        } & {
            classes: any;
            fixedClasses: any;
            variants: any;
            variant: any;
        } & import("vue").default<Record<string, any>, Record<string, any>, never, never, (event: string, ...args: any[]) => import("vue").default>, unknown, {
            render(createElement: import("vue").CreateElement): import("vue").VNode;
            inputHandler(e: Event): void;
        }, unknown, {
            type: string;
            max: string | number;
            min: string | number;
            classes: any;
        }, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin>;
        props: {
            fixedClasses: string;
            classes: string;
            variants: {
                haiti: string;
                'haiti-error': string;
                tertiary: string;
                'tertiary-error': string;
            };
        };
    };
    't-modal': {
        component: import("vue/types/vue").ExtendedVue<{
            getElementCssClass(elementName?: string | undefined, defaultClasses?: import("vue-tailwind/dist/types/CssClass").default): import("vue-tailwind/dist/types/CssClass").default;
        } & {
            componentClass: import("vue-tailwind/dist/types/CssClass").default;
            activeVariant: string | undefined;
        } & {
            classes: any;
            fixedClasses: any;
            variants: any;
            variant: any;
        } & import("vue").default<Record<string, any>, Record<string, any>, never, never, (event: string, ...args: any[]) => import("vue").default>, {
            overlayShow: boolean;
            modalShow: boolean;
            params: undefined;
            preventAction: boolean;
        }, {
            render(createElement: import("vue").CreateElement): import("vue").VNode;
            renderWrapper(createElement: import("vue").CreateElement): import("vue").VNode;
            renderModal(createElement: import("vue").CreateElement): import("vue").VNode;
            renderChilds(createElement: import("vue").CreateElement): import("vue").VNode[] | undefined;
            clickHandler(e: MouseEvent): void;
            keyupHandler(e: KeyboardEvent): void;
            beforeOpen(): void;
            opened(): void;
            beforeClose(): void;
            closed(): void;
            prepareDomForModal(): void;
            hide(): void;
            show(params?: undefined): void;
            cancel(): void;
            outsideClick(): void;
            getOverlay(): HTMLDivElement | undefined;
        }, unknown, {
            name: string;
            value: boolean;
            header: string;
            footer: string;
            clickToClose: boolean;
            escToClose: boolean;
            noBody: boolean;
            hideCloseButton: boolean;
            disableBodyScroll: boolean;
            bodyScrollLockOptions: any;
            focusOnOpen: boolean;
            fixedClasses: any;
            classes: any;
        }, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin>;
        props: {
            fixedClasses: {
                overlay: string;
                wrapper: string;
                modal: string;
                body: string;
                header: string;
                footer: string;
                close: string;
            };
            classes: {
                overlay: string;
                wrapper: string;
                modal: string;
                header: string;
                footer: string;
                close: string;
                closeIcon: string;
                overlayEnterClass: string;
                overlayEnterActiveClass: string;
                overlayEnterToClass: string;
                overlayLeaveClass: string;
                overlayLeaveActiveClass: string;
                overlayLeaveToClass: string;
                enterClass: string;
                enterActiveClass: string;
                enterToClass: string;
                leaveClass: string;
                leaveActiveClass: string;
                leaveToClass: string;
            };
            variants: {};
        };
    };
    't-radio': {
        component: import("vue/types/vue").ExtendedVue<{
            getListeners(listeners: {
                [key: string]: Function | Function[];
            } | undefined): {
                [key: string]: Function | Function[];
            } | undefined;
        } & {
            id: string;
            name: string;
            disabled: boolean;
            readonly: boolean;
            autofocus: boolean;
            required: boolean;
            tabindex: string | number;
        } & {
            getElementCssClass(elementName?: string | undefined, defaultClasses?: import("vue-tailwind/dist/types/CssClass").default): import("vue-tailwind/dist/types/CssClass").default;
        } & {
            componentClass: import("vue-tailwind/dist/types/CssClass").default;
            activeVariant: string | undefined;
        } & {
            classes: any;
            fixedClasses: any;
            variants: any;
            variant: any;
        } & import("vue").default<Record<string, any>, Record<string, any>, never, never, (event: string, ...args: any[]) => import("vue").default>, {
            localValue: any;
        }, {
            renderWrapped(createElement: import("vue").CreateElement): import("vue").VNode;
            render(createElement: import("vue").CreateElement): import("vue").VNode;
            inputHandler(e: Event): Promise<void>;
            sendInputEventToTheNotCheckedInputs(): void;
            selectPrevRadio(e: KeyboardEvent): void;
            selectNextRadio(e: KeyboardEvent): void;
            wrapperSpaceHandler(e: KeyboardEvent): void;
            blurHandler(e: FocusEvent): void;
            focusHandler(e: FocusEvent): void;
            blur(): void;
            click(): void;
            focus(options?: FocusOptions | undefined): void;
        }, {
            isChecked: boolean;
        }, {
            value: any;
            checked: string | boolean;
            model: any;
            wrapped: boolean;
            wrapperTag: string;
            inputWrapperTag: string;
            labelTag: string;
            label: string | number;
            classes: any;
        }, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin>;
        props: {
            fixedClasses: string;
            classes: string;
            variants: {
                Custom: string;
            };
        };
    };
    't-rich-select': {
        component: import("vue/types/vue").ExtendedVue<{
            value: any;
            multiple: boolean;
        } & {
            localValue: any;
        } & {
            normalizeOptions(options: import("vue-tailwind/dist/types/InputOptions").default): import("vue-tailwind/dist/types/NormalizedOptions").default;
        } & {
            normalizedOptions: import("vue-tailwind/dist/types/NormalizedOptions").default;
            flattenedOptions: import("vue-tailwind/dist/types/NormalizedOptions").default;
        } & {
            value: any;
            valueAttribute: string;
            textAttribute: string;
            options: any;
        } & {
            getListeners(listeners: {
                [key: string]: Function | Function[];
            } | undefined): {
                [key: string]: Function | Function[];
            } | undefined;
        } & {
            id: string;
            name: string;
            disabled: boolean;
            readonly: boolean;
            autofocus: boolean;
            required: boolean;
            tabindex: string | number;
        } & {
            getElementCssClass(elementName?: string | undefined, defaultClasses?: import("vue-tailwind/dist/types/CssClass").default): import("vue-tailwind/dist/types/CssClass").default;
        } & {
            componentClass: import("vue-tailwind/dist/types/CssClass").default;
            activeVariant: string | undefined;
        } & {
            classes: any;
            fixedClasses: any;
            variants: any;
            variant: any;
        } & import("vue").default<Record<string, any>, Record<string, any>, never, never, (event: string, ...args: any[]) => import("vue").default>, {
            hasFocus: boolean;
            show: boolean;
            localValue: string | number | boolean | symbol | (string | number | boolean | symbol | null)[] | null;
            highlighted: number | null;
            query: string;
            filteredOptions: import("vue-tailwind/dist/types/NormalizedOptions").default;
            selectedOption: import("vue-tailwind/dist/types/NormalizedOption").default | undefined;
            selectedOptions: import("vue-tailwind/dist/types/NormalizedOptions").default;
            searching: boolean;
            delayTimeout: NodeJS.Timeout | undefined;
            nextPage: number | undefined;
            tagsAreFocusable: boolean;
        }, {
            findOptionByValue(value: string | number | boolean | symbol | null): undefined | import("vue-tailwind/dist/types/NormalizedOption").default;
            optionHasValue(option: import("vue-tailwind/dist/types/NormalizedOption").default, value: string | number | boolean | symbol | null): boolean;
            createSelect(createElement: import("vue").CreateElement): import("vue").VNode;
            filterOptions(query: string): Promise<void>;
            getFilterPromise(query: string): Promise<{
                results: import("vue-tailwind/dist/types/InputOptions").default;
                hasMorePages?: boolean;
            }>;
            listEndReached(): void;
            queryFilter(options: import("vue-tailwind/dist/types/NormalizedOptions").default): import("vue-tailwind/dist/types/NormalizedOptions").default;
            hideOptions(): void;
            showOptions(): void;
            toggleOptions(): void;
            focusSearchBox(): Promise<void>;
            blurHandler(e: FocusEvent): void;
            focusHandler(e: FocusEvent): void;
            clickHandler(e: MouseEvent): void;
            findNextOptionIndex(currentOptionIndex?: null | number): number;
            findPrevOptionIndex(currentOptionIndex: null | number): number;
            arrowUpHandler(e: KeyboardEvent): Promise<void>;
            arrowDownHandler(e: KeyboardEvent): void;
            listScrollHandler(e: Event): void;
            scrollToHighlightedOption(behavior?: "auto" | "smooth"): void;
            escapeHandler(e: KeyboardEvent): void;
            enterHandler(e: KeyboardEvent): void;
            searchInputHandler(e: Event): void;
            getButton(): HTMLButtonElement;
            getTagsContainer(): HTMLButtonElement;
            getSearchBox(): HTMLInputElement;
            selectOption(option: import("vue-tailwind/dist/types/NormalizedOption").default, focus?: boolean): Promise<void>;
            unselectOptionAtIndex(index: number): void;
            clearButtonClickHandler(e: MouseEvent): void;
            blur(): void;
            focus(options?: FocusOptions | undefined): void;
            selectTag(tag: HTMLButtonElement): Promise<void>;
            unselectTag(): Promise<void>;
        }, {
            usesAjax: boolean;
            shouldShowSearchbox: boolean;
            hasMinimumInputLength: boolean;
            flattenedOptions: import("vue-tailwind/dist/types/NormalizedOptions").default;
            filteredflattenedOptions: import("vue-tailwind/dist/types/NormalizedOptions").default;
            atLeastOneValidOptionExists: boolean;
            normalizedHeight: string;
            selectedOptionIndex: number | undefined;
            highlightedOption: import("vue-tailwind/dist/types/NormalizedOption").default | undefined;
        }, {
            delay: number;
            fetchOptions: Function;
            minimumResultsForSearch: number;
            minimumInputLength: number;
            minimumInputLengthText: TimerHandler;
            hideSearchBox: boolean;
            openOnFocus: boolean;
            closeOnSelect: boolean;
            selectOnClose: boolean;
            clearable: boolean;
            multiple: boolean;
            placeholder: string;
            searchBoxPlaceholder: string;
            noResultsText: string;
            searchingText: string;
            loadingMoreResultsText: string;
            maxHeight: string | number;
            fixedClasses: any;
            classes: any;
        }, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin>;
        props: {
            fixedClasses: {
                wrapper: string;
                buttonWrapper: string;
                selectButton: string;
                selectButtonLabel: string;
                selectButtonTagWrapper: string;
                selectButtonTag: string;
                selectButtonTagText: string;
                selectButtonTagDeleteButton: string;
                selectButtonTagDeleteButtonIcon: string;
                selectButtonPlaceholder: string;
                selectButtonIcon: string;
                selectButtonClearButton: string;
                selectButtonClearIcon: string;
                dropdown: string;
                dropdownFeedback: string;
                loadingMoreResults: string;
                optionsList: string;
                searchWrapper: string;
                searchBox: string;
                optgroup: string;
                option: string;
                disabledOption: string;
                highlightedOption: string;
                selectedOption: string;
                selectedHighlightedOption: string;
                optionContent: string;
                optionLabel: string;
                selectedIcon: string;
                enterClass: string;
                enterActiveClass: string;
                enterToClass: string;
                leaveClass: string;
                leaveActiveClass: string;
                leaveToClass: string;
            };
            classes: {
                wrapper: string;
                buttonWrapper: string;
                selectButton: string;
                selectButtonLabel: string;
                selectButtonTagWrapper: string;
                selectButtonTag: string;
                selectButtonTagText: string;
                selectButtonTagDeleteButton: string;
                selectButtonTagDeleteButtonIcon: string;
                selectButtonPlaceholder: string;
                selectButtonIcon: string;
                selectButtonClearButton: string;
                selectButtonClearIcon: string;
                dropdown: string;
                dropdownFeedback: string;
                loadingMoreResults: string;
                optionsList: string;
                searchWrapper: string;
                searchBox: string;
                optgroup: string;
                option: string;
                disabledOption: string;
                highlightedOption: string;
                selectedOption: string;
                selectedHighlightedOption: string;
                optionContent: string;
                optionLabel: string;
                selectedIcon: string;
                enterClass: string;
                enterActiveClass: string;
                enterToClass: string;
                leaveClass: string;
                leaveActiveClass: string;
                leaveToClass: string;
            };
            variants: {
                primary: {};
                secondary: {
                    selectButton: string;
                    selectButtonTag: string;
                    dropdown: string;
                    searchBox: string;
                };
                tertiary: {};
                alabaster: {};
                'white-smoke': {};
                haiti: {
                    wrapper: string;
                    buttonWrapper: string;
                    selectButton: string;
                    selectButtonLabel: string;
                    selectButtonTagWrapper: string;
                    selectButtonTag: string;
                    selectButtonTagText: string;
                    selectButtonTagDeleteButton: string;
                    selectButtonTagDeleteButtonIcon: string;
                    selectButtonPlaceholder: string;
                    selectButtonIcon: string;
                    selectButtonClearButton: string;
                    selectButtonClearIcon: string;
                    dropdown: string;
                    dropdownFeedback: string;
                    loadingMoreResults: string;
                    optionsList: string;
                    searchWrapper: string;
                    searchBox: string;
                    optgroup: string;
                    option: string;
                    disabledOption: string;
                    highlightedOption: string;
                    selectedOption: string;
                    selectedHighlightedOption: string;
                    optionContent: string;
                    optionLabel: string;
                    selectedIcon: string;
                    enterClass: string;
                    enterActiveClass: string;
                    enterToClass: string;
                    leaveClass: string;
                    leaveActiveClass: string;
                    leaveToClass: string;
                };
                mirage: {
                    wrapper: string;
                    buttonWrapper: string;
                    selectButton: string;
                    selectButtonLabel: string;
                    selectButtonTagWrapper: string;
                    selectButtonTag: string;
                    selectButtonTagText: string;
                    selectButtonTagDeleteButton: string;
                    selectButtonTagDeleteButtonIcon: string;
                    selectButtonPlaceholder: string;
                    selectButtonIcon: string;
                    selectButtonClearButton: string;
                    selectButtonClearIcon: string;
                    dropdown: string;
                    dropdownFeedback: string;
                    loadingMoreResults: string;
                    optionsList: string;
                    searchWrapper: string;
                    searchBox: string;
                    optgroup: string;
                    option: string;
                    disabledOption: string;
                    highlightedOption: string;
                    selectedOption: string;
                    selectedHighlightedOption: string;
                    optionContent: string;
                    optionLabel: string;
                    selectedIcon: string;
                    enterClass: string;
                    enterActiveClass: string;
                    enterToClass: string;
                    leaveClass: string;
                    leaveActiveClass: string;
                    leaveToClass: string;
                };
            };
        };
    };
    't-textarea': {
        component: import("vue/types/vue").ExtendedVue<{
            localValue: string | number | null;
            valueWhenFocus: string | number | null;
        } & {
            blurHandler(e: FocusEvent): void;
            focusHandler(e: FocusEvent): void;
            keyupHandler(e: KeyboardEvent): void;
            keydownHandler(e: KeyboardEvent): void;
            blur(): void;
            click(): void;
            focus(options?: FocusOptions | undefined): void;
            select(): void;
            setSelectionRange(start: number, end: number, direction?: "forward" | "backward" | "none" | undefined): void;
            setRangeText(replacement: string): void;
        } & {
            value: string | number;
            autocomplete: string;
            maxlength: string | number;
            minlength: string | number;
            multiple: boolean;
            pattern: string;
            placeholder: string;
            classes: any;
        } & {
            getListeners(listeners: {
                [key: string]: Function | Function[];
            } | undefined): {
                [key: string]: Function | Function[];
            } | undefined;
        } & {
            id: string;
            name: string;
            disabled: boolean;
            readonly: boolean;
            autofocus: boolean;
            required: boolean;
            tabindex: string | number;
        } & {
            getElementCssClass(elementName?: string | undefined, defaultClasses?: import("vue-tailwind/dist/types/CssClass").default): import("vue-tailwind/dist/types/CssClass").default;
        } & {
            componentClass: import("vue-tailwind/dist/types/CssClass").default;
            activeVariant: string | undefined;
        } & {
            classes: any;
            fixedClasses: any;
            variants: any;
            variant: any;
        } & import("vue").default<Record<string, any>, Record<string, any>, never, never, (event: string, ...args: any[]) => import("vue").default>, unknown, {
            render(createElement: import("vue").CreateElement): import("vue").VNode;
            inputHandler(e: Event): void;
        }, unknown, {
            rows: string;
            cols: string;
            wrap: string;
            classes: any;
        }, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin>;
        props: {
            fixedClasses: string;
            classes: string;
            variants: {
                danger: string;
                success: string;
            };
        };
    };
    't-toggle': {
        component: import("vue/types/vue").ExtendedVue<{
            getListeners(listeners: {
                [key: string]: Function | Function[];
            } | undefined): {
                [key: string]: Function | Function[];
            } | undefined;
        } & {
            id: string;
            name: string;
            disabled: boolean;
            readonly: boolean;
            autofocus: boolean;
            required: boolean;
            tabindex: string | number;
        } & {
            getElementCssClass(elementName?: string | undefined, defaultClasses?: import("vue-tailwind/dist/types/CssClass").default): import("vue-tailwind/dist/types/CssClass").default;
        } & {
            componentClass: import("vue-tailwind/dist/types/CssClass").default;
            activeVariant: string | undefined;
        } & {
            classes: any;
            fixedClasses: any;
            variants: any;
            variant: any;
        } & import("vue").default<Record<string, any>, Record<string, any>, never, never, (event: string, ...args: any[]) => import("vue").default>, {
            isChecked: boolean;
        }, {
            blurHandler(e: FocusEvent): void;
            focusHandler(e: FocusEvent): void;
            getElement(): HTMLDivElement;
            blur(): void;
            click(): void;
            spaceHandler(e: KeyboardEvent): void;
            clickHandler(): void;
            toggleValue(): void;
            setChecked(checked: boolean): void;
            focus(options?: FocusOptions | undefined): void;
        }, {
            isDisabled: boolean;
            currentValue: import("vue-tailwind/dist/types/CheckboxValues").default;
        }, {
            value: any;
            uncheckedValue: any;
            model: any;
            checked: boolean;
            tabindex: string | number;
            uncheckedPlaceholder: string;
            checkedPlaceholder: string;
            uncheckedLabel: string;
            checkedLabel: string;
            classes: any;
            fixedClasses: any;
        }, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin>;
        props: {
            fixedClasses: {};
            classes: {};
            variants: {};
        };
    };
};
export default _default;
//# sourceMappingURL=index.d.ts.map