declare namespace _default {
    export { getBreakpointForWidth };
    export { currentBreakpoint };
    export { getThemes };
    export { setTheme };
    export { currentTheme };
    export { isTheme };
}
export default _default;
declare function getBreakpointForWidth(width: number): object | null;
declare function currentBreakpoint(): string | null;
declare function getThemes(): string[];
declare function setTheme(theme: string): void;
declare class setTheme {
    constructor(theme: string);
    currentTheme: any;
}
declare function currentTheme(): string;
declare function isTheme(map: object, defaultValue: any): any;
//# sourceMappingURL=ui.d.ts.map