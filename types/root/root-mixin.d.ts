declare namespace _default {
    function data(): {
        shared: {
            TAILWIND: object;
        };
        userSettings: {
            theme: any;
        };
        currentBreakpoint: object | null;
        currentTheme: string | null;
        dataReady: boolean;
    };
    function created(): Promise<void>;
}
export default _default;
//# sourceMappingURL=root-mixin.d.ts.map