declare const PrivateRoute_base: any;
export class PrivateRoute extends PrivateRoute_base {
    [x: string]: any;
    constructor(path: string, name: string, component: Function | null, options?: object);
}
export default PrivateRoute;
//# sourceMappingURL=PrivateRoute.d.ts.map