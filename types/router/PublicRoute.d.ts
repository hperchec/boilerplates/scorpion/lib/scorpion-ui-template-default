declare const PublicRoute_base: any;
export class PublicRoute extends PublicRoute_base {
    [x: string]: any;
    constructor(path: string, name: string, component: Function | null, options?: object);
}
export default PublicRoute;
//# sourceMappingURL=PublicRoute.d.ts.map