declare const Route_base: any;
export class Route extends Route_base {
    [x: string]: any;
    static getBreadcrumb(route: any): any;
}
export default Route;
//# sourceMappingURL=Route.d.ts.map