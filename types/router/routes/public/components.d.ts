declare namespace _default {
    let path: string;
    let name: string;
    function component(): any;
    namespace options {
        namespace meta {
            namespace breadcrumb {
                let schema: string[];
                function title(vm: any): any;
            }
            namespace pageMeta {
                export function title_1(vm: any): any;
                export { title_1 as title };
            }
        }
    }
}
export default _default;
//# sourceMappingURL=components.d.ts.map